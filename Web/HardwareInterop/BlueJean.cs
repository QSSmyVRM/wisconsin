﻿//ZD 100147 Start //ZD 101217 new class added for Pexip
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : BlueJean.cs
 * DESCRIPTION : All pexip MCU api commands are stored in this file. 
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Threading;


namespace NS_BlueJean
{
    class BlueJean
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        internal bool isRecurring = false; 

        #region BlueJean
        internal BlueJean(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }
        #endregion

        #region Public Class

        internal class Participant
        {
            public string grant_type { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public string name { get; set; }
            public string numericId { get; set; }
            public string moderatorPasscode { get; set; }
        }

        internal class Access
        {
            public string grant_type { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
        }

        internal class Conference
        {
            public string title { get; set; }
            public string description { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public string timezone { get; set; }
            public string numericMeetingId { get; set; }
            public string addAttendeePasscode { get; set; }
            public string allow720p { get; set; }
            public string locked { get; set; }
            public string endPointType { get; set; }
            public string endPointVersion { get; set; }
            public List<ConferenceAttendees> attendees;
        }

        internal class ConferenceAttendees
        {
            public string email { get; set; }
        }    
    

        #endregion

        #region SendCommand
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="strUrl"></param>
        /// <param name="method"></param>
        /// <param name="jsonString"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        private bool SendCommand(string strUrl, string method, string jsonString, ref string receiveXML)
        {
            try
            {

                logger.Trace("*********SendXML***********");

                logger.Trace("Call URL: " + strUrl);
                logger.Trace("Call JsonString: " + jsonString);

                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                   ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);

                req.Method = method;
                req.KeepAlive = true;
                if (!string.IsNullOrEmpty(jsonString))
                {
                    req.PreAuthenticate = true;
                    req.Accept = "application/json";
                    req.ContentType = "application/json; charset=utf-8";
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(jsonString);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();
                }
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                Stream respStream = resp.GetResponseStream();
                StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                receiveXML = rdr.ReadToEnd();

                logger.Trace("*********ResponseXML***********");
                logger.Trace(receiveXML);

                if (!string.IsNullOrEmpty(receiveXML))
                {
                    XNode node = JsonConvert.DeserializeXNode(receiveXML, "Root");
                    receiveXML = node.ToString();
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region  GetUserId
        /// <summary>
        /// GetUserId
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool GetUserId_Old(int Roomid, string email, string password, string access_token)
        {
            string StrURL = "", method = "", input = "", responseXML = "", BJNUserid = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            try
            {
                StrURL = "https://api.bluejeans.com/oauth2/token";

                method = "POST";

                ParticipantList = new List<Participant>();
                Party = new Participant();
                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                Party.grant_type = "password";
                Party.username = email;
                Party.password = password;

                ParticipantList.Add(Party);

                input = JsonConvert.SerializeObject(ParticipantList, Jsetting);
                input = input.Replace("[", "").Replace("]", "");

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret) 
                    return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/scope/user");
                    if (node != null)
                        BJNUserid = node.InnerText.Trim();
                }

                if (BJNUserid != "")
                {
                    ret = UpdateUserDetails(Roomid, BJNUserid, access_token);
                    if (!ret) return false;
                }
                else
                {
                    logger.Trace("No User Found.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        internal bool GetUserId(int Roomid, string email, NS_MESSENGER.MCU mcu, int accessType) //0-Room , 1-User
        {
            string StrURL = "", method = "", input = "", responseXML = "", BJNUserid = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            try
            {

                StrURL = "https://api.bluejeans.com/v1/enterprise/" + mcu.ienterpriseId + "/users/?textSearch=" + email + "&access_token=" + mcu.sBJNAccessToken;

                method = "GET";

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret)
                    return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/users/id");
                    if (node != null)
                        BJNUserid = node.InnerText.Trim();
                }

                if (BJNUserid != "")
                {
                    if (accessType == 0)
                    {
                        ret = UpdateUserDetails(Roomid, BJNUserid, mcu.sBJNAccessToken);
                        if (!ret) return false;
                    }
                    else
                    {
                        ret = GetBJNRoomDetails(Roomid, BJNUserid, mcu.sBJNAccessToken);
                        if (!ret) return false;
                    }
                }
                else
                {
                    logger.Trace("No User Found.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  UpdateUserDetails
        /// <summary>
        /// UpdateUserDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool UpdateUserDetails(int Roomid, string BJNUserid, string Accestoken)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            string BJNUsername = "", BJNPartyPassCode = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            XmlNode node = null;
            XmlDocument xd = new XmlDocument();
            try
            {

                //StrURL = "https://api.bluejeans.com/v1/user/" + BJNUserid + "/?access_token=" + Accestoken;

                //method = "GET";

                //ret = false;
                //ret = SendCommand(StrURL, method, input, ref responseXML);
                //if (!ret) 
                //    return false;

                //if (!string.IsNullOrEmpty(responseXML))
                //{
                //    node = null;
                //    xd = new XmlDocument();
                //    xd.LoadXml(responseXML);

                //    node = xd.SelectSingleNode("//Root/username");
                //    if (node != null)
                //        BJNUsername = node.InnerText.Trim();

                //}

                StrURL = "https://api.bluejeans.com/v1/user/" + BJNUserid + "/room/?access_token=" + Accestoken;

                method = "GET";

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret)
                    return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    node = null;
                    xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/name");
                    if (node != null)
                        BJNUsername = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//Root/participantPasscode");
                    if (node != null)
                        BJNPartyPassCode = node.InnerText.Trim();
                }


                ret = db.UpdateBJNUserDetails(Roomid, BJNUserid, BJNUsername, BJNPartyPassCode);
                if (!ret)
                {
                    logger.Trace("Error in Updating BJN Tokens.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  GetBJNRoomDetails
        /// <summary>
        /// GetBJNRoomDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool GetBJNRoomDetails(int userid, string BJNUserid, string Accestoken)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            string BJNUsername = "", BJNMeetingId = "", BJNPassCode = "", BJNPartyPassCode = "";
            bool ret = false;
            try
            {

                StrURL = "https://api.bluejeans.com/v1/user/" + BJNUserid + "/room/?access_token=" + Accestoken;

                method = "GET";

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret)
                    return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/name");
                    if (node != null)
                        BJNUsername = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//Root/numericId");
                    if (node != null)
                        BJNMeetingId = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//Root/moderatorPasscode");
                    if (node != null)
                        BJNPassCode = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//Root/participantPasscode");
                    if (node != null)
                        BJNPartyPassCode = node.InnerText.Trim();
                }

                ret = db.UpdateBJNUserDetails(userid, BJNUserid, BJNUsername, BJNMeetingId, BJNPassCode, BJNPartyPassCode);
                if (!ret)
                {
                    logger.Trace("Error in Updating BJN Tokens.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  Create_updateUserRoom
        /// <summary>
        /// Create_updateUserRoom
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool Create_updateUserRoom(string AccessToken, NS_MESSENGER.BJNRoomDetails BJNDetails)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            cryptography.Crypto crypto = null;
            try
            {

                StrURL = "https://api.bluejeans.com/v1/user/" + BJNDetails.userID + "/room/?access_token=" + AccessToken;

                method = "PUT";

                ParticipantList = new List<Participant>();
                Party = new Participant();
                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                Party.name = BJNDetails.name;
                Party.numericId = BJNDetails.numericId;

                crypto = new cryptography.Crypto();
                if (!string.IsNullOrEmpty(BJNDetails.moderatorPasscode))
                    BJNDetails.moderatorPasscode = crypto.decrypt(BJNDetails.moderatorPasscode);


                Party.moderatorPasscode = BJNDetails.moderatorPasscode;

                ParticipantList.Add(Party);

                input = JsonConvert.SerializeObject(ParticipantList, Jsetting);
                input = input.Replace("[", "").Replace("]", "");

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);

                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  DeleteConference
        /// <summary>
        /// DeleteConference
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool DeleteConference(NS_MESSENGER.Conference conf, string accesstoken)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            try
            {

                StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/" + conf.sBJNUniqueid + "?access_token=" + accesstoken;
                method = "DELETE";

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);

                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  GetConference
        /// <summary>
        /// GetConference
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool GetConference(NS_MESSENGER.Conference conf, string BJNUniqueid, string accesstoken, ref string Meetingnumber, ref string Passcode)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            try
            {

                StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/" + BJNUniqueid + "?access_token=" + accesstoken;

                method = "GET";

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret) return false;


                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/numericMeetingId");
                    if (node != null)
                        Meetingnumber = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//Root/attendeePasscode");
                    if (node != null)
                        Passcode = node.InnerText.Trim();
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  SetConference
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool SetConference(NS_MESSENGER.Conference templateConf, string accesstoken)
        {
            string StrURL = "", method = "", input = "", responseXML = "", Passcode = "", Meetingnumber = "";
            bool ret = false;
            int isPushtoExternal = 0, cnfCnt = 0;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Conference> conferencelist = new List<Conference>();
            List<ConferenceAttendees> conferenceAttendeelist = new List<ConferenceAttendees>();
            Conference conference = new Conference();
            ConferenceAttendees ConferenceAttendee = new ConferenceAttendees();
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
            List<NS_MESSENGER.Conference> confList = new List<NS_MESSENGER.Conference>();
            NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                if (templateConf.iRecurring == 1 && isRecurring)
                {
                    DeleteSyncConference(templateConf, accesstoken);
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.BLUEJEANS, 0);
                    confList = confs;
                }
                else
                    confList[0] = templateConf;


                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    conf = confList[cnfCnt];
                    if (string.IsNullOrEmpty(conf.sBJNUniqueid))
                    {
                        StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/?access_token=" + accesstoken;
                        method = "POST";
                    }
                    else
                    {
						//ALLDEV-847 - Start
                        StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/" + conf.sBJNUniqueid + "/?access_token=" + accesstoken;
                        method = "PUT";

                        
                        /*DeleteConference(conf, accesstoken);
                        conf.sBJNUniqueid = "";
                        ret = db.UpdateBJNCallId(conf.iDbID, conf.iInstanceID, conf.sBJNUniqueid, Meetingnumber, Passcode);
                        if (!ret)
                            return false;

                        if (conf.iBJNConf == 0)
                        {
                            logger.Trace("Meeting is updates as Non-BJN conference");
                            return false;
                        }
                        StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/?access_token=" + accesstoken;
                        method = "POST";*/
						//ALLDEV-847 - End
                    }

                    conferencelist = new List<Conference>();
                    conference = new Conference();
                    Jsetting.NullValueHandling = NullValueHandling.Ignore;

                    conference.title = conf.sDbName;

                    if (string.IsNullOrEmpty(conf.sDescription))
                        conference.description = "";
                    else
                        conference.description = conf.sDescription;
                    int StartunixTimestamp = (int)(conf.dtStartDateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    conference.start = StartunixTimestamp.ToString() + "000";
                    int EndunixTimestamp = (int)(conf.dtEndDateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    conference.end = EndunixTimestamp.ToString() + "000";
                    conference.timezone = "Africa/Abidjan";
                    if (string.IsNullOrEmpty(conf.sBJNUniqueid))
                        conference.numericMeetingId = conf.iDbNumName.ToString();
                    conference.addAttendeePasscode = "true";
                    conference.allow720p = "true";
                    conference.locked = "false";
                    conference.endPointType = "WEB_APP";
                    conference.endPointVersion = "1.80";

                    conferenceAttendeelist = new List<ConferenceAttendees>();
                    conference.attendees = new List<ConferenceAttendees>();
                    ConferenceAttendee.email = "";
                    conference.attendees.Add(ConferenceAttendee);

                    conferencelist.Add(conference);

                    input = JsonConvert.SerializeObject(conferencelist, Jsetting);
                    input = input.Remove(input.IndexOf("["), 1);
                    input = input.Remove(input.Length - 1);

                    ret = false;
                    ret = SendCommand(StrURL, method, input, ref responseXML);
                    if (!ret)
                        return false;

                    if (!string.IsNullOrEmpty(responseXML))
                    {
                        XmlNode node = null;
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml(responseXML);

                        node = xd.SelectSingleNode("//Root/id");
                        if (node != null)
                            conf.sBJNUniqueid = node.InnerText.Trim();
                    }

                    ret = GetConference(conf, conf.sBJNUniqueid, accesstoken, ref Meetingnumber, ref Passcode);
                    if (!ret)
                    {
                        logger.Trace("Error in Fetching Meeting");
                        return false;
                    }
                    if (!string.IsNullOrEmpty(conf.sBJNUniqueid))
                    {
                        ret = db.UpdateBJNCallId(conf.iDbID, conf.iInstanceID, conf.sBJNUniqueid, Meetingnumber, Passcode);
                        if (!ret)
                            return false;

                        //ret = db.UpdateBJNPartcipantId(conf.iDbID, conf.iInstanceID, Meetingnumber, Passcode);
                        //if (!ret)
                        //    return false;

                        isPushtoExternal = 1;
                        db.UpdatePushToExternal(conf.iDbID, conf.iInstanceID, isPushtoExternal);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  UpdateAccessToken
        /// <summary>
        /// UpdateAccessToken
        /// </summary>
        /// <param name="Appkey"></param>
        /// <param name="Appsecret"></param>
        /// <returns></returns>
        internal bool UpdateAccessToken(ref NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            int expires_in = 0, enterpriseId = 0;
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Access> acclist = new List<Access>();
            Access acc = new Access();
            try
            {
                StrURL = "https://api.bluejeans.com/oauth2/token";

                method = "POST";

                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                acc.grant_type = "client_credentials";
                acc.client_id = mcu.sBJNAPPkey;
                acc.client_secret = mcu.sBJNAppsecret;

                acclist.Add(acc);

                input = JsonConvert.SerializeObject(acclist, Jsetting);
                input = input.Replace("[", "").Replace("]", "");

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);

                if (ret)
                {
                    if (!string.IsNullOrEmpty(responseXML))
                    {
                        XmlNode node = null;
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml(responseXML);

                        node = xd.SelectSingleNode("//Root/access_token");
                        if (node != null)
                            mcu.sBJNAccessToken = node.InnerText.Trim();

                        node = xd.SelectSingleNode("//Root/expires_in");
                        if (node != null)
                        {
                            int.TryParse(node.InnerText.Trim(), out expires_in);
                            mcu.iBJNExpiry = expires_in;
                        }
                        node = xd.SelectSingleNode("//Root/scope/enterprise");
                        if (node != null)
                        {
                            int.TryParse(node.InnerText.Trim(), out enterpriseId);
                            mcu.ienterpriseId = enterpriseId;
                        }
                    }
                    mcu.dBJNLastmodifieddatetime = DateTime.Now;
                    mcu.dBJNExpiryDateTime = DateTime.Now.AddSeconds(expires_in);
                    ret = db.UpdateBJNToken(mcu);
                    if (!ret)
                    {
                        logger.Trace("Error in Updating BJN Tokens.");
                        return false;
                    }
                }
                else
                    return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region DeleteSyncConference
        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf, string accesstoken)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete, NS_MESSENGER.MCU.eType.BLUEJEANS);

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {

                    bool ret = DeleteConference(confListDelete[cnfCnt], accesstoken);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                    }
                    else
                    {
                        if (confListDelete[cnfCnt].iWebExconf != 1)
                        {
                            ret = false;
                            ret = db.UpdateSyncStatus(confListDelete[cnfCnt]);
                            if (!ret)
                                logger.Exception(100, "Delete failed in confSyncMCUAdj");

                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf,string accesstoken)
        {
            try
            {
                List<NS_MESSENGER.Conference>  confList = new List<NS_MESSENGER.Conference>();
                List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);
                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.BLUEJEANS, 1); //for delete mode need to send delStatus as 1 //ZD 100221
                    confList = confs;
                }
                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    bool ret = DeleteConference(confList[cnfCnt], accesstoken);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }

        #endregion

        #region  ExtendConfEndTime
        /// <summary>
        /// ExtendConfEndTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool ExtendConfEndTime(NS_MESSENGER.Conference conf, int extendTime, string accesstoken)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            int Duration = 0;
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Conference> conferencelist = new List<Conference>();
            List<ConferenceAttendees> conferenceAttendeelist = new List<ConferenceAttendees>();
            Conference conference = new Conference();
            ConferenceAttendees ConferenceAttendee = new ConferenceAttendees();
            try
            {

                StrURL = "https://api.bluejeans.com/v1/user/" + conf.sBJNUserId + "/scheduled_meeting/" + conf.sBJNUniqueid + "/?access_token=" + accesstoken;
                method = "PUT";

                conferencelist = new List<Conference>();
                conference = new Conference();
                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                conference.title = conf.sDbName;
                int StartunixTimestamp = (int)(conf.dtStartDateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                conference.start = StartunixTimestamp.ToString() + "000";

                Duration = conf.iDuration + extendTime;
                conf.dtEndDateTime = conf.dtStartDateTime.AddMinutes(Duration);

                int EndunixTimestamp = (int)(conf.dtEndDateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                conference.end = EndunixTimestamp.ToString() + "000";
                conference.timezone = "Africa/Abidjan";

                conferencelist.Add(conference);

                input = JsonConvert.SerializeObject(conferencelist, Jsetting);
                input = input.Remove(input.IndexOf("["), 1);
                input = input.Remove(input.Length - 1);

                ret = false;
                ret = SendCommand(StrURL, method, input, ref responseXML);
                if (!ret)
                    return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

    }
}
