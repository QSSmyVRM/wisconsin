/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class en_Allocation : System.Web.UI.Page
{

    #region Private Data Members
    /// <summary>
    /// Private Data Members
    /// </summary>

    
    private String inXML = "";
    private String inXMLBulk = "";

    String outGetAllocationXML = "";
    String outGetMgBulkUserXML = "";
    String outXml = "";


   

    XmlDocument XmlDoc = null;
    XmlDocument XmlBulkDoc = null;
    XmlNode node = null;
    XmlNodeList nodes = null;
    

    private String errorMessage = "";

    
    private String departmentsCS = "";
    private String timezonesCS = "";
    private String timezonesIDCS = "";
    private String rolesCS = "";
    private String enableAVCS = "";
    private String tzdisplayCS = "";
    private String tformatCS = "";
    private String dtformatCS = "";
    private String languagesCS = "";
    private String bridgesCS = "";
    private Int32 length = 0;
    private String[] userIds = null;
    private String Email = "";

    private ns_Logger.Logger log;
    private myVRMNet.NETFunctions obj;
    public en_Allocation() //FB 2027
    {
          log = new ns_Logger.Logger();
    }
    private String enableDominoCS = ""; //FB 1599
    private String enableExchangeCS = ""; //FB 1599
    private String enableMobileCS = ""; //FB 1979
    private String enableAVWOCS = "";//ZD 101122
    private String enableCateringWOCS = "";
    private String enableFacilityWOCS = "";
    #endregion

    #region Protected Data members

    /// <summary>
    /// Protected Data members
    /// </summary>

    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.DropDownList DdlOperation;
    protected System.Web.UI.WebControls.Button AllocationSubmit;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnusers;
    protected System.Web.UI.HtmlControls.HtmlInputHidden SelectedUser;
    protected System.Web.UI.HtmlControls.HtmlInputHidden sortby;
    protected System.Web.UI.HtmlControls.HtmlInputHidden alphabet;
    protected System.Web.UI.HtmlControls.HtmlInputHidden pageno;
    protected System.Web.UI.HtmlControls.HtmlInputHidden timezones;
    protected System.Web.UI.HtmlControls.HtmlInputHidden roles;
    protected System.Web.UI.HtmlControls.HtmlInputHidden languages;
    protected System.Web.UI.HtmlControls.HtmlInputHidden bridges;
    protected System.Web.UI.HtmlControls.HtmlInputHidden departments;
    protected System.Web.UI.HtmlControls.HtmlInputHidden canAll;
    protected System.Web.UI.HtmlControls.HtmlInputHidden totalPages;
    protected System.Web.UI.HtmlControls.HtmlInputHidden totalNumber;
    protected System.Web.UI.HtmlControls.HtmlInputHidden dtformat;
    protected System.Web.UI.HtmlControls.HtmlInputHidden tformat;
    protected System.Web.UI.HtmlControls.HtmlInputHidden tzdisplay;
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableAV;
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableAO;//ZD 101093
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableAVWO;//ZD 101122
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableCateringWO;
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableFacilityWO;
    protected System.Web.UI.HtmlControls.HtmlSelect SelPage;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox CheckAllAdUser;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox CheckAllAdUser_Org; // ZD 101362
    //Code added by Offshore for FB Issue 1073 -- Start
    protected String format = "";
    //Code added by Offshore for FB Issue 1073 -- End
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableDomino;//FB 1599
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableExchange;//FB 1599
    protected System.Web.UI.HtmlControls.HtmlInputHidden enableMobile;//FB 1979
    #endregion

    #region Page Load Event Handlers
    /// <summary>
    /// Page Load Event Handlers
    /// </summary>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("allocation.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            
            obj = new myVRMNet.NETFunctions();
            

            /* *** Code added for FB 1425 QA Bug -Start *** */

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
            {
                if (DdlOperation.Items.FindByValue("6") != null)
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("6"));

                if (DdlOperation.Items.FindByValue("12") != null) //Code added for MOJ Phase 2 QA Bug
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("12"));

                if (DdlOperation.Items.FindByValue("11") != null) //Code added for MOJ Phase 2 QA Bug
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("11"));
                if (DdlOperation.Items.FindByValue("3") != null) //Code added for MOJ Phase 2 QA Bug
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("3"));

                if (DdlOperation.Items.FindByValue("13") != null) //FB 1599
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("13"));

                if (DdlOperation.Items.FindByValue("14") != null) //FB 1599
                    DdlOperation.Items.Remove(DdlOperation.Items.FindByValue("14"));
            }

            /* *** Code added for FB 1425 QA Bug -End *** */
            //Code added by Offshore for FB Issue 1073 -- Start
            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }
            //Code added by Offshore for FB Issue 1073 -- End

            if (!IsPostBack)
            {
                this.IntializeUIResources();
                BindData();
                outGetAllocationXML = "";
                outGetMgBulkUserXML = "";
            }
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
           // LblError.Text = "PageLoad: " + ex.StackTrace; ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("Page_Load:" + ex.Message);//ZD 100263

        }

    }

    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Intialize UI Resources
    /// <summary>
    /// Intializing UI Controls
    /// </summary>
    /// 
    private void IntializeUIResources()
    {
        this.DdlOperation.Attributes.Add("onchange", "JavaScript:setOprValue();");
        //this.DdlOperation.SelectedIndexChanged += new EventHandler(DdlOperation_SelectedIndexChanged);
    }


    #endregion

    #region User Defined Methods

    /// <summary>
    /// User Defined Methods 
    /// </summary>

    #region BindData
    /// <summary>
    /// User Defined Method - Building GetQueryStrings Structure
    /// </summary>

    private void BindData()
    {
        try
        {
            GetQueryStrings();
            XMLTransactions();
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "GetQueryStrings: " + ex.StackTrace; ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("BindData:" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region GetQueryStrings

    /// <summary>
    /// User Defined Method - Building GetQueryStrings Structure
    /// </summary>

    private void GetQueryStrings()
    {

        try
        {
            if (Request.QueryString["sb"] != null)
            {
                if (Request.QueryString["sb"].ToString().Trim() != "")
                {
                  
                    sortby.Value = Request.QueryString["sb"].ToString().Trim();
                }
            }
            else
            {
                
                sortby.Value = "2";

            }
            if (Request.QueryString["a"] != null)
            {
                if (Request.QueryString["a"].ToString().Trim() != "")
                {
                    
                    alphabet.Value =Request.QueryString["a"].ToString().Trim();
                }
            }
            else
                alphabet.Value = "A";
            if (Request.QueryString["pn"] != null)
            {
                if (Request.QueryString["pn"].ToString().Trim() != "")
                {
                   
                    pageno.Value = Request.QueryString["pn"].ToString().Trim();
                }
            }
            else
                pageno.Value = "1";

        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "QueryString: " + ex.StackTrace;//ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("GetQueryStrings:" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region BuildInXML
    /// <summary>
    /// User Defined Method - Building InXML Structure
    /// </summary>

    private void BuildInXML(String commandName)
    {
        try
        {
            inXML = "";
            switch (commandName)
            {
                case ("GetAllocation"):
                    inXML += "<login>";
                    inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inXML += "<sortBy>" + sortby.Value+ "</sortBy>";
                    inXML += "<alphabet>" + alphabet.Value + "</alphabet>";
                    inXML += "<pageNo>" + pageno.Value + "</pageNo>";
                    inXML += "</login>";
                    break;
                case ("GetManageBulkUsers"):
                    inXML += "<login>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inXML += "</login>";
                    break;
            }

        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "BuildInXML: " + ex.StackTrace;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("BuildInXML:" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region GetOutXML
    /// <summary>
    /// User Defined Method - Retrieving OutXML  from COM
    /// </summary>

    private void XMLTransactions()
    {
        XmlDocument xmlDocAlloc = null;
        XmlDocument xmlDocBulkUser = null;

        try
        {
            obj = new myVRMNet.NETFunctions();
            
            inXML = "";
            String cmdUser = "GetManageBulkUsers";
            BuildInXML(cmdUser);
            //FB 2027 Start
            outGetMgBulkUserXML = obj.CallMyVRMServer(cmdUser, inXML, Application["MyVRMServer_ConfigPath"].ToString());
            //outGetMgBulkUserXML = obj.CallCOM(cmdUser, inXML, Application["COM_ConfigPath"].ToString());
            //FB 2027 End
            if (outGetMgBulkUserXML.IndexOf("<error>") < 0)
            {
                xmlDocBulkUser = new XmlDocument();
                xmlDocBulkUser.LoadXml(outGetMgBulkUserXML);

                AllocationIncFile();

               
                xmlDocBulkUser = null;
            }
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "SendInXML: " + ex.StackTrace;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("XMLTransactions:" + ex.Message);//ZD 100263

        }
    }
    #endregion

    #region Included Method
    /// <summary>
    /// User Defined Method - Included File'inc/AllocationInc.asp' logic is implemented as method
    /// </summary>

    private void AllocationIncFile()
    {
        ArrayList ddlOperations = null;
        XmlDoc = new XmlDocument();

        try
        {
            
            if (Convert.ToString(Session["errMsg"]) != "")
            {
                if ((Convert.ToString(outGetAllocationXML).IndexOf("<error>") != 0) && outGetAllocationXML != "") //FB 2018
                {
                    Response.Write("<br><br><p align='center'><font size=4><b>" + Convert.ToString(outGetAllocationXML) + "<b></font></p>");
                    Response.End();
                }
                else
                {
                    LblError.Text = Convert.ToString(Session["errMsg"]);//FB 2018
                }
            }

            XmlBulkDoc = new XmlDocument();
            XmlBulkDoc.LoadXml(outGetMgBulkUserXML);

            if(XmlBulkDoc != null)
            {

                //if (XmlBulkDoc.DocumentElement.SelectNodes("getManageBulkUsers").Count > 0)
                //{
                    departments.Value = "";
                    nodes = XmlBulkDoc.DocumentElement.SelectNodes("departments/department");
                    foreach (XmlNode dNode in nodes)
                    {
                        departmentsCS = departmentsCS + dNode.SelectSingleNode("id").InnerText + "``" + dNode.SelectSingleNode("name").InnerText + "||"; // FB 1888
                        departments.Value += dNode.SelectSingleNode("id").InnerText + "``" + dNode.SelectSingleNode("name").InnerText + "||"; // FB 1888
                    }
                    timezonesIDCS = XmlBulkDoc.DocumentElement.SelectSingleNode("timezoneID").InnerText;
                    timezonesCS = "";
                    timezones.Value = "";
                    nodes = XmlBulkDoc.DocumentElement.SelectNodes("timezones/timezone");
                    /* Code Modified For FB 1453 - Start */
                    //foreach (XmlNode tNode in nodes)
                    //{
                    //    timezonesCS = timezonesCS + tNode.SelectSingleNode("timezoneID").InnerText + "``" + tNode.SelectSingleNode("timezoneName").InnerText + ";;";
                    //    timezones.Value += tNode.SelectSingleNode("timezoneID").InnerText + "``" + tNode.SelectSingleNode("timezoneName").InnerText + ";;";
                    //}
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    DataView dv;
                    DataTable dt;

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dtTZ;
                        dtTZ = ds.Tables[0];
                        dtTZ.Columns.Add(new DataColumn("TimeZoneOrder", typeof(Int32)));

                        foreach (DataRow drTZ in dtTZ.Rows)
                        {
                            String varTZ = drTZ["timezonename"].ToString();
                            String var1 = varTZ.Split(' ')[0];
                            String var2 = var1.Substring(4);
                            String var3 = var2.Split(')', ':')[0];
                            if (var3 == "")
                                drTZ["TimeZoneOrder"] = "0";
                            else
                                drTZ["TimeZoneOrder"] = var3;
                        }
                        dv = new DataView(dtTZ);
                        dv.Sort = "TimeZoneOrder Asc";
                        dt = dv.ToTable();
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow drNew in dt.Rows)
                            {
                                timezonesCS = timezonesCS + drNew["timezoneID"].ToString() + "``" + drNew["timezoneName"].ToString() + "||"; // FB 1888
                                timezones.Value += drNew["timezoneID"].ToString() + "``" + drNew["timezoneName"].ToString() + "||"; // FB 1888

                            }
                        }
                    }               
                    
                    /* Code Modified For FB 1453 - Start */
                    rolesCS = "";
                    roles.Value = "";
                    nodes = XmlBulkDoc.DocumentElement.SelectNodes("roles/role");
                    foreach(XmlNode rNode in nodes)
                    {
                        if (Session["UsrCrossAccess"] != null)
                        {
                            if (Session["UsrCrossAccess"].ToString() != "1" || Session["organizationID"].ToString() != "11")
                            {
                                if(rNode.SelectSingleNode("name").InnerText == "Site Administrator")
                                    continue;
                            }
                        }

                        rolesCS = rolesCS + rNode.SelectSingleNode("ID").InnerText + "``" + rNode.SelectSingleNode("name").InnerText + "||"; // FB 1888
						/* *** Code added for FB 1425 QA Bug -Start *** */
						if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
						{
                        if (rNode.SelectSingleNode("name").InnerText.Contains("Conferencing"))
                            roles.Value += rNode.SelectSingleNode("ID").InnerText + "``" + rNode.SelectSingleNode("name").InnerText.Replace("Conferencing", "Hearing") + "||"; // FB 1888
                        else
                            roles.Value += rNode.SelectSingleNode("ID").InnerText + "``" + obj.GetTranslatedText(rNode.SelectSingleNode("name").InnerText) + "||"; // FB 1888 //ZD 100288
                    }/* *** Code added for FB 1425 QA Bug -End *** */
                    else
                            roles.Value += rNode.SelectSingleNode("ID").InnerText + "``" + obj.GetTranslatedText(rNode.SelectSingleNode("name").InnerText) + "||";// FB 1888
                    }
                    languagesCS = "";
                    languages.Value = "";
                    nodes = XmlBulkDoc.DocumentElement.SelectNodes("languages/language");
                    //ZD 100288 Starts
                    foreach (XmlNode lNode in nodes)
                    {
                        languagesCS = languagesCS + lNode.SelectSingleNode("ID").InnerText + "``" + obj.GetTranslatedText(lNode.SelectSingleNode("name").InnerText) + "||"; // FB 1888
                        languages.Value += lNode.SelectSingleNode("ID").InnerText + "``" + obj.GetTranslatedText(lNode.SelectSingleNode("name").InnerText) + "||"; // FB 1888
                    }
                    //ZD 100288 End
                    bridgesCS = "";
                    bridges.Value = "";
                    nodes = XmlBulkDoc.DocumentElement.SelectNodes("bridges/bridge");
                    foreach (XmlNode bNode in nodes)
                    {
                        bridgesCS = bridgesCS + bNode.SelectSingleNode("ID").InnerText + "``" + bNode.SelectSingleNode("name").InnerText + "||";// FB 1888
                        bridges.Value += bNode.SelectSingleNode("ID").InnerText + "``" + bNode.SelectSingleNode("name").InnerText + "||"; // FB 1888
                    }
                    enableAVCS = "";
                    enableAV.Value = "";
                    enableAVCS = enableAVCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";// FB 1888 //FB 2940 //ZD 100288
                    enableAV.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";// FB 1888 //FB 2940
                    enableAVCS = enableAVCS + "0" + "``" + obj.GetTranslatedText("No") + "||"; // FB 1888 //FB 2940
                    enableAV.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";// FB 1888 //FB 2940

                    //FB 1599 - Start
                    enableDominoCS = "";
                    enableDomino.Value = "";
                    enableDominoCS = enableDominoCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940 
                    enableDomino.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940
                    enableDominoCS = enableDominoCS + "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940
                    enableDomino.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940

                    enableExchangeCS = "";
                    enableExchange.Value = "";
                    enableExchangeCS = enableExchangeCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940
                    enableExchange.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940
                    enableExchangeCS = enableExchangeCS + "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940
                    enableExchange.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940       
                    //FB 1599 - End
                    //FB 1979 - Start
                    enableMobileCS = "";
                    enableMobile.Value = "";
                    enableMobileCS = enableMobileCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940
                    enableMobile.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";//FB 2940
                    enableMobileCS = enableMobileCS + "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940
                    enableMobile.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";//FB 2940             
                    //FB 1979 - End

                    //ZD 101093 START
                    enableAVCS = "";
                    enableAO.Value = "";
                    enableAVCS = enableAVCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableAO.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableAVCS = enableAVCS + "0" + "``" + obj.GetTranslatedText("No") + "||";
                    enableAO.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";
                    //ZD 101093 END

                    dtformatCS = "";
                    dtformat.Value = "";
                    //ZD 101476 Starts
                    dtformatCS = dtformatCS + "MM/dd/yyyy" + "``" + "mm/dd/yyyy||";// FB 1888
                    dtformat.Value += "MM/dd/yyyy" + "``" + "mm/dd/yyyy||";// FB 1888
                    dtformatCS = dtformatCS + "dd/MM/yyyy" + "``" + "dd/mm/yyyy||";// FB 1888
                    dtformat.Value += "dd/MM/yyyy" + "``" + "dd/mm/yyyy||";// FB 1888
                    //ZD 101476 End

                    tformatCS = "";
                    tformat.Value = "";
                    tformatCS = tformatCS + "1" + "``" + "12 "+ obj.GetTranslatedText("hour") + " (01:00 PM)||";// FB 1888 //ZD 100288
                    tformat.Value += "1" + "``" + "12 " + obj.GetTranslatedText("hour") + " (01:00 PM)||";// FB 1888
                    tformatCS = tformatCS + "0" + "``" + "24 " + obj.GetTranslatedText("hour") + " (13:00)||";// FB 1888
                    tformat.Value += "0" + "``" + "24 " + obj.GetTranslatedText("hour") + " (13:00)||";// FB 1888

                    tzdisplayCS = "";
                    tzdisplay.Value = "";
                    tzdisplayCS = tzdisplayCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";// FB 1888
                    tzdisplay.Value += "1" + "``" + obj.GetTranslatedText("Yes") +"||"; // FB 1888
                    tzdisplayCS = tzdisplayCS + "0" + "``" + obj.GetTranslatedText("No") + "||";// FB 1888
                    tzdisplay.Value += "0" + "``" + obj.GetTranslatedText("No") + "||"; // FB 1888

                    //ZD 101122 - Start
                    enableAVWOCS = "";
                    enableAVWO.Value = "";
                    enableAVWOCS = enableAVWOCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableAVWO.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||"; 
                    enableAVWOCS = enableAVWOCS + "0" + "``" + obj.GetTranslatedText("No") + "||";
                    enableAVWO.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";

                    enableCateringWOCS = "";
                    enableCateringWO.Value = "";
                    enableCateringWOCS = enableCateringWOCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableCateringWO.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableCateringWOCS = enableCateringWOCS + "0" + "``" + obj.GetTranslatedText("No") + "||";
                    enableCateringWO.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";

                    enableFacilityWOCS = "";
                    enableFacilityWO.Value = "";
                    enableFacilityWOCS = enableFacilityWOCS + "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableFacilityWO.Value += "1" + "``" + obj.GetTranslatedText("Yes") + "||";
                    enableFacilityWOCS = enableFacilityWOCS + "0" + "``" + obj.GetTranslatedText("No") + "||";
                    enableFacilityWO.Value += "0" + "``" + obj.GetTranslatedText("No") + "||";
                    //ZD 101122 - End
                }
        }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "AllocationInc: " + ex.StackTrace;ZD 100263
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("AllocationIncFile:" + ex.Message);//ZD 100263


        }
    }
    #endregion

    #endregion

    #region Submit Click
    /// <summary>
    /// Event Handlers Methods 
    /// </summary>

    protected void Submit_Click(object sender, EventArgs e)
    {
        string[] uArr = null;
        string[] uInnArr = null;
        string comCmd = "";
        string[] deptary = null;
        string allUsers = "0";
        try 
        {
            //FB 2027 Start
            StringBuilder inXML = new StringBuilder();
            //inXML = "";
            //inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><users>";//Organization Module Fixes
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("<users>");

            if (SelectedUser.Value != "" || CheckAllAdUser_Org.Checked == true)
            {
                String[] delimiter = { "||" };//FB 1888
                uArr = SelectedUser.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                //uArr = SelectedUser.Value.Split(';');

                foreach (String u in uArr)
                {
                    if (u != "")
                    {
                        uInnArr = u.Split('`');

                        if (uInnArr.Length > 0)
                        {
                            if (uInnArr[0].ToString() != "11")
                            {
                                inXML.Append("<userID>" + uInnArr[0].ToString() + "</userID>");
                            }
                        }
                    }
                }

                inXML.Append("</users>");

                // ZD 101362 Start
                if (CheckAllAdUser_Org.Checked == true)
                {
                    allUsers = "1";
                    CheckAllAdUser_Org.Checked = false;
                }
                inXML.Append("<allusers>" + allUsers + "</allusers>");
                // ZD 101362 Ends

                if (DdlOperation.SelectedValue != "")
                {
                    inXML.Append("<type>"+ DdlOperation.SelectedValue +"</type>");
                    switch (DdlOperation.SelectedValue)
                    {
                        case "6":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
						    inXML.Append("<timeZoneID>"+Request.Form["OprValue"] +"</timeZoneID>");
                            break;
                        case "5":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            inXML.Append("<roleID>" + Request.Form["OprValue"] + "</roleID>");
                            break;
                        case "4":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            inXML.Append("<languageID>" + Request.Form["OprValue"] + "</languageID>");
                            break;
                        case "3":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            inXML.Append("<bridgeID>" + Request.Form["OprValue"] + "</bridgeID>");
                            break;
                        case "0":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            inXML.Append("<minutes>" + Request.Form["OprValue"] + "</minutes>");
                            break;
                        case "7":
                            comCmd = "SetBulkUserDelete";
                            break;
                        case "8":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            break;
                        case "2":
                            comCmd = "SetBulkUserUpdate"; // ZD 104204
                            inXML.Append("<expiryDate>" + myVRMNet.NETFunctions.GetDefaultDate(Request.Form["OprValue"]) + "</expiryDate>");//FB 1073
                            break;
                        case "1":
                            comCmd = "SetBulkUserUpdate";
                            if (Request.Form["OprValue"] != null) //FB 2164
                            {
                                deptary = Request.Form["OprValue"].Split(',');
                                if (deptary.Length > 0)
                                {
                                    inXML.Append("<departments>");
                                    foreach (String s in deptary)
                                        inXML.Append("<departmentID>" + s + "</departmentID>");
                                    inXML.Append("</departments>");
                                }
                            }
                            break;
                        case "9":
                        case "10":
                        case "11":
                        case "12":
                        case "13"://FB 1599
                        case "14"://FB 1599
                        case "15"://FB 1979
                        case "16"://ZD 101015
                        case "17"://ZD 101093
                        case "18"://ZD 101122
                        case "19":
                        case "20":
                            comCmd = "SetBulkUserUpdate";
                            inXML.Append("<newValue>" + Request.Form["OprValue"] + "</newValue>");
                            break;
                        case "21":
                            comCmd = "SetBulkUserUpdate"; // ZD 104850
                            if (Request.Form["OprValue"].Contains("'"))
                            {
                                string UserDomain = Request.Form["OprValue"];
                                UserDomain = UserDomain.Replace("'", "''");
                                inXML.Append("<UserDomain>" + UserDomain + "</UserDomain>");
                            }
                            else
                            {
                                inXML.Append("<UserDomain>" + Request.Form["OprValue"] + "</UserDomain>");
                            }
                            break;
   
                    }
                    inXML.Append("</login>");
                    if(comCmd == "")
                    {
                         //FB 1881 start
                         //outXml ="<error><errorCode>ASP</errorCode><message>Failed. This operation is not been implemented.</message><level>I</level></error>";
                         //if (outXml.IndexOf("<error>") > 0)
                         log.Trace("<error><errorCode>ASP</errorCode><message>Failed. This operation is not been implemented.</message><level>I</level></error>");
                         outXml  = obj.ShowSystemMessage();
                         throw new Exception(outXml);
                         //Response.End();
                         //FB 1881 end
			            
                    }
                    obj = new myVRMNet.NETFunctions();
                    //if(Int32.Parse(DdlOperation.SelectedValue) < 9 && Int32.Parse(DdlOperation.SelectedValue) != 0)
                    //    outXml = obj.CallCOM(comCmd,inXML.ToString(),Application["COM_ConfigPath"].ToString());
                    //else
                        outXml = obj.CallMyVRMServer(comCmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //FB 2027 End
                    if (outXml.IndexOf("<error>") >= 0) //FB 1599
                    {
                        LblError.Text = obj.ShowErrorMessage(outXml);
                        LblError.Visible = true;
                        return;
                    }
                    //ZD 101362
                    CheckAllAdUser.Checked = false;
                    CheckAllAdUser_Org.Checked = false;
                    
                    if(Session["errMsg"] != null)//Login Mangement
                    if (Session["errMsg"].ToString() != "")
                    {
                        if (outXml.IndexOf("<error>") > 0)
                            throw new Exception(outXml);
                    }

                    //inXML = ""; FB 2027
                    //LblError.Text = "Operation Sucessful!"; //FB 1881
					LblError.Text = obj.ShowSuccessMessage();//FB 1881
                    SelectedUser.Value = "";
                    //Response.Write("<script></script>"); Commented for FB 2050
                    
                }
            }
        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            LblError.Visible = true;
            //LblError.Text = "PageLoad: " + ex.StackTrace;
            LblError.Text = obj.ShowSystemMessage();
            log.Trace("Submit_Click:" + ex.Message);//ZD 100263
        }
    }


 #endregion
}
