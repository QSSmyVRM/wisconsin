﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Data;
using DevExpress.Web.ASPxGridView;

namespace ns_MyVRM
{
    public partial class ScheduleAssistantMain : System.Web.UI.Page
    {
        #region Protected Variables

        protected System.Web.UI.WebControls.Table tblUsrList;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfStartDate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfEndDate;
        
        #endregion

        #region private variable

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DateTime ConfStartDate = new DateTime();
        DateTime ConfEndDate = new DateTime();
        protected String format = "MM/dd/yyyy";
        string ConfStDate = "";
        string ConfEndDateVal = "";
        DataTable rptTable = new DataTable();
        string partyList = "", partyName = "";
        string ConfDate = DateTime.Now.Date.ToString();
        int tRowVal = 13;
        TableRow tRow = null;
        TableCell tCell = null;

        #endregion

        #region Constructor
        public ScheduleAssistantMain()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["stDate"] != null)
                {
                    if (Request.QueryString["stDate"] != "")
                        hdnConfStartDate.Value = Request.QueryString["stDate"].ToString();
                }
                if (Request.QueryString["enDate"] != null)
                {
                    if (Request.QueryString["enDate"] != "")
                        hdnConfEndDate.Value = Request.QueryString["enDate"].ToString();
                }

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if (ConfStDate != "")
                    ConfStartDate = Convert.ToDateTime(ConfStDate);

                if (ConfEndDateVal != "")
                    ConfEndDate = Convert.ToDateTime(ConfEndDateVal);

                if (ConfStDate != "")
                    ConfDate = ConfStDate.Split(' ')[0];

                if (Session["PartyList"] != null)
                {
                    if (Session["PartyList"].ToString() != "")
                        partyList = Session["PartyList"].ToString();
                }

            }
        }

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

               if(!IsPostBack)
               {
                   ScriptManager.RegisterStartupScript(this, this.GetType(), "DataLoadingDft", "DataLoadingnew(1);", true);
                   BindUserName();
               }

            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
            }
        }
        #endregion


        private void BindUserName()
        {
            try
            {
                string[] strParty = partyList.Split(';');

                if (strParty.Length > 13)
                    tRowVal = strParty.Length;

                for (int x = 0; x < tRowVal; x++)
                {
                    partyName = "";
                    tRow = new TableRow();
                    tCell = new TableCell();
                    tCell.Text = "";
                    tCell.Style.Add("height", "17px");

                    if (x < strParty.Length)
                        partyName = strParty[x].Split('|')[0];

                    Label lblTemp = new Label();
                    lblTemp.Text = partyName;
                    tCell.Controls.Add(lblTemp);

                    tRow.Cells.Add(tCell);
                    tblUsrList.Controls.Add(tRow);
                }
            }
            catch (Exception ex)
            {
                log.Trace("CreateTable " + ex.Message);
            }
        }
    }
}