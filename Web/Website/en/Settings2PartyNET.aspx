<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<script type="text/javascript" src="script/wincheck.js"></script>
<%--FB 3055-URL--%>
<script language="JavaScript" src="inc/functions.js"></script>
<link rel="stylesheet" type="text/css" href="css/fixedHeader.css" />
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<script type="text/javascript" src="script/errorList.js"></script>
<script runat="server">

        protected override void InitializeCulture()
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
</script>
<!-- JavaScript begin -->
<script language="JavaScript">
<!--

var	oldpartyno = 0;
var	newpartyno = 0;

var isRm = ( (parent.document.getElementById("CreateBy").value == "7") ? true : false ) ;
var isVD = ( (parent.document.getElementById("CreateBy").value == "2") || (parent.document.getElementById("CreateBy").value == "4") || (parent.document.getElementById("CreateBy").value == "9") ? true : false ) ; //ZD 100513
var isAD = ( (parent.document.getElementById("CreateBy").value == "6") ? true : false ) ;
var isVMR = false, isRoomVMR = false;//ZD 101561
var isPCCOnf= false; //FB 2819 

//FB 2376,2491 - start
if(parent.document.getElementById("isVMR"))
 if((parent.document.getElementById("isVMR").value == "1"))
    isVMR = true;
//ZD 101561
if(parent.document.getElementById("hdnVMRId"))
 if(parent.document.getElementById("hdnVMRId").value == "2")
    isRoomVMR = true;

//ZD 104133 - start
if(parent.document.getElementById("lstVMR"))
{
  var e = parent.document.getElementById("lstVMR");
  var VMRType=  e.options[e.selectedIndex].value;
  if(VMRType == 2)
    isRoomVMR = true;
}
//ZD 104133 - end

//FB 2376,2491 - End    
//FB 2819 Starts 
if(parent.document.getElementById("isPCCOnf"))
 if((parent.document.getElementById("isPCCOnf").value == "1"))
    isPCCOnf = true;
 //FB 2819 Ends
var isFuCreate = (isAD || isVD); //true;
var isImFu = (isAD || isVD || isRm); // && isCreate);;

var EnableSurvey = false;
var PCModule = false;
if("<%=Session["multisiloOrganizationID"]%>" != null && "<%=Session["multisiloOrganizationID"]%>" != "")
{
  if("<%=Session["EnableSurveySilo"]%>" == "1")
  {
    EnableSurvey = true;
  }
//  if("<%=Session["PCModuleSilo"]%>" == "1") //FB 2693
//  {
//    PCModule = true;
//  }
}
else 
{
    if("<%=Session["EnableSurvey"]%>" == "1")
    {
        EnableSurvey = true;
    }
//    if("<%=Session["PCModule"]%>" == "1") //FB 2693
//    {
//        PCModule = true;
//    }
}

//alert(isRm + " : " + isVD + " : " + isAD + " : " + isFuCreate + " : " + isImFu);
/*
var isImFu = ( ( (parent.document.frmSettings2.CreateBy.value == "1") || (parent.document.frmSettings2.CreateBy.value == "3") || (parent.document.frmSettings2.CreateBy.value == "-1") ) ? true : false ) ;

if (parent.document.frmSettings2.ConfID)
	var isCreate = ( (parent.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
else	// template
	var isCreate = true ;

var isFu = ( (parent.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
var isRm = ( ((parent.document.frmSettings2.CreateBy.value == "2") || (parent.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

var isFuCreate = (isFu && isCreate)
var isRmCreate = (isRm && isCreate)
*/
function deleteBlankLine(linenum, str)
{	
	var newstr = "";
	
	strary = str.split("||");//FB 1888
	//alert(strary.length);
	for (var i=linenum; i < strary.length-1; i++) {
		newstr += strary[i] + "||";//FB 1888
	}
	return newstr;
}


function addBlankLine(linenum, str)
{
	var newstr = str;
    
	for (var i=0; i < linenum; i++) {
		//newstr = ( isRm ? ",,,,0,1,0,0,0,1,,,0,,,;" : ",,,,1,0,0,0,0,1,,,0,,,;") + newstr;
		if (isRm)
			newstr = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0!!0!!1!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347
            //newstr = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347 //ALLDEV-814
			//newstr = ",,,,0,1,0,1,0,1,,,0,,,;" + newstr;
		if (isVD)
		    //newstr = "!!!!!!!!1!!0!!0!!1!!0!!0!!!!!!0!!!!!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347
            newstr = "!!!!!!!!1!!0!!0!!1!!0!!0!!!!!!0!!!!!!0!!0!!0!!0!!0!!1!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347 //ALLDEV-814
		    //newstr = ",,,,1,0,0,1,0,0,,,0,,,;" + newstr;
	    if (isVD)
		    //newstr = "!!!!!!!!1!!0!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347
            newstr = "!!!!!!!!1!!0!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0!!0!!1!!0||" + newstr;//FB 1888 //FB 2348 //FB 2347 //ALLDEV-814
		    //newstr = ",,,,1,0,0,1,0,1,,,0,,,;" + newstr;
	}
	return newstr;
}


function assemble(ary, endstr)
{
	var newstr = "";
	for (var i=0; i < ary.length-1; i++) {
		newstr += ary[i] + endstr;
	}
	newstr += ary[ary.length-1];
	return newstr;
}


function checkNewParty(fncb, lncb, emlcb)
{
	if ((Trim(fncb.value) == "[<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>]") && (Trim(lncb.value) == "[<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>]") && (Trim(emlcb.value) == ""))
		return true;
	if (Trim(fncb.value) == "[<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>]") {
		alert("<asp:Literal Text='<%$ Resources:WebResources, ReqAccFName%>' runat='server'></asp:Literal>")
		fncb.focus();
		return false;
	}
	/*else{ //FB 1888
	if(checkInvalidChar(fncb.value) == false){
	return false;
	}
	}*/
	
	
	if (Trim(lncb.value) == "[<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>]") {
		alert("<asp:Literal Text='<%$ Resources:WebResources, ReqAccLName%>' runat='server'></asp:Literal>")
		lncb.focus();
		return false;
	}
	/*else{ //FB 1888
		if(checkInvalidChar(lncb.value) == false){
			return false;
		}
	}*/
	
	if (Trim(emlcb.value) == "") {
		alert("<asp:Literal Text='<%$ Resources:WebResources, ReqAccEmail%>' runat='server'></asp:Literal>")
		emlcb.focus();
		return false;
	}
	else{
		if(checkInvalidChar(emlcb.value) == false){
			return false;
		}
	}
	//alert(checkEmail(Trim(emlcb.value)));
	if (checkemail(Trim(emlcb.value)))
		return true;
	else {
		alert("<asp:Literal Text='<%$ Resources:WebResources, ReqAccEmail%>' runat='server'></asp:Literal>");
		emlcb.focus();
		return false;
	}
}


function bfrRefresh()
{
    
	partysinfo = parent.document.getElementById("txtPartysInfo").value;	
	eno = -1;
	cornew = 0;
	partysinfo = deleteBlankLine(newpartyno, partysinfo);
    var pcParty = "0";
    var vmrParty = "0";
    var infoParty = "0";
//=== need add: check input valid.
//=== if above check is wrong, then return false cause two calling function break.
	for (i=0; i<newpartyno; i++) {//FB 1888 - Starts
		//alert(document.frmSettings2party.elements[eno+1].value + "," + document.frmSettings2party.elements[eno+2].value + "," + document.frmSettings2party.elements[eno+3].value);
		willContinue = checkNewParty(document.frmSettings2party.elements[eno+1], document.frmSettings2party.elements[eno+2], document.frmSettings2party.elements[eno+3]);
		if (!willContinue)
			return false;
		infoParty = "";
		pcParty = "0";
		vmrParty = "0";
		if ( (document.frmSettings2party.elements[eno+3].value != "") && (partysinfo.indexOf("!!" + document.frmSettings2party.elements[eno+3].value + "!!") == -1) ) {
			infoParty = "new!!" + document.frmSettings2party.elements[++eno].value + "!!" + document.frmSettings2party.elements[++eno].value + "!!" +  
							document.frmSettings2party.elements[++eno].value + "!!" +

							( document.frmSettings2party.elements[++eno].checked ? "1" : "0") + "!!" +  // : (isRm ? "0" : (document.frmSettings2party.elements[++eno].checked ? "1" : "0") ) ) + "," +
							( document.frmSettings2party.elements[++eno].checked ? "1" : "0") + "!!" ; //: (document.frmSettings2party.elements[++eno].checked ? "1" : "0") ) ) + "," + 
							
							if(document.frmSettings2party.elements[++eno].checked)
							    pcParty = "1";
							if(document.frmSettings2party.elements[++eno].checked)
							    vmrParty = "1";
							
			 infoParty +=   ( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
			                ( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" +
							//FB 2348 Start
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + //FB 2348
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							//pcParty +"!!"+ vmrParty +"!!0||" ;//FB 1888 //FB 2347 //FB 2550 - Public Party //ALLDEV-814
                            pcParty +"!!"+ vmrParty +"!!0!!" + //FB 1888 //FB 2347 //FB 2550 - Public Party   //ALLDEV-814
                            document.frmSettings2party.elements[++eno].value + "!!" +  //ALLDEV-814
                            document.frmSettings2party.elements[++eno].value + "||"; //ALLDEV-814
							//FB 2348 End
							//"-1,,,;" + partysinfo;
			
			partysinfo = infoParty + partysinfo;
							
			cornew ++;
			eno += 0; //FB 2347
			
		} else {
			eno += ( ( isFuCreate || isRm) ? 14 : 15);
			alert("<asp:Literal Text='<%$ Resources:WebResources, RSUniqueEmail%>' runat='server'></asp:Literal>" );
			return false;
		}
	}
//alert("after New Party=" + partysinfo);

	partysinfo = addBlankLine(newpartyno-cornew, partysinfo);
//alert("after addBlankLine=" + partysinfo);

	partysary = partysinfo.split("||");//FB 1888
	//alert(oldpartyno);
	
	for (i=0; i<oldpartyno; i++) {
		pemail = (document.frmSettings2party.elements[eno+1].name).substr(1, (document.frmSettings2party.elements[eno+1].name).length-1)
		for (j=0; j < partysary.length-1; j++) {
			partyary = partysary[j].split("!!");//FB 1888
			if (partyary[3] + "" + partyary[20] ==pemail) {
 				partyary[4] = (document.frmSettings2party.elements[eno+1].checked) ? "1" : "0";
				partyary[5] = (document.frmSettings2party.elements[eno+2].checked) ? "1" : "0";
				partyary[17] = (document.frmSettings2party.elements[eno+3].checked) ? "1" : "0";//FB 2347 2376
				partyary[18] = (document.frmSettings2party.elements[eno+4].checked) ? "1" : "0";//FB 2347
				partyary[6] = (document.frmSettings2party.elements[eno+5].checked) ? "1" : "0";//FB 2347
				partyary[7] = (document.frmSettings2party.elements[eno+6].checked) ? "1" : "0";//FB 2347
				
//				partyary[4] = ( isFuCreate ? (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") : (isRm ? "0" : (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") ) );
//				partyary[5] = ( isFuCreate ? "0" : (isRm ? (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") : (document.frmSettings2party.elements[eno+2].checked ? "1" : "0") ) );
				
//				partyary[6] = (document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+2) : (eno+3)].checked) ? "1" : "0";
//				partyary[7] = (document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+3) : (eno+4)].checked) ? "1" : "0";
				partyary[8] = (document.frmSettings2party.elements[(!isRm) ? (eno+7) : (eno+6)].checked) ? "1" : "0";//FB 2376
				partyary[9] = (document.frmSettings2party.elements[(isVD) ? (eno+8) : (eno+5)].checked) ? "1" : "0";//FB 2376
				partyary[10] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+7) : (eno+6)].value;
				partyary[11] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+8) : (eno+7)].value;
				//alert(partyary[12]);
				partyary[12] = partyary[12]; //document.frmSettings2party.elements[(isVD) ? (eno+9) : (eno+8)].value;
				partyary[13] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+10) : (eno+9)].value;
				partyary[14] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+10) : (eno+9)].value;
				partyary[15] = "0"; //document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+11) : (eno+12)].value;
				partyary[16] = (document.frmSettings2party.elements[eno+15].checked) ? "1" : "0";//FB 2348
				//FB 2376
				if(!isVMR && !isPCCOnf) //FB 2819
				    partyary[18] = "0";
				//ALLDEV-814 Starts
				partyary[19] = partyary[19];
                partyary[20] = partyary[20];	
                partyary[21] = partyary[21];	
				//ALLDEV-814 Ends			
				partysary[j] = assemble(partyary, "!!");//FB 1888
				//alert(partysary[j]);
			}
		}
/*		if (isRm)
		    eno += 12;
		if (isAD)
		    eno += 12;
		if (isVD)
		    eno += 12;
*/
		eno += 17; //FB 2347 FB 2348 //ALLDEV-814
	}

	partysinfo = assemble(partysary, "||");//FB 1888
//alert("after Old Party, partysary=" + partysary);
	parent.document.getElementById("txtPartysInfo").value = partysinfo;	
	
	return true;
}

function nFocus(cb)
{
	if ( (cb.value=="[<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>]") || (cb.value=="[<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>]") || (cb.value=="[<asp:Literal Text='<%$ Resources:WebResources, EnterEmail%>' runat='server'></asp:Literal>]") ) {
		cb.value = "";
	}
}


function nBlur(cb)
{
	if ( ((cb.name).indexOf("NewParticipantFirstName") != -1) && (cb.value=="") ) {
		cb.value = "[<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>]";
	}
	
	if ( ((cb.name).indexOf("NewParticipantLastName") != -1) && (cb.value=="") ) {
		cb.value = "[<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>]";
	}
	if ( ((cb.name).indexOf("NewParticipantEmail") != -1) && (cb.value=="") ) {
		cb.value = "[<asp:Literal Text='<%$ Resources:WebResources, EnterEmail%>' runat='server'></asp:Literal>]";
	}
}

function addExternalParty(args)
{
	//ZD 101254 Starts
    var  isExternalParty = false;
    if(window.parent.document.location.href.toLowerCase().indexOf("expressconference") > 0 && (args == 1 || args == 2))
	{
        if(args == 1 && bfrRefresh())
        	isExternalParty = true;

	    if(args == 2 || isExternalParty) 
		    window.parent.document.getElementById("btnSample").click();
	}
	//ZD 101254 End
}
function addExternalParty2(arg1, arg2) // ZD 101253
{
    if(window.parent.document.location.href.toLowerCase().indexOf("expressconference") > 0 && arg1 == 1)
	{
        if(bfrRefresh())
	        window.parent.document.getElementById("btnSample").click();
        else
        {
            if(navigator.userAgent.indexOf('Trident') > -1)
            {
                if(arg2.parentNode.parentNode.childNodes[3].childNodes[0] != null)
                  arg2.parentNode.parentNode.childNodes[3].childNodes[0].checked = true;
            }
            else
            {
             	if(arg2.parentNode.parentNode.childNodes[7].childNodes[0] != null)
                	arg2.parentNode.parentNode.childNodes[7].childNodes[0].checked = true;
            }
        }
	}
}
function addExternalParty3(arg1, arg2, arg3) // ZD 101253
{
    //ZD 104256 - Start
    if(window.parent.document.getElementById("hdnAudioId") != null)
    {
        if(arg2.indexOf("new") > 0)
            window.parent.document.getElementById("hdnAudioId").value = arg3;
        else    
            window.parent.document.getElementById("hdnAudioId").value = arg2;
    }
    //ZD 104256 - End
	//ZD 101254 Starts
    var  isExternalParty = false;

    if(window.parent.document.location.href.toLowerCase().indexOf("expressconference") > 0 && arg1 == 1)
	{
        if(bfrRefresh())
        	isExternalParty = true;

	    if(isExternalParty) 
		    window.parent.document.getElementById("btnSample").click();
	}
}

function deleteThisParty(thisparty)
{
    var eno = -1;//ZD 101254
    var removeExternalGrid = false;//ZD 100834
	var needRemove = confirm("<asp:Literal Text='<%$ Resources:WebResources, RemoveParty%>' runat='server'></asp:Literal>")
	if (needRemove == true) {
		if (thisparty != "") {
			willContinue = true; //bfrRefresh();  FB Case 727
            if (willContinue) {
				partysinfo = window.parent.document.getElementById("txtPartysInfo").value; // FB 2050
				
				//FB 1966 - Starts
				var pInfoSplit = partysinfo.split("||")
				var strpartyInfo = ""
				for(var p=0;p< pInfoSplit.length; p++)
				{
                    //ALLDEV-814
                    var partyary = pInfoSplit[p].split("!!"); 
				    //var party = pInfoSplit[p].indexOf(thisparty);
				    if(partyary[3] + "" + partyary[20] != thisparty)
				    {
						//ZD 101254 Starts
                        if(window.parent.document.location.href.toLowerCase().indexOf("expressconference") > 0 && newpartyno >= 1)
                        {
                            if(!checkNewParty(document.frmSettings2party.elements[eno+1], document.frmSettings2party.elements[eno+2], document.frmSettings2party.elements[eno+3]))
                                return false;
                        }
						//ZD 101254 End
				        if(strpartyInfo == "")
				            strpartyInfo = pInfoSplit[p];
				        else
				            strpartyInfo = strpartyInfo + "||" + pInfoSplit[p];
				    }
                    else //ZD 100834
                    {
                        var removeParty = pInfoSplit[p];
                        var removeParty1 = removeParty.split('!!');
                        if(removeParty1[4] == '1')
                            removeExternalGrid = true;
                    }
				}
				parent.document.getElementById("txtPartysInfo").value = strpartyInfo;
				if(window.parent.document.location.href.toLowerCase().indexOf("expressconference") < 0) //FB 2266
				    parent.document.getElementById("hdnParty").value = strpartyInfo; //FB 2018
				/*if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 )//FB 1888
					parent.document.getElementById("txtPartysInfo").value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+1, partysinfo.length);//FB 1888
			    */
				//FB 1966 - End
				//history.go(0); // FB 2050
				parent.refreshIframe(); // FB 2050
                if(removeExternalGrid)  //ZD 100834
                    addExternalParty(2);//ZD 101254
			}
		} else {
				partysinfo = window.parent.document.getElementById("txtPartysInfo").value; // FB 2050
				
				if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 )//FB 1888
					parent.document.getElementById("txtPartysInfo").value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+2, partysinfo.length);
			
				//history.go(0); // FB 2050
				parent.refreshIframe(); // FB 2050
		}
	}
}


function frmSettings2party_Validator()
{
	return true;
}

//-->
</script>
<!-- JavaScript finish -->
<!-- JavaScript begin -->
<script language="JavaScript">
<!--
    partysinfo = parent.document.getElementById("txtPartysInfo").value;
    
    _d = document;
    var mt = "";
    //ZD 101344
	var varFN = "<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>";
    var varLN = "<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>";
    var varLoN = "<asp:Literal Text='<%$ Resources:WebResources, EmailSearch_LoginName%>' runat='server'></asp:Literal>";
    var varEmail = "<asp:Literal Text='<%$ Resources:WebResources, Email%>' runat='server'></asp:Literal>";
    var varSelected = "<asp:Literal Text='<%$ Resources:WebResources, Selected%>' runat='server'></asp:Literal>";
    var varExternalAttendees = "<asp:Literal Text='<%$ Resources:WebResources, ExternalAttendees%>' runat='server'></asp:Literal>";
    var varRoomAttendees = "<asp:Literal Text='<%$ Resources:WebResources, RoomAttendees%>' runat='server'></asp:Literal>";
    var varNotify = "<asp:Literal Text='<%$ Resources:WebResources, EditInventory_Notify%>' runat='server'></asp:Literal>";
    var varAudio = "<asp:Literal Text='<%$ Resources:WebResources, Audio%>' runat='server'></asp:Literal>";
    var varVideo = "<asp:Literal Text='<%$ Resources:WebResources, Video%>' runat='server'></asp:Literal>";
    var varDelete = "<asp:Literal Text='<%$ Resources:WebResources, Delete%>' runat='server'></asp:Literal>";
    var varName = "<asp:Literal Text='<%$ Resources:WebResources, Name%>' runat='server'></asp:Literal>";
    var varSurvey = "<asp:Literal Text='<%$ Resources:WebResources, mainadministrator_Survey%>' runat='server'></asp:Literal>";
    var varPC = "<asp:Literal Text='<%$ Resources:WebResources, PCAttendee%>' runat='server'></asp:Literal>";
    var varVMR = "<asp:Literal Text='<%$ Resources:WebResources, ConferenceSetup_VMR%>' runat='server'></asp:Literal>";
    var varRemove = "<asp:Literal Text='<%$ Resources:WebResources, Remove%>' runat='server'></asp:Literal>";

    mt += "<div align='center'>";
    mt += "  <form name='frmSettings2party' method='POST' action=''>"
    mt += "  <table id='tblParticipants' border='0' cellpadding='0' cellspacing='0' width='100%'>"; // ZD 101253
    //	alert(partysinfo)

    if (isRm || isAD || isVD) {
        mt += "<thead><tr class='tableHeader'>";
        if (parent.document.location.href.toLowerCase().indexOf("expressconference") <= 0)
            mt += " <td align='center' class='tableHeader'>"+ varDelete +"</td>";
        mt += " <td align='center' class='tableHeader' width='20%'>"+ varName +"</td>";
        mt += " <td align='center' class='tableHeader'>"+ varEmail +"</td>";
        //FB 1779
        //ZD 100834
        //if (parent.document.location.href.toLowerCase().indexOf("expressconference") < 0)
        {
			//ZD 101561
            //if (!isRm && !isVMR && !isPCCOnf)//FB 2448      //FB 2819
            if ((!isRm && !isVMR && !isPCCOnf) || isRoomVMR)//FB 2448      //FB 2819
                mt += " <td align='center' class='tableHeader'>"+ varExternalAttendees +"</td>";
            mt += " <td id='tdRoomAttendee' align='center' class='tableHeader'>"+ varRoomAttendees +"</td>"; // ZD 102425
            //FB 2347 Start
            if (PCModule)
                mt += " <td align='center' class='tableHeader'>"+ varPC +"</td>";
            else
                mt += " <td align='center' style='display: none' class='tableHeader'>"+ varPC +"</td>";

            //FB 2347 End 
            if (isVMR)//FB 2376
                mt += " <td align='center' class='tableHeader'>"+ varVMR +"</td>";
            mt += " <td align='center' class='tableHeader'>CC</td>";
            mt += " <td align='center' class='tableHeader'>"+ varNotify +"</td>";
            if (isVD)
                mt += " <td align='center' class='tableHeader'>"+ varVideo +"</td>";
            else //FB 1760
                mt += " <td width='0%' style='display: none' class='tableHeader'>"+ varVideo +"</td>";
            if (isAD || isVD)
                mt += " <td align='center' class='tableHeader'>"+ varAudio +"</td>";
            //FB 2348 Start

            if (EnableSurvey) {
                mt += " <td align='center' class='tableHeader'>"+ varSurvey +"</td>";
            }
            else {
                mt += " <td align='center' style='display: none' class='tableHeader'>"+ varSurvey +"</td>";
            }
            //FB 2348 End
            if (parent.document.location.href.toLowerCase().indexOf("expressconference") > 0) {
                mt += " <td align='center' class='tableHeader' width='5%'>"+ varDelete +"</td>";   //ZD 100425
            }
        }  //FB 1779
        mt += "</tr></thead>";
    }
    
    partysary = partysinfo.split("||"); //FB 1888
    for (var i = 0; i < partysary.length - 1; i++) {
        tdbgcolor = (i % 2 == 0) ? "#e1e1e1" : "#e1e1e1";

        partyary = partysary[i].split("!!"); //FB 1888
        if (partyary[2] == " ") //FB 2023//FB 2388
            partyary[2] = "";
        //alert(partyary);
        partyemail = partyary[3];
        //alert(partyary);
        if (partyemail == "") {
            newpartyno++;
            mt += "    <tr class='tableBody'>"
            if (parent.document.location.href.toLowerCase().indexOf("expressconference") <= 0) {
                mt += "      <td align='center' width='3%'>"
                mt += "        <a href='javascript:return false;' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\");return false;'><img border='0' src='image/btn_delete.gif' alt='Delete' WIDTH='16' HEIGHT='16' style='cursor:pointer;' title='" + varDelete + "'></a>" //ZD 100420 //ALLDEV-833
                mt += "      </td>"
            }
            mt += "      <td align='center' width='28%'>"
            mt += "        <input id='addPartyFname' type='text' name='NewParticipantFirstName" + newpartyno + "' size='10' value='[<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' onchange='javascript:chkLimit(this, 254);'  class='altText'>" //FB 1888
            mt += "        <input id='addPartyLname' type='text' name='NewParticipantLastName" + newpartyno + "' size='10' value='[<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' onchange='javascript:chkLimit(this, 254);' class='altText'>" //FB 1888
            mt += "      </td>"
            mt += "      <td align='center' width='27%'>"
            mt += "        <input type='text' name='NewParticipantEmail" + newpartyno + "' value='[<asp:Literal Text='<%$ Resources:WebResources, EnterEmail%>' runat='server'></asp:Literal>]'  onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' size='15' class='altText'>" //fogbugz case 482
            mt += "      </td>"

            //mt += ( (isFuCreate || isRm) ? "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='1'" + ( ( (partyary[4]=="1") || (partyary[5]=="1") ) ? " checked" : "") + "></td>" : "" );
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='1'" + ((partyary[4]=="1") ? " checked" : "") + "></td>" );
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='2'" + ((partyary[5]=="1") ? " checked" : "") + "></td>" );
            if (isRm || isVMR)//FB 2448
            {
				//ZD 101561
                //partyary[18] = "1"; //FB 2448
                if (isRoomVMR)
                    disp = "";
                else
                    disp = "none";
            }
            else
                disp = "";

            if (isPCCOnf) //FB 2819
            {
                partyary[5] = "1";
                disp = "none";
            }

            //FB 1779 - START 
            //ZD 100834
            /*if(parent.document.location.href.toLowerCase().indexOf("expressconference") > 0)
            {
            dispNew = "none";
            disp = dispNew;
            }
            else*/
            dispNew = "";
            //FB 1779 - End

            mt += "      <td align='center' width='3%' style='display:" + disp + "'><input type='radio' onclick='javascript:return addExternalParty2(1,this);'  name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[4] == "1") ? " checked" : "") + "></td>"
            /* Code Modified For FB 1456 - Start */

            if (isVMR == false && partyary[18] == "1") {
                partyary[5] = "1";
                partyary[18] = "0";
            }

            if (isRm) /* FB 1779 - End */
                mt += "      <td align='center' width='3%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[5] == "1") ? " checked" : " checked") + "></td>"
            else
                mt += "      <td align='center' width='3%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[5] == "1") ? " checked" : "") + "></td>"
            /* Code Modified For FB 1456 - End */
            //FB 2347 Start
            if (PCModule) {
                mt += "      <td align='center' width='3%' style='display:" + dispNew + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[17] == "1") ? " checked" : "") + "></td>"
            }
            else {
                mt += "      <td align='center' style='display: none' width='3%'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[17] == "1") ? " checked" : "") + "></td>"
            }
            //FB 2347 End    
            if (isVMR) /* FB 2376 - End */
                mt += "      <td align='center' width='3%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[18] == "1") ? " checked" : "") + "></td>"
            else
                mt += "      <td align='center' width='3%'  style='display:none;'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[18] == "1") ? " checked" : "") + "></td>"

            mt += "      <td align='center' width='3%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[6] == "1") ? " checked" : "") + "></td>"
            mt += "      <td align='center' width='5%' style='display:" + dispNew + "'><input type='checkbox' name='NewNotifyParticipant" + newpartyno + "' value='1'" + ((partyary[7] == "1") ? " checked" : "") + "></td>"
            //FB 1760 - Start
            if (!isRm) {
                if (!isVD) {
                    mt += "<td align='centre' style='display: none'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + "' value='0'" + ((partyary[8] == "1") ? "" : "") + "></td>";
                    mt += "<td align='centre' width='10%'style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + "' value='0'" + ((partyary[9] == "1") ? "checked" : "checked") + "></td>";
                }
                if (isVD) {
                    mt += "<td align='centre' width='5%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + "' value='0'" + ((partyary[8] == "1") ? "checked" : "checked") + "></td>";
                    mt += "<td align='centre' width='5%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty3(\" " + partyary[4] + " \",\" " + partyary[0] + " \", \" " + partyary[3] + " \");' name='3" + partyary[3] + "' value='0'" + ((partyary[9] == "1") ? "checked" : "") + "></td>"; // ZD 101253
                }
            } /* FB 1779 - End */
            else {
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8] == "1") ? "" : "") + "></td>";
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9] == "1") ? "" : "") + "></td>";
            }
            //FB 1760 - End
            mt += "      <input type='hidden' name='NewInterfaceTypeParticipant" + newpartyno + "' value='" + partyary[10] + "'>";
            mt += "      <input type='hidden' name='NewConnectionTypeParticipant" + newpartyno + "' value='" + partyary[11] + "'>";
            mt += "      <input type='hidden' name='NewSourceParticipant" + newpartyno + "' value='" + partyary[12] + "'>";
            mt += "      <input type='hidden' name='NewIPISDNAddressParticipant" + newpartyno + "' value='" + partyary[13] + "'>";
            mt += "      <input type='hidden' name='NewVideoEquipmentParticipant" + newpartyno + "' value='" + partyary[14] + "'>";
            mt += "      <input type='hidden' name='NewLineRateParticipant" + newpartyno + "' value='" + partyary[15] + "'>";
            mt += "      <input type='hidden' name='NewProfileID" + newpartyno + "' value='" + partyary[20] + "'>"; //ALLDEV-814
            mt += "      <input type='hidden' name='NewUID" + newpartyno + "' value='" + partyary[21] + "'>"; //ALLDEV-814
            //FB 2348 Start

            if (EnableSurvey) {
                mt += "<td align='center' width='5%' style='display:" + dispNew + "'><input type='checkbox' name='NewsurveyParticipant" + newpartyno + "' value='0'" + ((partyary[16] == "1") ? " checked" : "") + "></td>";
            }
            else {
                mt += "<td align='center' width='5%' style='display:none'><input type='checkbox' name='NewsurveyParticipant" + newpartyno + "' value='0'" + ((partyary[16] == "1") ? " checked" : "") + "></td>";
            }
            //FB 2348 End
            if (parent.document.location.href.toLowerCase().indexOf("expressconference") > 0) {
                mt += "<td align='center' class ='tableBody2' width='5%'><a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\");return false;' class='lblError'>" + varRemove + "</a></td>"; //ZD 100834 //ALLDEV-833
            }
            mt += "    </tr>"

        } else {
            if (partyary[3].length > 18) {
                strLongEmail = partyary[3].substr(0, 17) + "..."
            }
            else {
                strLongEmail = partyary[3]
            }

            if (partyary[2] == "") //FB 2023
                strLongEmail = "";

            oldpartyno++;
            mt += "    <tr class='tableBody' height='10px'>"
            if (parent.document.location.href.toLowerCase().indexOf("expressconference") <= 0) {
                mt += "      <td align='center' width='3%'>" + " <a href='javascript:return false;' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + partyary[20] + "\");return false;'><img border='0' src='image/btn_delete.gif' alt='Delete' WIDTH='16' HEIGHT='16' style='cursor:pointer;' title='" + varDelete + "'></a></td>"; //ALLDEV-833
            }  //ZD 100420
            mt += "      <td align='center' class ='tableBody' width='28%'>" + partyary[1].replace("++", ",") + " " + partyary[2].replace("++", ",") + "</td>" //FB 1640
            mt += "      <td align='center' class ='tableBody' width='27%' title='" + strLongEmail + "'>" + strLongEmail + "</td>"
            //mt += "      <td align='center' width='27%' bgcolor='" + tdbgcolor + "'>" + partyary[3] + "</td>"
            //mt += ( (isFuCreate || isRm) ? "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='1'" + ( ( (partyary[4]=="1") || (partyary[5]=="1") ) ? " checked" : "") + "></td>" : "");
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='1'" + ((partyary[4]=="1") ? " checked" : "") + "></td>" );
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='2'" + ((partyary[5]=="1") ? " checked" : "") + "></td>" );
            if (isRm || isVMR)//FB 2448
            {
				//ZD 101561
                if (isRoomVMR)
                    disp = "";
                else
                    disp = "none";
                //partyary[18] = "1"; //FB 2647 (for External attendee as VMR attendee)
            }
            else
                disp = "";

            if (isPCCOnf) { //FB 2819
                disp = "none";
                partyary[5] = "1";
            }

            //FB 1779 - START
            //ZD 100834
            /*if(parent.document.location.href.toLowerCase().indexOf("expressconference") > 0)
            {
            dispNew = "none";
            disp = dispNew;
            }
            else*/
            dispNew = "";
            //FB 1779 - End

            mt += "<td align='center' width='7%' style='display:" + disp + "'><input type='radio' onclick='javascript:return addExternalParty2(1,this);' name='1" + partyary[3] + partyary[20] + "' value='1'" + ((partyary[4] == "1") ? " checked" : "") + "></td>";
            /* Code Modified For FB 1456 - Start */

            if (isVMR == false && partyary[18] == "1") {
                partyary[5] = "1";
                partyary[18] = "0";
            }

            //FB 2647 Starts
            if (isVMR == true && partyary[5] == "1") //This is for room attndee
            {
                partyary[18] = "0";
            }
            //FB 2647 Ends
			//ZD 101561 Starts
            if (isVMR == true && !isRoomVMR && partyary[4] == "1") //This is for room attndee
            {
                partyary[4] = "0";
                partyary[5] = "1";
            }
			//ZD 101561 End
            if (isRm) /* FB 1779 - Start */
                mt += "<td align='center' width='7%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='1" + partyary[3] + partyary[20] + "' value='2'" + ((partyary[5] == "1") ? " checked" : " checked") + "></td>";
            else
                mt += "<td align='center' width='7%' style='display:" + dispNew + "' ><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='1" + partyary[3] + partyary[20] + "' value='2'" + ((partyary[5] == "1") ? " checked" : "") + "></td>";
            /* Code Modified For FB 1456 - End */
            //FB 2347 Start
            if (PCModule) {
                mt += "<td align='center' width='7%' style='display:" + dispNew + "'><input type='radio' name='1" + partyary[3] + partyary[20] + "' value='4'" + ((partyary[17] == "1") ? " checked" : "") + "></td>";
            }
            else {
                mt += "<td align='center' width='7%' style='display:none'><input type='radio' name='1" + partyary[3] + partyary[20] + "' value='4'" + ((partyary[17] == "1") ? " checked" : "") + "></td>";
            }

            if (isVMR) /* FB 2376 */
                mt += "      <td align='center' width='7%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='1" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[18] == "1") ? " checked" : "") + "></td>"
            else
                mt += "      <td align='center' width='7%' style='display:none;'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='1" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[18] == "1") ? " checked" : "") + "></td>"

            //FB 2347 End             
            mt += "<td align='center' width='7%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='1" + partyary[3] + partyary[20] + "' value='3'" + ((partyary[6] == "1") ? " checked" : "") + "></td>";
            mt += "<td align='center' width='5%' style='display:" + dispNew + "'><input type='checkbox' name='2" + partyary[3] + partyary[20] + "' value='1'" + ((partyary[7] == "1") ? " checked" : "") + "></td>"
            //FB 1760 - Start
            if (!isRm) {
                if (!isVD) {
                    mt += "<td align='centre' style='display: none'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[8] == "1") ? "" : "") + "></td>";
                    mt += "<td align='centre' width='10%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[9] == "1") ? "checked" : "checked") + "></td>"; //ZD 101254
                }
                if (isVD) {
                    mt += "<td align='centre' width='5%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty(\" " + partyary[4] + " \");' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[8] == "1") ? "checked" : "checked") + "></td>";
                    mt += "<td align='centre' width='5%' style='display:" + dispNew + "'><input type='radio' onclick='javascript:return addExternalParty3(\" " + partyary[4] + " \",\" " + partyary[0] + " \", \" " + partyary[3] + " \");' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[9] == "1") ? "checked" : "") + "></td>"; // ZD 101253
                }
            } /* FB 1779 - End */
            else {
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[8] == "1") ? "" : "") + "></td>";
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[9] == "1") ? "" : "") + "></td>";
            }
            //FB 1760 - End
            mt += "      <input type='hidden' name='NewInterfaceTypeParticipant" + newpartyno + "' value='" + partyary[10] + "'>";
            mt += "      <input type='hidden' name='NewConnectionTypeParticipant" + newpartyno + "' value='" + partyary[11] + "'>";
            mt += "      <input type='hidden' name='NewSourceParticipant" + newpartyno + "' value='" + partyary[12] + "'>";
            mt += "      <input type='hidden' name='NewIPISDNAddressParticipant" + newpartyno + "' value='" + partyary[13] + "'>";
            mt += "      <input type='hidden' name='NewVideoEquipmentParticipant" + newpartyno + "' value='" + partyary[14] + "'>";
            mt += "      <input type='hidden' name='NewLineRateParticipant" + newpartyno + "' value='" + partyary[15] + "'>";
            mt += "      <input type='hidden' name='NewProfileID" + newpartyno + "' value='" + partyary[20] + "'>"; //ALLDEV-814
            mt += "      <input type='hidden' name='NewUID" + newpartyno + "' value='" + partyary[21] + "'>"; //ALLDEV-814
            //FB 2348 Start
            if (EnableSurvey) {
                mt += "<td align='center' width='5%' style='display:" + dispNew + "'><input type='checkbox' name='2" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[16] == "1") ? " checked" : "") + "></td>";
            }
            else {
                mt += "<td align='center' width='5%' style='display:none'><input type='checkbox' name='2" + partyary[3] + partyary[20] + "' value='0'" + ((partyary[16] == "1") ? " checked" : "") + "></td>";
            }
            //FB 2348 End
            if (parent.document.location.href.toLowerCase().indexOf("expressconference") > 0) {
                mt += "<td align='center' class ='tableBody2' width='5%'><a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + partyary[20] + "\");return false;' class='lblError'>" + varRemove + "</a></td>"; //ZD 100834 //ALLDEV-833
            }

            mt += "    </tr>"
        }
    }

    mt += "  </table>"
    mt += "  </form>"
    mt += "  </div>" //Edited for FF

    _d.write(mt)
    if (document.getElementById("addPartyFname") != null)//ZD 100393
        document.getElementById("addPartyFname").focus();

    if (parent.document.frmSettings2.NeedInitial) {
        if (parent.document.frmSettings2.NeedInitial.value == "1") {

            parent.initialpartlist();

            parent.document.frmSettings2.NeedInitial.value = "0";
        }
    } else {
        setTimeout('window.location.reload();', 500);
    }

    if (document.getElementById("tblParticipants").rows.length > 1) // ZD 102425
        document.getElementById("tdRoomAttendee").width = '10%';
//-->
</script>
<!-- JavaScript finish -->
<html>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
</body>
</html>
