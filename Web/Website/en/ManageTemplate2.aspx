<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits ="ns_ManageTemplate2.ManageTemplate2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopnet.aspx" -->

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MyVRM</title>
   
        <script type="text/vbscript" language="vbscript" src="script/outlook.vbs"></script><%--Edited for FF--%>
        <script type="text/vbscript" language="vbscript" src="script/lotus.vbs"></script><%--Edited for FF--%>
        <script type="text/vbscript" language="vbscript" src="script/settings2.vbs"></script><%--Edited for FF--%>

        <script type="text/javascript" src="script/mousepos.js"></script>
        <script type="text/javascript" src="../<%=Session["language"] %>/script/saveingroup.js"></script>

        <!-- JavaScript begin -->

        <script type="text/javascript" language="javascript1.1" src="extract.js"></script>

        <script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
        <script type="text/javascript" src="../<%=Session["language"] %>/script/settings2.js"></script>
       <%-- <script type="text/javascript" src="script/group2.js"></script>  - Code Commented For FB 1476--%>
        <script type="text/javascript" src="script/RoomSearch.js"></script>
        <script type="text/javascript" language="JavaScript">
 
 
        
<!--

AddRooms();
  //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
  //ZD 100604 End
function CreateNewConference()
{
//    window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + document.getElementById("TemplateID").value; //Login Management
      window.location.href = "ConferenceSetup.aspx&t=t&confid=" + document.getElementById("TemplateID").value;
 //   alert(document.getElementById("TemplateID").value);
}

function TranscodeSel()
	{
		var mod = "";
		mod = ( (document.location.href).indexOf("settings2future") != -1 ) ? "fu" : mod;
		mod = ( (document.location.href).indexOf("settings2immediate") != -1 ) ? "im" : mod;
		mod = ( (document.location.href).indexOf("settings2room") != -1 ) ? "rm" : mod;
		mod = ( (document.location.href).indexOf("managetemplate2") != -1 ) ? "te" : mod;    
   
		url = "transcoding1.aspx?from=" + mod + "&wintype=pop";
		winTranscoding = window.open(url, "videodisplaylayout", "width=750,height=400,resizable=yes,scrollbars=yes,status=no");
		
		winTranscoding.focus();
		if (!winTranscoding.opener) {
			winTranscoding.opener = self;
		}
	}

function chkRoomValid()
{

    //FB 3003 Starts
	/*if (ifrmLocation.document.frmSettings2loc) {
		document.frmSettings2.MainLoc.value = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);
		s0 = ifrmLocation.document.frmSettings2loc.nonvideolocname.value;
	} else {
		s = "";
		document.frmSettings2.MainLoc.value = "";
		s0 = "";
	}
	
	s = "";
	if ( s0 != "") {
		sary= s0.split("%");
		for (i = 1; i < sary.length-1; i++) {
			sary[i] = sary[i].split("`");
			s += "* " + sary[i][1] + "\n";
		}			
	}
	*/
    //FB 3003 End

	if (document.getElementById("selectedloc").value == "") //if (document.frmSettings2.MainLoc.value == "")  Room search
		if (haveInvitee(document.frmSettings2.txtPartysInfo.value)) {
			alert(selectRoom)
			//alert(EN_71)
			return false;
		} else
			return true;
	else
		if (!haveInvitee(document.frmSettings2.txtPartysInfo.value)) {
		if('<%=Application["Client"].ToString().ToUpper()%>' !="MOJ")//Added for FB 1425 QA bug
		{//Added for FB 1425 QA bug
			var willContinue = confirm(EN_186);
			if (!willContinue)
				return false;
		}//Added for FB 1425 QA bug
		}
	
		if ("<%=Application["Client"]%>" == "WASHU" && (s != "")) { //case 316
		var willContinue = confirm(tempMsg1 + s + tempMSg2);
		return (willContinue);
	}

	return true;
}



function hasNewInvitedParty()
{

	partysary = (document.frmSettings2.txtPartysInfo.value).split("||");//FB 1888
	for (var i=0; i < partysary.length-1; i++) {
		partyary = partysary[i].split("!!");//FB 1888
	 if ( (partyary[0] == "new") && (partyary[3]!="") && (partyary[4]=="1") ) {
			return true;
			break;
		}
	}
	return false;
}
    function fnConfTypeChange(drp)
    {
      
        var drptype =  drp
         
        if(drptype != null)
        {
            if(drptype.value=="7")
            {
                   document.getElementById('tdPassword').style.visibility = "hidden";
                   document.getElementById('tdPassword2').style.visibility = "hidden";
                   document.getElementById('trtempSecure').style.visibility = "hidden";//ZD 101377
                   document.getElementById('Settings2Audio').disabled=true;
            }
            else
            {
                   document.getElementById('tdPassword').style.visibility = "visible";
                   document.getElementById('tdPassword2').style.visibility = "visible";
                   document.getElementById('Settings2Audio').disabled=false;
                   if ('<%=Session["NetworkSwitching"]%>' != null && '<%=Session["NetworkSwitching"]%>' != 2) 
                      document.getElementById('trtempSecure').style.visibility = "hidden";
                   else
                      document.getElementById('trtempSecure').style.visibility = "visible";//ZD 101377
            }
        } 
        if(drptype != null) //FB 1759
        {
        document.getElementById("CreateBy").value = document.getElementById("ddlConType").value;
        ifrmPartylist.location.reload();
        }
          
    }

function frmSettings2_Validator()
{
        if(document.getElementById('regDescription').style.display == 'inline')
        {
            window.scrollTo(0,0);
            return false;
        }

        if (document.getElementById("TxtConferenceDurationhr").value=="")
        {       
            document.getElementById("TxtConferenceDurationhr").value="0";
          
        }
        if (document.getElementById("TxtConferenceDurationmi").value=="")
        {       
            document.getElementById("TxtConferenceDurationmi").value="0";
          
        }
          maxDuration = 120;
           document.getElementById("hdnMaxDuration").value =120
          if( "<%=Application["MaxConferenceDurationInHours"] %>" != "")
            maxDuration = parseInt("<%=Application["MaxConferenceDurationInHours"] %>",10);
           document.getElementById("hdnMaxDuration").value=maxDuration;        
       
	// !! template name
	if (Trim(document.frmSettings2.txtTemplateName.value) == "") {
		alert(RequiredTemplate);
		document.frmSettings2.txtTemplateName.value = "";
		document.frmSettings2.txtTemplateName.focus();
		return (false);		
	}
	else{
		if(checkInvalidChar(document.frmSettings2.txtTemplateName.value) == false){
			return false;
		}
	}

	// !! conf name
	if (Trim(document.frmSettings2.TxtConferenceName.value) == "") {
		alert(RequiredConference);
		document.frmSettings2.TxtConferenceName.value = "";
		document.frmSettings2.TxtConferenceName.focus();
		return (false);		
	}
	else{ //FB 1640
		if(checkInvalidCharNew(document.frmSettings2.TxtConferenceName.value) == false){
			return false;
		}
	}
    
    // !! conf duration
    if (document.getElementById("TxtConferenceDurationhr").value == "0" && document.getElementById("TxtConferenceDurationmi").value < 15)
    {

        alert(DurationLimit);
        return false;
    }
    
  //  if (isNaN(document.getElementById("TxtConferenceDurationhr").value) || document.getElementById("TxtConferenceDurationhr").value < 0 || document.getElementById("TxtConferenceDurationhr").value > document.getElementById("hdnMaxDuration").value)
   // {
     //   alert("Invalid duration hours. Value should be between 0 and " + document.getElementById("hdnMaxDuration").value);
    //    return false;
    //}
    if (isNaN(document.getElementById("TxtConferenceDurationhr").value) || document.getElementById("TxtConferenceDurationmi").value < 0)
    {
        alert(InvalidDuration);
        return false;
    }
    if ((document.getElementById("TxtConferenceDurationhr").value=="")&&(document.getElementById("TxtConferenceDurationmi").value=="") )
    {
        alert(InvalidDurHour + document.getElementById("hdnMaxDuration").value);
        document.getElementById("TxtConferenceDurationhr").value="0";
        document.getElementById("TxtConferenceDurationmi").value="0";
        return false;
    }
    
    if ((parseInt(document.getElementById("TxtConferenceDurationhr").value,10) * 60 + parseInt(document.getElementById("TxtConferenceDurationmi").value)) > (document.getElementById("hdnMaxDuration").value * 60))
    {
        alert(InavlidDura + (document.getElementById("hdnMaxDuration").value*60) + "mins"); //ZD 100528
        return false;        
    }
        
	// !! conf password
	if ( !chkConfPassword(document.frmSettings2.ConferencePassword) )
		return false;
		//Added for Password verification -Start	
       if(document.frmSettings2.ConferencePassword.value.length>0)
       {
       if((document.frmSettings2.ConferencePassword.value.length<4)||(document.frmSettings2.ConferencePassword.value.length>10))
        return false;
         if(document.frmSettings2.ConferencePassword.value.substring(0,1)==0) 
        { return false;
        }
        }    



     //Added for Password verification -End
	
	//FB 1888 start
	var txtDescrip = document.frmSettings2.TxtDescription ;
	if (Trim(txtDescrip.value) != "")
	{ //FB 1640
		//if(checkInvalidCharNew(document.frmSettings2.TxtDescription.value) == false)
		if (txtDescrip.value.search(/^(a-z|A-Z|0-9)*[^\\^+|!`\[\]{}\=@#$%~]*$/)==-1) //FB 1888 //FB 2236
	       {
	         txtDescrip.focus();
	         return false;
	       }
	}
	//FB 1888 end
	if (Trim(document.frmSettings2.TemplateDescription.value) != "") {
		if(checkInvalidChar(document.frmSettings2.TemplateDescription.value) == false){
			return false;
		}
	}

	// refresh the party list and calculate the party number
	willContinue = ifrmPartylist.bfrRefresh(); 
	if (!willContinue)
		return false;


	//	check room
	if (!chkRoomValid()) {
		return false;
	}

	if ( hasNewInvitedParty() )  {
   
		url = "partyinfoinput.aspx";
		if (!window.winrtc) {
			winrtc = window.open(url,'','status=yes,width=850,height=300,resizable=yes,scrollbars=yes');
			winrtc.focus();
		} else {	
			if (!winrtc.closed) {     // still open
				winrtc.focus();
			} else {
				winrtc = window.open(url,'','status=yes,width=850,height=300,resizable=yes,scrollbars=yes');
			    winrtc.focus();
			}
		}
		return false;
	} else {
	
		partysinfo = chkdelBlankLine(document.frmSettings2.txtPartysInfo.value);
		document.frmSettings2.txtPartysInfo.value = partysinfo;
		partysary = partysinfo.split("||");//FB 1888
		document.frmSettings2.PartysNum.value = partysary.length-1;

		needLecturerNET();		
		return false;
	
	}

	return true;
}


function newpartysubmit ()
{

	partysinfo = chkdelBlankLine(document.frmSettings2.txtPartysInfo.value);
	document.frmSettings2.txtPartysInfo.value = partysinfo;
	partysary = partysinfo.split("||");//FB 1888
	document.frmSettings2.PartysNum.value = partysary.length-1;

	needLecturerNET();
}
/*  Code Added For FB 1476 - Start  */
function getAGroupDetail(frm, cb, gid)
{
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
			alert(ConfSystemerror)
			return false;
		}
	} else {

		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(GroupDoubleClick);
			return false;
		}
	}
	
	//code added for Managegroup2.asp to aspx convertion 
	if(frm == 'g')
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=no");
	else
	    //gm = window.open("memberallstatus.asp?f=" + frm + "&n=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	if (gm) gm.focus(); else alert(PopupBlockMsg);
}
/*  Code Added For FB 1476 - End  */
//-->

 //FB 2384 Start
  if(document.getElementById('hdnParty') != null)
    {
    if (document.getElementById('hdnParty').Value != "" && document.getElementById('txtPartysInfo').Value.Trim() == "")
        document.getElementById('txtPartysInfo').Value = document.getElementById('hdnParty').Value;
    }
 //FB 2384 End
 
    function fnCancel() //FB 2565
	{
	
		window.location.replace('ManageTemplate.aspx');  //CSS Project
		return false;//ZD 100420
	}        
	//ZD 100420 start
	function fnReset()
	{
	    window.location.reload();
		return false;
	}
	//ZD 100420 End
</script>
 
 </head>
 
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form id="frmSettings2" name="frmSettings2" runat="server" method="post" lang="JavaScript"  onsubmit="DataLoading(1);"> <%--ZD 100176--%>
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" name="CreateBy" id="CreateBy" runat="server" value=""/> <%-- FB 1759 --%>
    <input type="hidden" name="ConfID" id="ConfID"/>
    <input type="hidden" name="outlookEmails" />
    <input type="hidden" name="lotusEmails" />
    <input type="hidden" id="helpPage" value="75"/>
    <input type="hidden" id="VideoLayout" runat="server" />
    <input type="hidden" id="Lecturer" name="Lecturer" runat="server"/>
    <input type="hidden" id="VideoProtocol" runat="server"/>
    <input type="hidden" id="hdnMaxDuration"/>
    <input type="hidden" id="hdnSubmit" name="hdnSubmit" runat="server"/>
    <input type="hidden" name="hdnXML" runat="server" id="hdnXML" />
    <!--new AV Param section-->
    <input type="hidden" id="maxAudio" runat="server" />
    <input type="hidden" id="maxVideo" runat="server"/>
    <input type="hidden" id="restrictProtocol" runat="server"/>
    <input type="hidden" id="restrictAV" runat="server"/>
    <input type="hidden" id="ManualVideoLayout" runat="server"/>

    <input type="hidden" id="LineRate" runat="server"/>
    <input type="hidden" id="AudioAlgorithm" runat="server"/>
    <input type="hidden" id="VideoSession" runat="server"/>

    <input type="hidden" id="conferenceOnPort" runat="server"/>
    <input type="hidden" id="encryption" runat="server"/>
    <input type="hidden" id="dualStream" runat="server"/>
    <input type="hidden" id="LectureMode" runat="server"/>
    <!--end new AV Param section-->

    <input type="hidden" id="TemplateID" runat="server"/>
    <input type="hidden" id="MainLoc" runat="server" />
    <input type="hidden" id="VideoEquipments"/>
    <input type="hidden" id="txtUsersStr" runat="server" />
    <input type="hidden" id="txtPartysInfo" runat="server"/>
    <input type="hidden" id="hdnSettingsStr" runat="server"/>
    <input type="hidden" id="PartysNum" runat="server" value='0'/>
 
     <input type="hidden" id="hdnSettings2locpg" value="<%=settings2locpg%>"/>
     <input type="hidden" id="settings2locstr" value="<%=settings2locstr%>"/>
    <input type="hidden" id="ownerFName" runat="server" />
    <input type="hidden" id="ownerLName" runat="server" /> 
    <input type="hidden" id="getOrgLocIDs" runat="server" /> <!--Added for FB 1398-->
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
    <input type="hidden" name="Recur" id="hdnParty" runat="server" /> <%--FB 2384--%>
    <div id="dataLoadingDIV" style="display:none" align="center" >
         <img border='0' src='image/wait1.gif'  alt='Loading..' />
    </div> <%--ZD 100678 End--%>
    <div align="center">
      
    <table border="0" cellpadding="4" cellspacing="0" width="850">
      <tr>
            <td colspan="6" align="center">
                <center>
                  <h3> <asp:Label id="LblTitle" runat="server"></asp:Label>&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Template%>" runat="server"></asp:Literal></h3>                  
                </center>
            </td>
      </tr>
      <tr>
        <td align="center" colspan="5">
            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
        </td>
    </tr>
      <tr>
        <td align="left" colspan="6">
          <span class="reqfldstarText">*</span>
          <span class="reqfldText"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_RequiredField%>" runat="server"></asp:Literal></span><!--FB-1719-->
        </td>
      </tr>
      <tr>
        <td height="5" colspan="3"></td>
      </tr>

      <tr>
        <td width="15%" align="left">
          <label for="TemplateName" class="blackblodtext">
          <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageTemplate_TemplateName%>" runat="server"></asp:Literal> </label><span class="reqfldstarText">*</span> <%--Edited for FB 1425 QA Bug--%>
        </td>
        <td width="35%" align="left">
          <input type="text" runat="server" id="txtTemplateName" maxlength="256" value="" onkeyup="javascript:chkLimit(this,'2');" size="28"  class="altText"/>
          
        </td>
        <td></td>
        <td width="15%" align="left"><label for="TemplatePublic" class="blackblodtext">
        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Public%>" runat="server"></asp:Literal></label></td>
        <td align="left"> <!-- FB 2050 -->
          <input type="checkbox" name="TemplatePublic" id="TemplatePublic" runat="server" value="1"/>
		</td>
        <td align="left">
          <span class="blackblodtext"> <font size="1"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Makestemplate%>" runat="server"></asp:Literal></font></span>
		</td>
      </tr>
      <tr>
        <td align="left" valign="top"><label for="TemplateDescription" class="blackblodtext">
        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConfirmTemplate_TemplateDescri%>" runat="server"></asp:Literal></label></td>
        <td align="left"><textarea cols="35" name="TemplateDescription" runat="server" id="TemplateDescription" rows="4" maxlength="2000"  onkeyup="javascript:chkLimit(this,'8');" class="altText"></textarea></td>
        <td></td>
        <td style="width:100%" colspan="4">
         <table width="100%">
          <tr>
            <td align="left" valign="top" style="width:30%"><label class="blackblodtext">
            <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, OwnerName%>" runat="server"></asp:Literal></label></td>
            <td colspan="2" valign="top" align="left"><font color="blue"><asp:Label ID="LblOwner" runat="server"></asp:Label></font></td>
          </tr>
          <tr style="height:20px"></tr>
          <%--FB 1719 start--%>
          <tr> 
            <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_SetasDefault%>" runat="server"></asp:Literal></td>
            <td align="left" valign="top">
                <asp:CheckBox ID="chkSetDefault" runat="server" Text="" TextAlign="Left" />
            </td>
          </tr>
          <%--FB 1719 end--%>
         </table>
        </td>
      </tr>
    </table>
    </div>  
  <hr width="950"/>
  <br/>

  <div align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="920">
      <tr>
        <td colspan="3">
&nbsp;
        </td>
      </tr>
      <tr>
        <td colspan="3" height="10"></td>
      </tr>

      <!--<tr>        <td colspan="3" align="right">          <SPAN class="reqfldstarText">*</SPAN>          <SPAN class="reqfldText">= Required Information</SPAN>        </td>      </tr>-->
      <tr>
        <td height="5" colspan="3"></td>
      </tr>
      
      
      <tr>
        <td width="3%">
&nbsp;        </td>
        <td width="1%" height="20"></td>
        <td width="96%" height="20" align="left">
		  <span class="subtitleblueblodtext" style="margin-left:-20px; position: relative;"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Hearing Settings<%}else{ %>
          <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ConferenceSettings%>" runat="server"></asp:Literal><%} %></span> <%--Edited  For FB 1428--%>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td align="center">
            <table border="0" cellpadding="2" width="100%">
              <tr>
                <td width="14%" align="left">
                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Name%>" runat="server"></asp:Literal></span><span class="reqfldstarText">*</span>
                </td>
                <td width="32%" align="left">                  
                  <asp:TextBox ID="TxtConferenceName" runat="server" size="28" class="altText"></asp:TextBox><%--FB 2508--%>
                                                             <%-- Code Added for FB 1640--%>                                                
                  <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="TxtConferenceName" Display="dynamic" runat="server" SetFocusOnError="true"  ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                  <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="TxtConferenceName"  ValidationExpression="^[\s\S]{0,256}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit2%>"
                   Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>
                </td>
                <td></td>
                <td width="21%"></td>
                <td width="31%" align="left">
                
                  <table cellspacing="0" cellpadding="0" border="0">
                     <tr id="TreeList" style="display:none;"> <%--Changed for FB 1438--%>
                      <td style="height: 20px">
                        <input type="radio"  class="blackblodtext" name="roomListDisplayMod" id="roomListDisplayMod" value="1" checked onClick="JavaScript:chgRoomDisplay(document.frmSettings2.hdnSettings2locpg.value, 1, true);"> <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_LevelView%>" runat="server"></asp:Literal></span>
                      </td>
                      <td width="10" style="height: 20px"></td>
                      <td style="height: 20px">
                        <input type="radio"  name="roomListDisplayMod" id="Radio1" value="2" onClick="JavaScript:chgRoomDisplay('<%=settings2locpg %>', 2, true);"> <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_ListView%>" runat="server"></asp:Literal></span>
                      </td>
                    </tr>
                  </table>

                </td>
              </tr>
              <tr>
                 <td align="left" id="tdPassword">
                    <span class="blackblodtext" id="mgpwd" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_mgpwd%>" runat="server"></asp:Literal></span><%--Edited for MOJ Phase 2 QA--%>
                </td>               
                 <td id="tdPassword2" align="left">
                  <%--FB 2244--%>
                  <input type="text" name="ConferencePassword" value="<% =confPassword %>" id="ConferencePassword" runat="server" maxlength="256"  onkeyup="javascript:chkLimit(this,'5');" size="28" class="altText"/>
                  <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<%$ Resources:WebResources, Required_numericPassword%>" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, NumericValuesOnly1%>" ControlToValidate="ConferencePassword" ValidationExpression="^([1-9])([0-9]\d{2,8})" Display="Dynamic"></asp:RegularExpressionValidator> <%--Comments: Fogbugz case 107, 522 --%>
                </td>
                 <td rowspan="7"></td> <td rowspan="7" valign="top" align="right">
                    <input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmSettings2');" value="<%$ Resources:WebResources, AddRoom%>" runat="server" class="altMedium0BlueButtonFormat" /><br />
                    
                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" />
                    <span class="blackblodtext"> <font size="1"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Doubleclickon%>" runat="server"></asp:Literal></font></span>
                  <iframe style="display:none;" name="ifrmLocation" src="" width="100%" height="300" align="left" valign="top">
                    <p><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_goto%>" runat="server"></asp:Literal><a id="aLocation" href="" name="aLocation"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_aLocation%>" runat="server"></asp:Literal></a></p>
                  </iframe> 
                </td>
                <td rowspan="7" valign="top" align="left">
                  <select size="4" name="RoomList" runat="server" id="RoomList" multiple="true" onDblClick="javascript:Delroms(this.value)" onkeydown="if(event.keyCode ==32){javascript:Delroms(this.value)}" class="treeSelectedNode" style="height:350px;width:80%;">  <%--ZD 100420--%>
                  </select>                  
                </td>
              </tr>
              <tr>
                <td align="left" valign="top"><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Description%>" runat="server"></asp:Literal></span></td>
                    <td align="left">
                        <textarea id="TxtDescription" runat="server" rows="3" cols="20" onkeyup="javascript:limitDescriptionLen(this)" class="altText"></textarea><%--FB 2508--%>
                                                             <%-- Code Added for FB 1640--%>                                                
                        <asp:RegularExpressionValidator ID="regDescription" ControlToValidate="TxtDescription" Display="dynamic" runat="server" SetFocusOnError="true"  ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters34%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\^+|!`\[\]{}\=%~]*$"></asp:RegularExpressionValidator><%--FB 1888--%> <%--FB 2236--%>
                        <asp:RegularExpressionValidator runat="server" ID="ValConfDesc" ControlToValidate="TxtDescription"  ValidationExpression="^[\s\S]{0,2000}$"   ErrorMessage="<%$ Resources:WebResources, MaxLimit3%>"
                         Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>   
                    </td>
              </tr>
              <tr>
                <td align="left">
                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Duration%>" runat="server"></asp:Literal></span><span class="reqfldstarText">*</span>
                </td>
                <td align="left">                    
                 <asp:TextBox ID="TxtConferenceDurationhr" runat="server" CssClass="altText" Columns ="3"></asp:TextBox>                  
                 <asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_hrs%>" runat="server"></asp:Literal>
                 <asp:TextBox ID="TxtConferenceDurationmi" runat="server" CssClass="altText"  Columns ="3"></asp:TextBox>                  
                 mins
                </td>
              </tr>
             <!-- Code added by offshore for FB Issue 1123 Start --> 
             <tr id="trCnTy"><%--Edited for MOJ Phase 2 QA--%>
                <td class="blackblodtext" align="left"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Hearing Type<%}else{ %>
                <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ConferenceType%>" runat="server"></asp:Literal><%} %></td> <%--Edited For FB 1428--%>
                <td style="height: 24px" align="left">     
                    <%--Window Dressing--%> 
                    <asp:DropDownList ID="ddlConType" class="SelectFormat"  runat="server">
                        <asp:ListItem Value="6" Text="<%$ Resources:WebResources, AudioOnly%>"></asp:ListItem>
                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, AudioVideo%>"></asp:ListItem>
                        <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                        <asp:ListItem Value="7" Text="<%$ Resources:WebResources, RoomConference%>"></asp:ListItem>
                    </asp:DropDownList>
                    
                </td>
              </tr>
              <!-- Code added by offshore for FB Issue 1123 END --> 

              <%--ZD 101377 START--%>
                        <tr id="trtempSecure">
                            <td class="blackblodtext" align="left">
                                <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ConferenceSetup_NetworkClassif%>"
                                    runat="server"></asp:Literal>
                            </td>
                            <td style="height: 24px" align="left">
                                <asp:DropDownList ID="drpNtwkClsfemp" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ConferenceSetup_NATOSecret%>"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, ConferenceSetup_NATOUnclassified%>"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <%--ZD 101377 END--%>
              
              
              <tr>
                <td height="20"></td>
                <td></td>
                
              </tr>

              <%--code added for MOJ Phase 2 QA Bug--%>
              <tr id="AVTR1" runat="server">
              <%--code added for MOJ Phase 2 QA Bug--%>
                <td align="left"></td>
                <td valign="bottom" align="left" id="VideoDisplayLayoutDiv">
                 <%-- <input type="button" id="Settings2Audio" name="Settings2Audio" value="Advanced Audio/Video" class="altLongBlueButtonFormat" onClick="JavaScript: TranscodeSel();">--%> <%--ZD 100420--%>
                 <button type="button" id="Settings2Audio" name="Settings2Audio" class="altLongBlueButtonFormat" style="width:250px;" onClick="JavaScript:TranscodeSel();"> 
                 <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ConferenceSetup_AdvancedAudioV%>" runat="server"></asp:Literal></button><%--ZD 100420 ZD 100922--%>
                </td>
              </tr>

              <tr>
                <td align="left" height="110"></td>
                <td valign="bottom" align="left">
                </td>
              </tr>
            </table>
        </td>
      </tr>

      <tr>
        <td height="10"></td>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
      <%--code added for MOJ Phase 2 QA Bug--%>
      <tr id="ParticipantTR1" runat="server">
          <td>
&nbsp;    </td>
        <td></td>
        <td align="left">
		  <span class="subtitleblueblodtext" style="margin-left:-20px; position:relative"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_ParticipantsSe%>" runat="server"></asp:Literal></span>
		</td>
      </tr>
      <tr  id="ParticipantTR2" runat="server">
      <%--code added for MOJ Phase 2 QA Bug--%> 
        <td height="21"></td>
        <td></td>
        <td align="center">
            <table border="0" cellpadding="2" cellspacing="0" width="100%" height="95">
              <%--<tr> FB 1759 - Starts
                <td width="10%" height="15" align="right" valign="top"></td>
                <td width="90%" bordercolor="#0000FF" colspan="4" align="left">
                  <table border="0" cellpadding="2" cellspacing="0" width="97%">
                    <tr class="tableHeader">
                      <td align="center" width="3%"><span class="tableHeader">DELETE</span></td>
                      <td align="center" width="25%"><span class="tableHeader">NAME</span></td>
                      <td align="center" width="27%"><span class="tableHeader">EMAIL</span></td>
                      <td align="center" width="8%"><span class="tableHeader">
                      <!--Add Value-->
                        <a title="">External Attendees</a>
                      </span></td>
                      <td align="center" width="8%"><span class="tableHeader">
                      <!--Add Value-->
                        <a title="">Room Attendees</a>
                      </span></td>
                      <td align="center" width="3%" class="tableHeader"><span class="tableHeader">CC</span></td>
                      <td align="center" width="5%"><span class="tableHeader">Notify</span></td>
                      <td align="center" width="5%"><span class="tableHeader">AUDIO</span></td>
                      <td align="center" width="5%"><span class="tableHeader">VIDEO</span></td>
                    </tr>
                  </table>
                </td>
              </tr> FB 1759 --%>
              <tr>
                <td height="21" align="left" valign="top" width="120px">
                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Participants%>" runat="server"></asp:Literal></span>
                  <input type="button" name="AddGroup" value="<%$ Resources:WebResources, AddtoGroup%>" runat="server" class="altMedium0BlueButtonFormat" title="<%$ Resources:WebResources, AddGroupMsg%>" style="width: 135px"  onClick="saveInGroupNET()"><%-- FB 2779 --%>
                  <input type="button" name="sbtgroup" id="sbtgroup" style="display:none;" onclick="saveGroupSucc()"> <%--Login Management--%> 
                                      <input type="button" name="cbtgroup" id="cbtgroup" style="display:none;" onclick="saveGroupFail('<%=Session["GrpErrMsg"]%>')"> <%--Login Management--%>

                  <!--<span class="cmmttext">add to group</span><br>
                  <a onClick="saveInGroup()" style="cursor: hand;"> 
                    <img src="image/addtogroup.gif" border="0" WIDTH="16" HEIGHT="16">
                  </a>-->
                </td>
                <td bordercolor="#0000FF" colspan="4" align="center">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="99">
                    <tr>
                      <td width="90%" height="150" valign="top" align="left"> <%--FB 1759--%>

                        <iframe id="ifrmPartylist" src="../en/settings2party.aspx" name="ifrmPartylist" width="100%" height="150" align="left" valign="top"> <%-- FB 2050 --%>
                          <p><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_goto%>" runat="server"></asp:Literal><a href="settings2party.aspx"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Participants%>" runat="server"></asp:Literal></a></p>
                        </iframe> 

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="62" align="left" valign="top">
                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Groups%>" runat="server"></asp:Literal></span>
                </td>
                <td>                 
                <%--Window Dressing--%>
                  <asp:ListBox runat="server" ID="Group" CssClass="SelectFormat"  Rows="4" SelectionMode="multiple" onchange="JavaScript:groupChgNET();" onDblClick='JavaScript: getAGroupDetail(1, this, null);'></asp:ListBox>       
                    <!-- SHD BE ADDED   --> 
                </td>
                <%--Window Dressing--%>
                <td align="left" valign="middle"><span class="blackblodtext"> <font size="1"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Clickongroup%>" runat="server"></asp:Literal></font></span></td>
                <td width="21%" align="left"></td>
                <td width="31%" align="left"><table border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                      <td width="100%" align="left">
                        <input type="button" id="btnRemoveAll" name="Settings2Submit" runat="server" value="<%$ Resources:WebResources, ManageGroup2_RemoveAll%>" class="altMedium0BlueButtonFormat" onClick="deleteAllPartyNET();" language="JavaScript">
                      </td>
                      <td width="100%" align="left">
                    <%--code changed for Softedge button--%>
                        <%--<input type="button" onfocus="this.blur()"  name="Settings2Submit" value="myVRM Look Up" class="altLongBlueButtonFormat" onClick="getYourOwnEmailListNET();" language="JavaScript">--%>
                        <button type="button" onfocus="this.blur()" id="btnAddressbook" name="Settings2Submit"  style="width:220px;" class="altLongBlueButtonFormat"  onClick="getYourOwnEmailListNET();">
                        <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, AddressBook%>" runat="server"></asp:Literal></button> <%--ZD 100922--%>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" align="right" colspan="2">
                    <%--code changed for Softedge button--%>
                        <%--<input type="button" onfocus="this.blur()" name="Settings2Submit" value="Add New Participant" class="altLongBlueButtonFormat" onclick="addNewParty1();" language="JavaScript">--%>
                        <button type="button" onfocus="this.blur()" id="btnAddNewPartcipant" name="Settings2Submit" class="altLongBlueButtonFormat" onclick="addNewParty1();" style="width:220px;">
                        <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, AddNewParticipant%>" runat="server"></asp:Literal></button> <%--ZD 100922--%>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" align="left" colspan="2">
                     <%
                         Boolean enableOUTLOOK = true;
                         if (Request.ServerVariables["HTTP_USER_AGENT"].IndexOf("MSIE") > 0 || Request.ServerVariables["HTTP_USER_AGENT"].ToUpper().IndexOf("FIREFOX") > 0) //iEdited for FF
                        {                                 
                            switch (emailClient)
                            {   case "0":
		                        case "1":
			                        if (enableOUTLOOK)
                                     //Changed Value on 3rdApr09 Start
                                        //Response.Write ("<input type='button' name='Settings2Submit' value='MS Outlook Look Up' class='altLongBlueButtonFormat' onClick='getOutLookEmailList();'>");
                                        Response.Write("<input type='button' onfocus='this.blur()' name='Settings2Submit' value='Outlook Address Book' class='altLongBlueButtonFormat' onClick='javascript:getOutLookEmailList();'>");
                                    //Changed Value on 3rdApr09 End
				                        
                                    break;
		                        case "2":
			                        //==============================================================
			                        //added on 3/24/04
			                        //get the lotus login info from session for lotus email function
			                        //==============================================================
			                        String lnLoginName;String lnPwd;String lnDBpath;
			                        lnLoginName = Session["lnLoginName"].ToString();
			                        lnPwd = Session["lnLoginPwd"].ToString();
			                        lnDBpath = "names.nsf";
                   //code changed for Softedge button
			                        Response.Write ("<input type='button' onfocus='this.blur()' name='Settings2Submit' value='Lotus Notes Look Up' class='altLongBlueButtonFormat' onClick='getLotusEmailList('"+ lnLoginName + "','" + lnPwd + "','" + lnDBpath + "');'>");
                                    break;
                            }
                        }
                        %>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              
              <tr>
                <td align="left" height="38"><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_Public%>" runat="server"></asp:Literal></span></td>
                <td align="left">

                  <table border="0">
                    <tr>
                      <td align="left">
                      <!--Add Value to script in''-->
                        <input type="checkbox" id="ChkPublicConf" value="1" runat="server" />
                      </td>
                      
                      <td width="20%"></td>
                        <%
                            
                            if (Session["dynInvite"].ToString() == "1")
                            {                           
                          
                        %>             

                      <td>
                        <table border="0" id="DyamicInviteDIV">
                          <tr>
                            <td align="left" class="blackblodtext">
                              <b><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_OpenforRegist%>" runat="server"></asp:Literal></b>
                            </td>
                            <td>
                              <input type="checkbox" id="DynamicInvite" runat="server" value="1" />
                            </td>
                          </tr>
                        </table>
                      </td>
                        <%
                        }
                        %>           


                    </tr>
                  </table>
                  
                </td>
              </tr>
              
            </table>
        </td>
      </tr>
      <tr>
        <td height="11"></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
          <td>
&nbsp;          </td>
        <td></td>
        <td align="left">
		  <span class="subtitleblueblodtext" style="margin-left:-20px; position:relative"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_ConfirmYourTe%>" runat="server"></asp:Literal></span>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td align="center">
          <table border="0" cellpadding="2" cellspacing="2" width="100%">
            <tr>
              <td width="10%" align="left" valign="top" rowspan="3"></td>
              <td width="32%" rowspan="3"></td>          
              <td rowspan="3"></td>
              <td width="21%" rowspan="3">
              </td>
              <td width="31%"></td>
            </tr>
            <tr>
              <td align="right">
                    <%--code changed for Softedge button--%>
                <%--<input type="button" onfocus="this.blur()" name="Settings2Submit" value="Reset" class="altLongBlueButtonFormat" onclick="JavaScript:window.location.reload();" style="width:100px" OnClientClick="DataLoading(1)"/> <%--ZD 100176--%>
               <button type="reset" name="Settings2Submit" id="BtnReset" class="altLongBlueButtonFormat" onclick="javascript:return fnReset();DataLoading(1)" style="width:100px">
               <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button> <%--ZD 100922--%>
              </td>                          
              <td align="right">
                <%--<input type="button" id="Close" class="altLongYellowButtonFormat" value="Cancel" onclick="fnCancel()" style="width:100px" OnClientClick="DataLoading(1)"/> <%--FB 2565--%>   <%--ZD 100176--%>
                <button id="btnCancel" class="altLongYellowButtonFormat" onclick="javascript:return fnCancel();DataLoading(1)" style="width:100px" >
                <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
              </td>
              <td align="right">
                <input type="hidden" name="roomVideoAvailable" value=""/>
                    <%--code changed for Softedge button--%>
                    <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/> <%--Edited for FF--%>
                <%--<asp:Button ID="Submit"  onfocus="this.blur()" runat="server" Text="Submit" CssClass="altLongYellowButtonFormat" OnClick="Submit_Click" Width="100px"/>--%>
                <button id="Submit"  runat="server" Class="altLongYellowButtonFormat"  onserverclick="Submit_Click" style="Width:100px">
				<asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
              </td>
            </tr>
         </table>
        </td>
      </tr>
    </table>
  </div>
  
  <!--ADD A Value-->
<input type="hidden" name="NeedInitial" value="1"/>

  <!--ADD A Value-->


<!--Add a Frame of  Save in Group page-->

<!-- Add a Script-->
    </form>
    <iframe src="ifrmsaveingroup.aspx?wintype=ifr" id="ifrmSaveingroup" name="ifrmSaveingroup" width="0" height="0">
  <p><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate2_SaveinGroupp%>" runat="server"></asp:Literal></p>
</iframe>
</body>

<script Language="JavaScript">
<!--
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    //Added for default selection of conference -start
    var drptype= document.frmSettings2.ddlConType;
    if(drptype.value=="7")
    {
           document.getElementById('tdPassword').style.visibility = "hidden";
           document.getElementById('tdPassword2').style.visibility = "hidden";
           document.getElementById('trtempSecure').style.visibility = "hidden";//ZD 101377
           document.getElementById('Settings2Audio').disabled=true;
    }	
	//Added for default selection of conference -End	
	document.frmSettings2.ConferencePassword.value="<% =confPassword %>";
	function initgeneral ()
	{
		// initial template
		document.frmSettings2.TemplatePublic.checked = ("<%=strTemplatePublic%>"=="1") ? true : false;
	
	/*	// initial conference
		dhour = Math.floor(<%=dmin%> / 60);
		dmin = <%=dmin%> % 60;

        document.getElementById("TxtConferenceDurationhr").value = dhour;
        document.getElementById("TxtConferenceDurationmi").value = dmin;
        */
        
		document.frmSettings2.ChkPublicConf.checked = (<%=publicConf%>==1) ? true : false;

		if ("<%=Session["dynInvite"]%>" == "1") {
			document.frmSettings2.DynamicInvite.checked = ("<%=strDynamicInvite%>"=="1") ? true : false;
			clkpublic (document.frmSettings2.ChkPublicConf, '<%=Session["dynInvite"]%>');
		}

	}

	initgeneral();

	function initialpartlist ()
	{
	   /* var	gcb = document.frmSettings2.Group;
		for (j = 0; j < gcb.length; j++)
			if ("<%=gId%>" == gcb.options[j].value) {
				gcb.options[j].selected = true;
				initGroup(j, 1);
			}*/
	
	}
	
	//Added for MOJ Phase 2 QA START
	if('<%=Application["Client"].ToString().ToUpper()%>' == "MOJ")
    {
        document.getElementById("ConferencePassword").style.display = "none";
        document.getElementById("mgpwd").style.display = "none";
        document.getElementById("trCnTy").style.display = "none";
        document.getElementById("ChkPublicConf").checked = true;
        //document.getElementById("TemplatePublic").checked = true;
    }
    //Added for MOJ Phase 2 QA END
	
	if (document.frmSettings2.hdnSettings2locpg.value !="")
	    ifrmLocation.src=document.frmSettings2.hdnSettings2locpg.value;
	    
   //Added for Fb 1438 -- start
	if(document.frmSettings2.settings2locstr.value == "!" || document.frmSettings2.settings2locstr.value == "")
	   document.getElementById("TreeList").disabled = true;
	//Added for Fb 1438 -- end
	    
	    //Added for FB 1398 Start
	    if (typeof(ifrmLocation) != "undefined") {
	    tmpstr = "<%=getOrgLocIDs.Value%>";
		//tmpstr = ",";
		switch ("<%=Session["RoomListView"]%>") {
			case "level":
				document.frmSettings2.roomListDisplayMod[0].checked = true;
				ifrmLocation.location.href = "<%=settings2locpg%>" + "&mod=1&cursel=" + tmpstr + "&";
				break;
			case "list":
				document.frmSettings2.roomListDisplayMod[1].checked = true;
				ifrmLocation.location.href = "<%=settings2locpg%>" + "&mod=2&cursel=" + tmpstr + "&";
				break;
		}
		//Added for FB 1398 End
	}
//-->
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>

<!-- FB 2050 Start -->
<script type="text/javascript">

function refreshIframe()
{
var iframeId = document.getElementById('ifrmPartylist');
iframeId.src = iframeId.src;
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100420
document.getElementById('ConferencePassword').setAttribute("onblur", "document.getElementById('opnRooms').focus(); document.getElementById('opnRooms').setAttribute('onfocus', '');");
document.getElementById('btnRemoveAll').setAttribute("onblur", "document.getElementById('btnAddressbook').focus(); document.getElementById('btnAddressbook').setAttribute('onfocus', '');");
document.getElementById('btnAddressbook').setAttribute("onblur", "document.getElementById('btnAddNewPartcipant').focus(); document.getElementById('btnAddNewPartcipant').setAttribute('onfocus', '');");
//document.getElementById('ChkPublicConf').setAttribute("onblur", "document.getElementById('BtnReset').focus(); document.getElementById('BtnReset').setAttribute('onfocus', '');");
//if (document.getElementById("DynamicInvite") != null) { //ZD 100369
//    document.getElementById('ChkPublicConf').setAttribute("onblur", "document.getElementById('DynamicInvite').focus(); document.getElementById('DynamicInvite').setAttribute('onfocus', '');");
//    document.getElementById('DynamicInvite').setAttribute("onblur", "document.getElementById('BtnReset').focus(); document.getElementById('BtnReset').setAttribute('onfocus', '');");
//}   
//ZD 100420
//ZD 100176 End
//ZD 101377 START
if ('<%=Session["NetworkSwitching"]%>' != null) {
    if ('<%=Session["NetworkSwitching"]%>' != 2) {
        document.getElementById('trtempSecure').style.visibility = "hidden";
    }
}
//ZD 101377 END
</script>
<!-- FB 2050 End -->

<!-- #INCLUDE FILE="inc/mainbottomnet.aspx" --> 
</html>
