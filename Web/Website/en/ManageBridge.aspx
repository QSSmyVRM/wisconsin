<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_Bridges.Bridges" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/managemcuorder.js"></script>

<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	function ManageOrder (par) {
		//ZD 100420
	    if (par == '0') {
	        mousedownY = 400;
	        mousedownX = 500;
	    }
        
		//ZD 100420
	    change_mcu_order_prompt('image/pen.gif', ManageMCUOrder, document.getElementById('Bridges').value, 'MCUs'); //Edited For FF...
		
//		frmsubmit('SAVE', '');
	}
	
	function frmsubmit()
	{
	
	
	    if(document.getElementById("btnManage")!= null)//FB 1920
	    {
	        document.getElementById("btnManage").click();
	    }
	    /* Commented for FB 1920
	
	    if(document.getElementById("__EVENTTARGET")!= null)//FB 1763
	    {
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	    }*/
	    //ZD 100176 start
	    function DataLoading(val) {
	        if (val == "1")
	            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	        else
	            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	    }
	    //ZD 100176 End
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage MCUs</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <div id="dataLoadingDIV" align="center" style="display:none">
                    <img border='0' src='image/wait1.gif'  alt='Loading..' />
             </div> <%--ZD 100176--%> <%--ZD 100678 End--%>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5" width="100%"> <%-- FB 2050 --%>
                        <tr>
                            <td>&nbsp;</td>
                            <td width="50%">
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageBridge_ExistingMCUs%>" runat="server"></asp:Literal></SPAN>
                            </td>
                            <%--FB 1920 Starts--%>
                            <td width = "100%" style="color: Red;" align="right">
                                <asp:Label  ID = "lblMCUmsg" runat="server">
                                    <b>(*)</b> <asp:Literal Text="<%$ Resources:WebResources, PublicMCUTenant%>" runat="server"></asp:Literal>
                                </asp:Label>
                            </td>
                            <%--FB 1920Ends--%>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgMCUs" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" style="border-collapse:separate" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                         OnItemDataBound="BindRowsToGrid" OnDeleteCommand="DeleteMCU" OnEditCommand="EditMCU" Width="90%" Visible="true" > <%--Edited for FF--%> <%--FB 1920--%>
                        <%--Window Dressing start--%>
                        <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody"/>
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <%--Window Dressing end--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns><%--window dressing start--%>
                            <asp:BoundColumn DataField="ID" Visible="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="interfaceType" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ResponseConference_InterfaceType%>" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="administrator" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Administrator%>" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="exist" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ExistingVirtual%>" ItemStyle-CssClass="tableBody"></asp:BoundColumn>   <%--FB 2907--%>
                            <asp:BoundColumn DataField="status" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageConference_Status%>" ItemStyle-CssClass="tableBody"></asp:BoundColumn>                <%--FB 2907--%>
                            <asp:BoundColumn DataField="order" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%"> <%-- FB 2050 --%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageBridge_btnEdit%>" id="btnEdit" commandname="Edit" onclientclick="javascript:DataLoading('1');"></asp:LinkButton>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageBridge_btnDelete%>" id="btnDelete" commandname="Delete" onclientclick="javascript:DataLoading('1');"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                <div style="float:left; text-align:left; width:150px"> <%-- FB 2050 --%>
                                    <%--Window Dressing--%>                                
                                    <b><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageBridge_TotalMCUs%>" runat="server"></asp:Literal></span><asp:Label  ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
				    <br>
				     <%--Window Dressing--%>                                
				    <b><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageBridge_LicensesRemaini%>" runat="server"></asp:Literal>&nbsp;</span><asp:Label id="lblRemaining" runat="server" text=""></asp:Label> </b>
				                </div> <%-- FB 2050 --%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <%--window dressing End--%>
                    <asp:Table runat="server" ID="tblNoMCUs" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No MCUs found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td align="left">               <%--FB 2907--%>
                                <button ID="btnManageOrder"  onkeydown="if(event.keyCode == 13){javascript:ManageOrder('0');return false;}" runat="server" onclick="javascript:ManageOrder('1');return false;" class="altLongBlueButtonFormat" style="width:310px">
								<asp:Literal Text="<%$ Resources:WebResources, ManageBridge_btnManageOrder%>" runat="server"></asp:Literal></button> <%--FB 2907--%><%--ZD 100176--%> <%--ZD 100420--%>
                                <asp:Button ID="btnManage" OnClick="ManageMCUOrder" runat="server" style="display:none;" Width="310px"  /> <%--FB 1920 FB 2050--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>MCU Resource Allocation Report</SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td align="left">               <%--FB 2907--%>
                                <asp:Button ID="btnMCUAllocationReport" OnClick="GenerateReport" runat="server" CssClass="altLongBlueButtonFormat" Text="<%$ Resources:WebResources, ManageBridge_btnMCUAllocationReport%>" width="310px" OnClientClick="javascript:DataLoading('1');" /><%--FB 2094 FB 2050--%> <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                               <%-- <SPAN class=subtitleblueblodtext>Create New MCU</SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td align="left">               <%--FB 2907--%>
                                <asp:Button ID="btnNewMCU" OnClick="CreateNewMCU" runat="server" Text="<%$ Resources:WebResources, ManageBridge_btnNewMCU%>" Width="310px" OnClientClick="javascript:DataLoading('1');" /><%--FB 2094 FB2050 FB 2796 FB 2907--%> <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
<%--            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" class="tableHeader" align="center">3</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Check Resource Availability</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="Button1" OnClick="GenerateReport" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit" />
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr style="display:none">
                <td align="center">
                    <asp:DropDownList ID="lstBridgeType" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                    <asp:DropDownList ID="lstBridgeStatus" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
  <input type="hidden" id="Bridges" name="Bridges" width="200">

<img src="keepalive.asp" alt="keepalive"  name="myPic" width="1px" height="1px" style="display:none"> <%--ZD 100419--%>
    </form>
<%--FB 1491 Start--%>
<script language="javascript">
    document.getElementById("Bridges").value = document.getElementById("<%=txtBridges.ClientID %>").value;

    //ZD 100420 Start

    if (document.getElementById('btnManageOrder') != null)
        document.getElementById('btnManageOrder').setAttribute("onblur", "fnManageOrderFocus();");
    if (document.getElementById('btnMCUAllocationReport') != null)
        document.getElementById('btnMCUAllocationReport').setAttribute("onblur", "document.getElementById('btnNewMCU').focus(); document.getElementById('btnNewMCU').setAttribute('onfocus', '');");

    function fnManageOrderFocus() {
        if (document.getElementById('BridgeList') != null) {
            document.getElementById('BridgeList').setAttribute("onfocus", "");
            document.getElementById('BridgeList').focus();
        }
        else {
            document.getElementById('btnMCUAllocationReport').focus();
            document.getElementById('btnMCUAllocationReport').setAttribute('onfocus', '');
        }
    }
    //ZD 100420 End

</script>
<%--FB 1491 End--%>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    function EscClosePopup() {
            cancelthis();
    }
</script>
<%--ZD 100428 END--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

