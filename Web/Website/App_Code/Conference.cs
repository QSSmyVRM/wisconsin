//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Conference
    {
        int externalDatabaseType;
        DataTable masterDT;
        DataTable masterRec;
        DataTable masterAV;
        DataTable masterRoom;
        DataTable masterParty;
        String configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DataTable dtZone;
        DataView dvZone;
        DataTable dtUser;
        DataTable masterCust;//ZD 102909

        public Conference(int external, DataTable masterDataTable, DataTable masterDatatableRecur, DataTable masterDatatableparty, DataTable masterDatatableroom
            , DataTable masterDatatableAvparams, DataTable masterDatatableCustOpt, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            masterRec = masterDatatableRecur;
            masterParty = masterDatatableparty;
            masterRoom = masterDatatableroom;
            masterAV = masterDatatableAvparams;
            masterCust = masterDatatableCustOpt;//ZD 102909
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        #region Process

        public String[] Process(ref int cnt,  ref Boolean allConferenceimport, ref DataTable dterror)
        {

            String[] trueval1 = { "no" };
            if (masterDT.Rows.Count > 0)
            {
                String[] Failedconferecne = new String[((masterDT.Rows.Count + 1) * 3)];
                int arr = 0, conferenceno = 0;
                DataRow errow = null;
                String[] trueval = { "yes" };
                String Conf_name = null;
                DataRow dr;
                DataRow drNext;
                XmlDocument failedlist = new XmlDocument();
                XmlNodeList nodelist2;
                XmlDocument user_list = new XmlDocument();
                XmlDocument user_list1 = new XmlDocument();
                XmlDocument Room_List = new XmlDocument();
                XmlNode newnode = null;
                String outXML = "";
                int sDur = 0, tDur = 0;
                int setup_temp = 0, Tear_temp = 0, ConferenceType = 2;

                String[] hostid1 = new String[3];

                for (int j = 0; j <= masterDT.Rows.Count - 1; j++)
                {
                    try
                    {
                        #region BasicDetails
                        dr = masterDT.Rows[j];

                        if (dr["Conf_No"].ToString() == "")
                            break;//ZD 101714

                        errow = dterror.NewRow();
                        drNext = masterDT.Rows[j];
                        string inxml = "";
                        setup_temp = 0;
                        Tear_temp = 0; sDur = 0; tDur = 0;
                        String Conferencetype_Temp = null, Description = null, Public = null, Open_Registration = null, recurrance = null;
                        String recurrance_string = null;
                        int[] weeklyday = new int[7];
                        int Restrictnwpro = 0, AudioCodeCs = 0, VideoCodecs = 0, avcount = 0;
                        int Conf_no_Temp = 0, restrict = 0;
                        String Roomname = null, Restrict_Network_Access = null, Single_dail_in = null;
                        String email = null, Fname = null, Lname = null;
                        String startdate = null, starttime = null, lstConferenceTZ = null, tdzone = null;
                        conferenceno = Int32.Parse(dr["Conf_No"].ToString());
                        String videocodecs = null, audiocodecs = null, DualStream = null, Restrict_Usage = null, Encryption = null;
                        String[] Recurr = new String[10];
                        string EntityCode = null;//ZD 102909

                        if (dr["Conference Name"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Conference Name.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Conference Host"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Host Name.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Conference Requestor"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Requestor Name.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Start Date (MM/DD/YYYY)"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Start Date.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        string pattern = @"[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]";
                         Regex check = new Regex(pattern);

                        if (dr["Start Time (hh:mm tt)"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Start Time.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        else if (!check.IsMatch(dr["Start Time (hh:mm tt)"].ToString(), 0))
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Please enter valid Start Time.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Duration (Minutes)"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Duration.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Time Zone"].ToString() == "")
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Timezone.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        if (dr["Conference Type"].ToString() == "" || dr["Conference Type"].ToString().ToLower().Trim().Contains("none"))
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Conference doesn't have the Conference Type.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        String Enablerecurrence = "";

                        if (HttpContext.Current.Session["enableRecurrance"] != null)
                            Enablerecurrence = HttpContext.Current.Session["enableRecurrance"].ToString();

                        if (Enablerecurrence != null)
                        {
                            if (Enablerecurrence == "0")
                            {
                                if (dr["Recurrence (Yes/No)"].ToString().ToLower() == "yes")
                                {
                                    errow["Row No"] = conferenceno.ToString();
                                    errow["Reason"] = obj.GetTranslatedText("Recurring conferences have been disabled by the administrator.");
                                    allConferenceimport = false;
                                    dterror.Rows.Add(errow);
                                    continue;
                                }
                            }
                        }

                        Conferencetype_Temp = dr["Conference Type"].ToString().ToLower();
                        if (Conferencetype_Temp.Contains("video"))
                            ConferenceType = 2;
                        else if (Conferencetype_Temp.Contains("audio"))
                            ConferenceType = 6;
                        else if (Conferencetype_Temp.Contains("room"))
                            ConferenceType = 7;
                        else if (Conferencetype_Temp.Contains("hotdesking") || Conferencetype_Temp.Contains("desking"))
                            ConferenceType = 8;
                        else if (Conferencetype_Temp.Contains("obtp"))
                            ConferenceType = 9;
                        else
                            ConferenceType = 4;

                        inxml += "<conference>";
                        inxml += obj.OrgXMLElement();
                        inxml += "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                        inxml += " <requestorID>" + dr["Conference Requestor"] + "</requestorID>";
                        inxml += " <confHost>" + dr["Conference Host"].ToString() + "</confHost>";

                        inxml += "  <confInfo>";
                        inxml += "<confID>new</confID>";
                        Conf_name = dr["Conference Name"].ToString();
                        if (ConferenceType == 8)
                            inxml += "      <confName>Hotdesking Reservation</confName>";//ZD 103095
                        else
                            inxml += "      <confName>" + dr["Conference Name"].ToString() + "</confName>";


                        inxml += "<timeCheck></timeCheck>";
                        inxml += "<confOrigin>0</confOrigin>";
                        inxml += "<isVMR>0</isVMR>";
                        inxml += "<isRoomVMRPINChange></isRoomVMRPINChange>";
                        inxml += "<CloudConferencing>0</CloudConferencing>";
                        inxml += "<isPCconference>0</isPCconference>";
                        inxml += "<pcVendorId>0</pcVendorId>";
                        inxml += "<StartMode>0</StartMode>";
                        inxml += "<MeetandGreetBuffer>24</MeetandGreetBuffer>";
                        inxml += "<Secured>1</Secured>";
                        inxml += "<EnableNumericID>0</EnableNumericID>";
                        inxml += "<CTNumericID></CTNumericID>";
                        inxml += "<WebExConf>0</WebExConf>";
                        inxml += "<WebExConfPW></WebExConfPW>";
                        inxml += "<GoogleGUID></GoogleGUID>";
                        inxml += "<GoogleSequence>2</GoogleSequence>";
                        inxml += "      <IcalID></IcalID>";
                        inxml += "<EnableStaticID>0</EnableStaticID>";
                        inxml += "<MeetingId>1</MeetingId>";
                        inxml += "<StaticID></StaticID>";

                        if (ConferenceType == 2 || ConferenceType == 6 || ConferenceType == 9)
                        {
                            inxml += "<McuSetupTime>" + HttpContext.Current.Session["McuSetupTime"].ToString() + "</McuSetupTime>";
                            inxml += "<MCUTeardonwnTime>" + HttpContext.Current.Session["MCUTeardonwnTime"].ToString() + "</MCUTeardonwnTime>"; 
                        }
                        else
                        {
                            inxml += "<McuSetupTime>0</McuSetupTime>";
                            inxml += "<MCUTeardonwnTime>0</MCUTeardonwnTime>";
                        }

                        inxml += "<confPassword>" + dr["Password"].ToString() + "</confPassword>";

                        inxml += "<createBy>" + ConferenceType + "</createBy>";
                        Description = dr["Description"].ToString();
                        inxml += "<description>" + Description + "</description>";
                        Public = dr["Public (Yes/No)"].ToString();
                        if ((Public.ToLowerInvariant()).Contains("yes"))
                            inxml += "      <publicConf>1</publicConf>";
                        else
                            inxml += "      <publicConf>0</publicConf>";
                        Open_Registration = dr["Open For Registration (Yes/No)"].ToString();
                        if ((Open_Registration.ToLowerInvariant()).Contains("yes"))
                            inxml += "      <dynamicInvite>1</dynamicInvite>";
                        else
                            inxml += "      <dynamicInvite>0</dynamicInvite>";
                        inxml += "		<ServiceType></ServiceType>";
                        recurrance = dr["Recurrence (Yes/No)"].ToString();

                        if (recurrance == "")
                            recurrance = "no";

                        if ((recurrance.ToLowerInvariant()).Contains("no"))
                        {
                            inxml += "<immediate>0</immediate>";
                            startdate = dr["Start Date (MM/DD/YYYY)"].ToString();
                            String[] start_Date = startdate.Split(' ');
                            if ((dr["Setup Duration (mins)"].ToString()) != "")
                                setup_temp = Int32.Parse(dr["Setup Duration (mins)"].ToString());
                            starttime = dr["Start Time (hh:mm tt)"].ToString();
                            String[] start_time = starttime.Split(' ');
                            DateTime dStart = DateTime.Parse(start_Date[0] + " " + start_time[0] + " " + start_time[1]); //DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(start_Date[0]) + " " + start_time[0] + " " + start_time[1]);
                            //dStart = dStart.Subtract(TimeSpan.FromMinutes(setup_temp));
                            int duration = Int32.Parse(dr["Duration (Minutes)"].ToString());

                            if (duration < 15)
                            {
                                errow["Row No"] = conferenceno.ToString();
                                errow["Reason"] = obj.GetTranslatedText("Invalid Duration.Conference duration should be minimum of 15 minutes.");
                                allConferenceimport = false;
                                dterror.Rows.Add(errow);
                                continue;
                            }

                            DateTime dEnd = DateTime.Parse(start_Date[0] + " " + start_time[0] + " " + start_time[1]);
                            Int32 tearDur = 0;
                            if ((dr["Teardown Duration (mins)"].ToString()) != "")
                                Tear_temp = Int32.Parse(dr["Teardown Duration (mins)"].ToString());
                            tearDur = Tear_temp;

                            Tear_temp = duration + Tear_temp + setup_temp;

                            dEnd = dStart.AddMinutes(Tear_temp);
                            TimeSpan ts = dEnd.Subtract(dStart);
                            inxml += "<recurring>0</recurring>";
                            inxml += "<recurringText></recurringText>";
                            inxml += "<startDate>" + start_Date[0] + "</startDate>";
                            inxml += "<startHour>" + dStart.ToString("hh") + "</startHour>";
                            inxml += "<startMin>" + dStart.ToString("mm") + "</startMin>";
                            inxml += "<startSet>" + dStart.ToString("tt") + "</startSet>";
                            lstConferenceTZ = dr["Time Zone"].ToString();
                            tdzone = GetConferenceTimeZoneID(lstConferenceTZ);
                            inxml += "<timeZone>" + tdzone + "</timeZone>";
                            DateTime sDateTime = DateTime.Parse(start_Date[0] + " " + start_time[0] + " " + start_time[1]);
                            DateTime tDateTime = DateTime.Parse(start_Date[0] + " " + start_time[0] + " " + start_time[1]);
                            //duration = duration + setup_temp;
                            tDateTime = dStart.AddMinutes(duration);
                            sDateTime = sDateTime.AddSeconds(45).AddMinutes(-setup_temp);
                            tDateTime = tDateTime.AddSeconds(45).AddMinutes(tearDur);
                            inxml += "    <durationMin>" + duration + "</durationMin>";
                            inxml += "<setupDuration>" + setup_temp + "</setupDuration>"; //ZD 102536
                            inxml += "<teardownDuration>" + tearDur + "</teardownDuration>";
                            inxml += "<setupDateTime>" + sDateTime + "</setupDateTime>";
                            inxml += "<teardownDateTime>" + tDateTime + "</teardownDateTime>";

                        }
                        #endregion
                        #region Recurrance
                        else
                        {
                            inxml += "<immediate>0</immediate>";
                            sDur = 0;
                            if (dr["Setup Duration (mins)"].ToString() != "")
                                sDur = Int32.Parse(dr["Setup Duration (mins)"].ToString());
                            tDur = 0;
                            if (dr["Teardown Duration (mins)"].ToString() != "")
                                tDur = Int32.Parse(dr["Teardown Duration (mins)"].ToString());
                            inxml += "<setupDuration>" + sDur + "</setupDuration>";
                            inxml += "<teardownDuration>" + tDur + "</teardownDuration>";
                            inxml += "<setupDateTime></setupDateTime>";
                            inxml += "<teardownDateTime></teardownDateTime>";
                            inxml += "<recurring>1</recurring>";
                            lstConferenceTZ = dr["Time Zone"].ToString();
                            tdzone = GetConferenceTimeZoneID(lstConferenceTZ);
                            inxml += "<timeZone>" + tdzone + "</timeZone>";
                            for (int w = 0; w <= masterRec.Rows.Count - 1; w++)
                            {
                                dr = masterRec.Rows[w];
                                drNext = masterRec.Rows[w];
                                if (dr["Conf_No"].ToString() != "")
                                    Conf_no_Temp = Int32.Parse(dr["Conf_No"].ToString());
                                else
                                    Conf_no_Temp = 0;

                                if (conferenceno == Conf_no_Temp)
                                {
                                    recurrance_string = dr["Recurrence Pattern String"].ToString();
                                    recurrance_string = recurrance_string.Trim();
                                    Recurr = Recurrance_Splitfn(recurrance_string);
                                    break;
                                }
                            }
                            inxml += "<recurringText>" + recurrance_string + "</recurringText>";
                            inxml += "<appointmentTime>";
                            inxml += "<timeZone>" + tdzone + "</timeZone>";
                            String[] start_time_temp = Recurr[2].Split(':');
                            inxml += "<startHour>" + start_time_temp[0] + "</startHour>";
                            inxml += "<startMin>" + start_time_temp[1] + "</startMin>";
                            inxml += "<startSet>" + Recurr[1] + "</startSet>";
                            inxml += "<durationMin>" + Int32.Parse(Recurr[0]) + "</durationMin>";
                            inxml += "<setupDuration>" + sDur + "</setupDuration>";
                            inxml += "<teardownDuration>" + tDur + "</teardownDuration>";
                            inxml += "</appointmentTime>";
                            inxml += "<recurrencePattern>";
                            inxml += "   <recurType>" + Int32.Parse(Recurr[5]) + "</recurType>";
                            switch (Int32.Parse(Recurr[5]))
                            {
                                case 1:
                                    inxml += "<dailyType>" + Int32.Parse(Recurr[6]) + "</dailyType>";
                                    if (Recurr[7] != null)
                                        inxml += "<dayGap>" + Int32.Parse(Recurr[7]) + "</dayGap>";
                                    else
                                        inxml += "<dayGap>-1</dayGap>";
                                    break;
                                case 2:
                                    inxml += "<weekGap>" + Recurr[7] + "</weekGap>";
                                    inxml += "<weekDay>" + Recurr[6] + "</weekDay>";
                                    break;
                                case 3:
                                    inxml += "<monthlyType>" + Int32.Parse(Recurr[6]) + "</monthlyType>";
                                    switch (Int32.Parse(Recurr[6]))
                                    {
                                        case 1:
                                            inxml += "<monthDayNo>" + Int32.Parse(Recurr[8]) + "</monthDayNo>";
                                            inxml += "<monthGap>" + Int32.Parse(Recurr[7]) + "</monthGap>";
                                            break;
                                        case 2:
                                            inxml += "<monthWeekDayNo>" + Int32.Parse(Recurr[7]) + "</monthWeekDayNo>";
                                            inxml += "<monthWeekDay>" + Int32.Parse(Recurr[9]) + "</monthWeekDay>";
                                            inxml += "<monthGap>" + Int32.Parse(Recurr[8]) + "</monthGap>";
                                            break;
                                    }
                                    break;
                                case 4:
                                    inxml += "<yearlyType>" + Int32.Parse(Recurr[6]) + "</yearlyType>";
                                    switch (Int32.Parse(Recurr[6]))
                                    {
                                        case 1:
                                            inxml += "<yearMonth>" + Int32.Parse(Recurr[8]) + "</yearMonth>";
                                            inxml += "<yearMonthDay>" + Int32.Parse(Recurr[7]) + "</yearMonthDay>";
                                            break;
                                        case 2:
                                            inxml += "<yearMonthWeekDayNo>" + Int32.Parse(Recurr[7]) + "</yearMonthWeekDayNo>";
                                            inxml += "<yearMonthWeekDay>" + Int32.Parse(Recurr[9]) + "</yearMonthWeekDay>";
                                            inxml += "<yearMonth>" + Int32.Parse(Recurr[8]) + "</yearMonth>";
                                            break;
                                    }
                                    break;
                            }
                            inxml += "   <recurrenceRange>";
                            inxml += "<startDate>" + Recurr[3] + "</startDate>";
                            inxml += "<endType>" + Int32.Parse(Recurr[10]) + "</endType>";
                            switch (Int32.Parse(Recurr[10]))
                            {
                                case 1:
                                    inxml += "<occurrence>-1</occurrence>";
                                    break;
                                case 2:
                                    inxml += "<occurrence>" + Int32.Parse(Recurr[4]) + "</occurrence>";
                                    break;
                                case 3:
                                    inxml += "<endDate>" + Recurr[4] + "</endDate>";
                                    break;
                            }
                            inxml += "</recurrenceRange>";
                            inxml += "</recurrencePattern>";
                        }
                        #endregion

                        inxml += "    <createBy>" + ConferenceType + "</createBy>";

                        #region Room
                        String rminxml = "";
                        int rmCnt = 0;
                        for (int w = 0; w <= masterRoom.Rows.Count - 1; w++)
                        {
                            dr = masterRoom.Rows[w];
                            drNext = masterRoom.Rows[w];
                            if (dr["Conf_No"].ToString() != "")
                                Conf_no_Temp = Int32.Parse(dr["Conf_No"].ToString());
                            else
                                Conf_no_Temp = 0;
                            
                            if (Conf_no_Temp == conferenceno)
                            {
                                rmCnt = rmCnt + 1;

                                Roomname = dr["Room Name"].ToString();

                                if (rminxml == "")
                                    rminxml = Roomname;
                                else
                                    rminxml += "," + Roomname;
                            }
                        }
                        #endregion

                        if (ConferenceType == 4 && (rmCnt < 2 || rmCnt > 2))
                        {
                            errow["Row No"] = conferenceno.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Only two Endpoints can be selected for Point-To-Point Conference.");
                            allConferenceimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        inxml += "		<ConfGuestRooms>";
                        inxml += "		</ConfGuestRooms>";
                        inxml += "<ConciergeSupport>";
                        inxml += "<OnSiteAVSupport>0</OnSiteAVSupport>";
                        inxml += "<MeetandGreet>0</MeetandGreet>";
                        inxml += "<ConciergeMonitoring>0</ConciergeMonitoring>";
                        inxml += "<DedicatedVNOCOperator>0</DedicatedVNOCOperator>";
                        inxml += "<VNOCAssignAdminID>0</VNOCAssignAdminID>";
                        inxml += "<ConfVNOCOperators></ConfVNOCOperators>";
                        inxml += "</ConciergeSupport>";

                        #region AVparams
                        inxml += "<advAVParam>";
                        for (int w = 0; w <= masterAV.Rows.Count - 1; w++)
                        {
                            dr = masterAV.Rows[w];
                            drNext = masterAV.Rows[w];
                            if (dr["Conf_No"].ToString() != "")
                                Conf_no_Temp = Int32.Parse(dr["Conf_No"].ToString());
                            else
                                Conf_no_Temp = 0;
                            
                            if (conferenceno == Conf_no_Temp)
                            {
                                avcount++;
                                inxml += "<maxAudioPart>" + dr["Max Audio Ports"].ToString() + "</maxAudioPart>";
                                inxml += "<maxVideoPart>" + dr["Max Video ports"].ToString() + "</maxVideoPart>";
                                Restrict_Network_Access = dr["Restrict Network Access to"].ToString();
                                if ((Restrict_Network_Access.ToLowerInvariant()).Contains("mpi"))
                                    Restrictnwpro = 4;
                                else if ((Restrict_Network_Access.ToLowerInvariant()).Contains("sip"))
                                    Restrictnwpro = 3;
                                else if ((Restrict_Network_Access.ToLowerInvariant()).Contains("isdn"))
                                    Restrictnwpro = 2;
                                else
                                    Restrictnwpro = 1;
                                inxml += "<restrictProtocol>" + Restrictnwpro + "</restrictProtocol>";
                                Restrict_Usage = dr["Restrict Usage to"].ToString();
                                if ((Restrict_Usage.ToLowerInvariant()).Contains("video"))
                                    restrict = 2;
                                else
                                    restrict = 1;
                                inxml += "<restrictAV>" + restrict + "</restrictAV>";
                                inxml += "<videoLayout>1</videoLayout>";
                                inxml += "<maxLineRateID>" + dr["Maximum Line Rate"].ToString() + "</maxLineRateID>";
                                videocodecs = dr["Video Codecs"].ToString();
                                if (videocodecs.Contains("."))
                                {
                                    String[] videocode_tem = videocodecs.Split('.');
                                    videocodecs = videocode_tem[0] + videocode_tem[1];
                                }
                                VideoCodecs = (int)(myVRMNet.NETFunctions.videocodecs)Enum.Parse(typeof(myVRMNet.NETFunctions.videocodecs), videocodecs);
                                audiocodecs = dr["Audio Codecs"].ToString();
                                if (audiocodecs.Contains("/"))
                                {
                                    String[] audiocode_temp = audiocodecs.Split(' ', '[', '/', ']');
                                    audiocodecs = audiocode_temp[1] + audiocode_temp[2] + audiocode_temp[0];
                                }
                                else
                                    if (audiocodecs.Contains("["))
                                    {
                                        String[] audiocode_temp = audiocodecs.Split(' ', '[', ']');
                                        audiocodecs = audiocode_temp[1] + audiocode_temp[0];
                                    }
                                audiocodecs = audiocodecs.ToLowerInvariant();
                                AudioCodeCs = (int)(myVRMNet.NETFunctions.audiocodecs)Enum.Parse(typeof(myVRMNet.NETFunctions.audiocodecs), audiocodecs);
                                inxml += "<audioCodec>" + AudioCodeCs + "</audioCodec>";
                                inxml += "<videoCodec>" + VideoCodecs + "</videoCodec>";
                                DualStream = dr["Dual Stream Mode (Yes/No)"].ToString();
                                if ((DualStream.ToLowerInvariant()).Contains("yes"))
                                    inxml += "<dualStream>1</dualStream>";
                                else
                                    inxml += "<dualStream>0</dualStream>";
                                Encryption = dr["Encryption (Yes/No)"].ToString();
                                if ((Encryption.ToLowerInvariant()).Contains("yes"))
                                    inxml += "<encryption>1</encryption>";
                                else
                                    inxml += "<encryption>0</encryption>";
                                inxml += "          <lectureMode>0</lectureMode>";

                                Single_dail_in = dr["Single Dial in (Yes/No)"].ToString();
                                if ((Single_dail_in.ToLowerInvariant()).Contains("yes"))
                                    inxml += "<SingleDialin>1</SingleDialin>";
                                else
                                    inxml += "      <SingleDialin>0</SingleDialin>";
                                inxml += "      <FECCMode>0</FECCMode>";
                                inxml += "<PolycomSendMail>0</PolycomSendMail>";
                            }
                        }
                        if (avcount == 0)
                        {
                            inxml += "<maxAudioPart></maxAudioPart>";
                            inxml += "<maxVideoPart></maxVideoPart>";
                            inxml += "<restrictProtocol>1</restrictProtocol>";
                            inxml += "<restrictAV>2</restrictAV>";
                            inxml += "<videoLayout>1</videoLayout>";
                            inxml += "<maxLineRateID>384Kbps</maxLineRateID>";
                            inxml += "<audioCodec>0</audioCodec>";
                            inxml += "<videoCodec>0</videoCodec>";
                            inxml += "<dualStream>0</dualStream>";
                            inxml += "<encryption>0</encryption>";
                            inxml += "<SingleDialin>0</SingleDialin>";
                            inxml += "          <lectureMode>0</lectureMode>";
                            inxml += "      <FECCMode>0</FECCMode>";
                            inxml += "<PolycomSendMail>0</PolycomSendMail>";

                        }
                        inxml += "    </advAVParam>";
                        #endregion

                        #region Participant

                        String partyInxml = "";

                        for (int y = 0; y <= masterParty.Rows.Count - 1; y++)
                        {
                            dr = masterParty.Rows[y];
                            drNext = masterParty.Rows[y];
                            if (dr["Conf_No"].ToString() != "")
                                Conf_no_Temp = Int32.Parse(dr["Conf_No"].ToString());
                            else
                                Conf_no_Temp = 0;
                            
                            String[] usrid = new String[3];
                            int userid = -1;
                            if (conferenceno == Conf_no_Temp)
                            {
                                email = dr["Email"].ToString();
                                Fname = dr["First Name"].ToString();
                                Lname = dr["Last Name"].ToString();

                                if (partyInxml == "")
                                    partyInxml = Fname + "," + Lname + "," + email;
                                else
                                    partyInxml += "|" + Fname + "," + Lname + "," + email;

                            }
                        }
                        #endregion

                        inxml += "      <ModifyType>0</ModifyType>";
                        inxml += "      <fileUpload>";
                        inxml += "          <file></file>";
                        inxml += "          <file></file>";
                        inxml += "          <file></file>";
                        inxml += "      </fileUpload>";

                        //ZD 102909 - Start
                        inxml += "<CustomAttributesList>";
                        #region EntityCode 
                        if (masterCust != null && masterCust.Rows.Count > 0)
                        {
                            for (int z = 0; z <= masterCust.Rows.Count - 1; z++)
                            {
                                dr = masterCust.Rows[z];                                
                                if (!string.IsNullOrEmpty(dr["Conf_No"].ToString()))
                                    Conf_no_Temp = int.Parse(dr["Conf_No"].ToString());
                                else
                                    Conf_no_Temp = 0;

                                if (conferenceno == Conf_no_Temp)
                                {
                                    EntityCode = dr["Entity Code"].ToString();
                                    inxml += "<CustomAttribute>";
                                    inxml += "<OptionValue>" + EntityCode + "</OptionValue>";
                                    inxml += "</CustomAttribute>";
                                }
                            }
                        }
                        #endregion                        
                        inxml += "</CustomAttributesList>";
                        //ZD 102909 - End

                        inxml += "<isExchange>0</isExchange>";
                        inxml += "<overBookConf>0</overBookConf>";
                        inxml += "<isExpressConference>0</isExpressConference>";

                        inxml += "</confInfo>";

                        inxml += "<Rooms>" + rminxml + "</Rooms>";
                        inxml += "<Party>" + partyInxml + "</Party>";

                        inxml += SetEndpoints();

                        inxml += "</conference>";

                        outXML = obj.CallMyVRMServer("SetConferenceFromDateImport", inxml, configPath);
                        log.Trace("SetConferenceFromDateImport OutXML: " + outXML);

                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            allConferenceimport = false;
                            failedlist.LoadXml(outXML);
                            newnode = failedlist.SelectSingleNode("//error/message");
                            nodelist2 = failedlist.SelectNodes("//error/message");
                            if (nodelist2.Count > 0)
                            {
                                errow["Row No"] = conferenceno.ToString();
                                errow["Reason"] = newnode.InnerXml;
                                dterror.Rows.Add(errow);
                            }
                        }
                        else //ZD 102663 Starts
                        {
                            if (outXML.IndexOf("<Info>") >= 0) //ZD 102909
                            {                                
                                failedlist.LoadXml(outXML);
                                newnode = failedlist.SelectSingleNode("//Info/message");
                                nodelist2 = failedlist.SelectNodes("//Info/message");
                                if (nodelist2.Count > 0)
                                {
                                    errow["Row No"] = conferenceno.ToString();
                                    errow["Reason"] = newnode.InnerXml;
                                    dterror.Rows.Add(errow);
                                }
                            }

                            if (HttpContext.Current.Application["External"].ToString() != "")
                            {
                                failedlist = null;
                                failedlist = new XmlDocument();
                                failedlist.LoadXml(outXML);

                                // Retriving the confid
                                newnode = failedlist.SelectSingleNode("//conferences/conference/confID");
                                if (newnode != null)
                                {
                                    try
                                    {
                                        String inEXML = "";
                                        inEXML = "<SetExternalScheduling>";
                                        inEXML += "<confID>" + newnode.InnerText + "</confID>";
                                        inEXML += "</SetExternalScheduling>";

                                        String outExml = obj.CallCommand("SetExternalScheduling", inEXML);

                                    }
                                    catch (Exception)
                                    {

                                        // We do nothing as this is a failure to external user. Must alert the user in future.
                                    }

                                }
                            }
                        }
                        //ZD 102663 Ends
                    }
                    catch (Exception ex)
                    {
                        Failedconferecne[arr] = conferenceno.ToString();
                        arr++;
                        Failedconferecne[arr] = ex.ToString();
                        arr++;
                        Failedconferecne[arr] = Conf_name;
                        arr++;
                    }
                }
                if (arr != 0)
                {
                    return Failedconferecne;
                }
                return trueval;
            }

            return trueval1;

        }
        #endregion

        #region SetEndpoints
        protected string SetEndpoints()
        {
            string inXML = "";

            inXML = "<SetAdvancedAVSettings>";
            inXML += "<isExchange>0</isExchange>";
            inXML += "<editFromWeb>1</editFromWeb>";
            inXML += obj.OrgXMLElement();
            inXML += " <language>en</language>";
            inXML += " <isVIP>0</isVIP>";
            inXML += "      <isDedicatedEngineer>0</isDedicatedEngineer>";

            inXML += "      <ConfMessageList></ConfMessageList>";

            inXML += "  <AVParams>";

            inXML += "      <isLiveAssitant>0</isLiveAssitant>";
            inXML += "<EnableNumericID>0</EnableNumericID>";
            inXML += "<CTNumericID></CTNumericID>";
            inXML += "<EnableStaticID>0</EnableStaticID>";
            inXML += "<StaticID></StaticID>";
            inXML += "      <isReminder>0</isReminder>";
            inXML += "      <SingleDialin>0</SingleDialin>";
            inXML += "  </AVParams>";
            inXML += "</SetAdvancedAVSettings>";

            return inXML;
        }
        #endregion

        protected String GetConferenceTimeZoneID(String tZoneStr)
        {

            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "";
            String tZone = "";
            String[] tZoneArr = null;
            try
            {
                //code added for BCS - start
                //Time Zone
                String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
                String zoneOutXML;
                tZone = tZoneStr;
                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();
                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        obj = null;
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                        //code added for BCS - start
                    }
                }
                if (tZone != "")
                {
                    tZoneArr = tZone.Split(' ');
                    int tzonelength = tZoneArr.Length;
                    String t1 = null, t2 = null;
                    if (tzonelength == 1)
                    {
                        t1 = tZoneArr[0];
                    }
                    else
                    {
                        t1 = tZoneArr[0];
                        t2 = tZoneArr[1];
                    }
                    foreach (DataRow row in dtZone.Rows)
                    {
                        if (tzonelength == 1)
                        {
                            if (row["timezoneName"].ToString().Contains(t1))
                            {

                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }
                        else
                        {
                            if (row["timezoneName"].ToString() == tZone || row["timezoneName"].ToString().Contains(tZone))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                            //else if (row["StandardName"].ToString().Contains(t1) && row["StandardName"].ToString().Contains(t2))
                            //{
                            //    tZoneID = row["timezoneID"].ToString();
                            //    break;
                            //}
                            //else if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                            //{
                            //    tZoneID = row["timezoneID"].ToString();
                            //    break;
                            //}
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tZoneID;
        }

        protected String GetConferenceTimeZoneID1(String tZoneStr)
        {

            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "33";
            String tZone = "";
            String[] tZoneArr = null;
            try
            {
                //code added for BCS - start
                //Time Zone
                String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
                String zoneOutXML;
                tZone = tZoneStr;
                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();
                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        obj = null;
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                        //code added for BCS - start
                    }
                }
                if (tZone != "")
                {
                    tZoneArr = tZone.Split(' ');
                    int tzonelength = tZoneArr.Length;
                    String t1 = null, t2 = null;
                    if (tzonelength == 1)
                    {
                        t1 = tZoneArr[0];
                    }
                    else
                    {
                        t1 = tZoneArr[0];
                        t2 = tZoneArr[1];
                    }
                    foreach (DataRow row in dtZone.Rows)
                    {
                        if (tzonelength == 1)
                        {
                            if (row["timezoneName"].ToString().Contains(t1))
                            {

                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }
                        else
                        {
                            if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }

                    }


                    //foreach (String s in tZoneArr)
                    //{
                    //    if (s.ToString().ToUpper() == "GMT")
                    //    {
                    //        tZoneID = "31";
                    //        break;
                    //    }
                    //    else if (s.Contains("GMT"))
                    //    {
                    //        if (dtZone.Rows.Count > 0)
                    //        {
                    //            foreach (DataRow row in dtZone.Rows)
                    //            {
                    //                if (row["timezoneName"].ToString().Contains(s))
                    //                {

                    //                    {
                    //                        tZoneID = row["timezoneID"].ToString();
                    //                        break;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tZoneID;
        }

       
        private String[] GetConfUserID(XmlNodeList nodes, String email1)
        {
            String[] a = new String[4];
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("email").InnerText.Trim().Equals(email1.Trim()))
                    {
                        a[0] = node.SelectSingleNode("userID").InnerText;
                        a[1] = node.SelectSingleNode("firstName").InnerText;
                        a[2] = node.SelectSingleNode("lastName").InnerText;
                        //a[3] = node.SelectSingleNode("accountexpiry").InnerText;
                    }
                    return a;
                }
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private String[] Recurrance_Splitfn(String recurrancestring)
        {
            int str_len = 0, str = 0, len = 0, temp = 0;
            int Durration_mins = 0, Duration_recurrance = 0;
            String[] recurrance_split = new String[11];
            String[] subrecurr = new String[10];
            String[] ret = { "true" };
            String[] Recurrance_stringsplit1 = recurrancestring.Split(' ');
            str_len = Recurrance_stringsplit1.Length;
            String[] Recurrance_stringsplit = new String[str_len];
            for (int l = 0; l < str_len; l++)
            {
                if (Recurrance_stringsplit1[l] != "")
                {
                    Recurrance_stringsplit[str] = Recurrance_stringsplit1[l].ToString();
                    str++;
                }
                else
                    len++;
            }
            temp = str_len - (len + 1);
            if ((Recurrance_stringsplit[temp].ToLowerInvariant()).Contains("mins"))
            {
                temp = temp - 1;
                Durration_mins = Int32.Parse(Recurrance_stringsplit[temp]);
                temp = temp - 1;
            }
            Duration_recurrance = Int32.Parse(Recurrance_stringsplit[temp - 1]);
            recurrance_split[0] = ((Duration_recurrance * 60) + Durration_mins).ToString();
            temp = temp - 3;
            recurrance_split[1] = Recurrance_stringsplit[temp];
            temp = temp - 1;
            recurrance_split[2] = Recurrance_stringsplit[temp];
            temp = temp - 2;
            if (Recurrance_stringsplit[temp] != "time(s)")
            {
                if (Recurrance_stringsplit[temp - 1] != "until")
                {
                    recurrance_split[3] = Recurrance_stringsplit[temp];
                    subrecurr = subrecur(Recurrance_stringsplit, temp);
                    recurrance_split[5] = subrecurr[0];
                    recurrance_split[6] = subrecurr[1];
                    recurrance_split[7] = subrecurr[2];
                    recurrance_split[8] = subrecurr[3];
                    recurrance_split[9] = subrecurr[4];
                    recurrance_split[10] = "1";
                }
                else
                {
                    recurrance_split[4] = Recurrance_stringsplit[temp];
                    temp = temp - 2;
                    recurrance_split[3] = Recurrance_stringsplit[temp];
                    subrecurr = subrecur(Recurrance_stringsplit, temp);
                    recurrance_split[5] = subrecurr[0];
                    recurrance_split[6] = subrecurr[1];
                    recurrance_split[7] = subrecurr[2];
                    recurrance_split[8] = subrecurr[3];
                    recurrance_split[9] = subrecurr[4];
                    recurrance_split[10] = "3";
                }
            }
            else
            {
                temp = temp - 1;
                recurrance_split[4] = Recurrance_stringsplit[temp];
                temp = temp - 2;
                recurrance_split[3] = Recurrance_stringsplit[temp];
                subrecurr = subrecur(Recurrance_stringsplit, temp);
                recurrance_split[5] = subrecurr[0];
                recurrance_split[6] = subrecurr[1];
                recurrance_split[7] = subrecurr[2];
                recurrance_split[8] = subrecurr[3];
                recurrance_split[9] = subrecurr[4];
                recurrance_split[10] = "2";
            }

            return recurrance_split;
        }
        private String[] subrecur(String[] Recurrance_stringsplit, int temp)
        {
            String[] sub_split = new String[10];
            int[] weeklyday = new int[7];
            String MonthTempDay = null;
            int k = 0;
            string tmpStr = string.Empty;
            if (Recurrance_stringsplit[temp - 2] == "day(s)")
            {
                sub_split[0] = "1";
                sub_split[1] = "1";
                temp = temp - 3;
                sub_split[2] = Recurrance_stringsplit[temp];
            }
            else
                if (Recurrance_stringsplit[temp - 2] == "weekday")
                {
                    sub_split[0] = "1";
                    sub_split[1] = "2";
                }
                else if (Recurrance_stringsplit[temp - 2] == "month(s)")
                {
                    if (temp != 9)
                    {
                        sub_split[0] = "3";
                        sub_split[1] = "1";
                        temp = temp - 3;
                        sub_split[2] = Recurrance_stringsplit[temp];
                        temp = temp - 3;
                        sub_split[3] = Recurrance_stringsplit[temp];
                    }
                    else
                    {
                        sub_split[0] = "3";
                        sub_split[1] = "2";
                        temp = temp - 3;
                        sub_split[3] = Recurrance_stringsplit[temp];
                        temp = temp - 3;
                        MonthTempDay = Recurrance_stringsplit[temp];
                        sub_split[4] = ((int)(myVRMNet.NETFunctions.DaysType)Enum.Parse(typeof(myVRMNet.NETFunctions.DaysType), MonthTempDay)).ToString();
                        temp = temp - 1;
                        MonthTempDay = Recurrance_stringsplit[temp];
                        sub_split[2] = ((int)(myVRMNet.NETFunctions.DayCount)Enum.Parse(typeof(myVRMNet.NETFunctions.DayCount), MonthTempDay)).ToString();
                    }
                }
                else
                    if (temp == 5)
                    {
                        temp = temp - 2;
                        sub_split[0] = "4";
                        sub_split[1] = "1";
                        sub_split[2] = Recurrance_stringsplit[temp];
                        temp = temp - 1;
                        MonthTempDay = Recurrance_stringsplit[temp];
                        sub_split[3] = ((int)(myVRMNet.NETFunctions.MonthNames)Enum.Parse(typeof(myVRMNet.NETFunctions.MonthNames), MonthTempDay)).ToString();
                    }
                    else
                        if (temp == 7 && Recurrance_stringsplit[temp - 3] == "of")
                        {
                            sub_split[0] = "4";
                            sub_split[1] = "2";
                            temp = temp - 2;
                            MonthTempDay = Recurrance_stringsplit[temp];
                            sub_split[3] = ((int)(myVRMNet.NETFunctions.MonthNames)Enum.Parse(typeof(myVRMNet.NETFunctions.MonthNames), MonthTempDay)).ToString();
                            temp = temp - 2;
                            MonthTempDay = Recurrance_stringsplit[temp];
                            sub_split[4] = ((int)(myVRMNet.NETFunctions.DaysType)Enum.Parse(typeof(myVRMNet.NETFunctions.DaysType), MonthTempDay)).ToString();
                            temp = temp - 1;
                            MonthTempDay = Recurrance_stringsplit[temp];
                            sub_split[2] = ((int)(myVRMNet.NETFunctions.DayCount)Enum.Parse(typeof(myVRMNet.NETFunctions.DayCount), MonthTempDay)).ToString();
                        }
                        else
                        {
                            sub_split[0] = "2";
                            temp = temp - 2;
                            do
                            {
                                if (Recurrance_stringsplit[temp] != "and")
                                {

                                    MonthTempDay = Recurrance_stringsplit[temp];
                                    if (MonthTempDay.IndexOf(',') > 0)
                                        MonthTempDay = MonthTempDay.Substring(0, MonthTempDay.Length - 1);
                                    weeklyday[k] = (int)(myVRMNet.NETFunctions.WeekDays)Enum.Parse(typeof(myVRMNet.NETFunctions.WeekDays), MonthTempDay);
                                    k++;

                                }
                                temp--;
                            }
                            while (Recurrance_stringsplit[temp] != "on");
                            for (int x = 0; x < k; x++)
                            {
                                if (x == 0)
                                    tmpStr = weeklyday[0].ToString();
                                else
                                    tmpStr += "," + weeklyday[x].ToString();
                            }
                            temp = temp - 2;
                            sub_split[1] = tmpStr;
                            sub_split[2] = Recurrance_stringsplit[temp];
                        }


            return sub_split;

        }
    }

}
