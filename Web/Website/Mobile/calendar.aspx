﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End ZD 100866-->
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="calendar.aspx.cs" Inherits="Droid_calendar" %>

<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>

<%
    if (Session["userID"] == null)
    {
        Response.Redirect("thankyou.aspx");
    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Calendar</title>
    <script type="text/javascript" src="script/scrollbutton.js"></script>

    <script type="text/javascript">
    function fnLogout()
    {
        window.location.assign('thankyou.aspx');
        return true;
    }
    
    function fnConf(arg1,arg2)
    {
        if(arg1 == "B")
            window.location.assign('conference.aspx');
        else if(arg1 == "T")
        {
            var time = arg2.toString(); 
            var conftime = new Date(time);                        
            var hdnDayDate = document.getElementById("hdnDayDate");
            var hour,min;
            var splttim = conftime.toTimeString().split(':');
            var timeFormat;            
            
            hour = splttim[0];
            min = splttim[1];

            timeFormat = '<%=timeFormat%>';
            if(timeFormat == "1")
            {
                hour = conftime.getHours();
                min = conftime.getMinutes();

                if(eval(hour) < 10)
                    hour = "0" + hour

                if(eval(min) < 10)
                    min = "0" + min

                min = min + " " + conftime.format("tt"); 
            }
            
            DataLoadingNew("1");
            window.location.assign('conference.aspx?sd=' + hdnDayDate.value + '&st=' + hour + ':' + min);               
        }
        
        return true;
    }

    function fnView()
    {
        var args = fnView.arguments;
        var DayScheduleRow = document.getElementById("DayScheduleRow");
        var WeekScheduleRow = document.getElementById("WeekScheduleRow");
        var MonthScheduleRow = document.getElementById("MonthScheduleRow");
        var hdnView = document.getElementById("hdnView");
        var CalHeaderTextDay = document.getElementById("CalHeaderTextDay");
        var CalHeaderTextWeek = document.getElementById("CalHeaderTextWeek");
        var CalHeaderTextMonth = document.getElementById("CalHeaderTextMonth");
        var hdnWeekScrollEnable = document.getElementById("hdnWeekScrollEnable");
        
        DayScheduleRow.style.display = 'none';
        WeekScheduleRow.style.display = 'none';
        MonthScheduleRow.style.display = 'none';
        CalHeaderTextDay.style.display = 'none';
        CalHeaderTextWeek.style.display = 'none';
        CalHeaderTextMonth.style.display = 'none';
        
        hdnView.value = args[0];
        
        if(args[0] == 1)
        {
           DayScheduleRow.style.display = 'block';
           CalHeaderTextDay.style.display = 'block';
        }
        else if(args[0] == 2)
        {
           WeekScheduleRow.style.display = 'block';
           CalHeaderTextWeek.style.display = 'block';
           
           if(hdnWeekScrollEnable.value == "")
           {
               scroll_2nd_div_week();
               hdnWeekScrollEnable.value = "1";
           }
        }
        else if(args[0] == 3)
        {
           MonthScheduleRow.style.display = 'block';
           CalHeaderTextMonth.style.display = 'block';
        }  
        
        return false;
    }

    function DataLoadingNew(val)
    {   
        var hdnView = document.getElementById("hdnView")
        
        if (val=="1")
        {
            if(hdnView.value == "1")
                document.getElementById("dataLoadingDIV").innerHTML="<img border='0' src='image/wait1Old.gif' width='100' height='8'>";
            else if(hdnView.value == "2")
                document.getElementById("dataLoadingDIVWeek").innerHTML="<img border='0' src='image/wait1Old.gif' width='100' height='8'>";
        }
        else
        {
            if(hdnView.value == "1")
                document.getElementById("dataLoadingDIV").innerHTML="";
            else if(hdnView.value == "2")
                document.getElementById("dataLoadingDIVWeek").innerHTML="";
        }
    }
    
    function fnShowHide(arg,e)
    {        
        if (arg == '1' || arg == '2' || arg == '3')
        {
            document.getElementById("PlaceHolder").style.display = 'block';
            document.getElementById("PlaceHolder1").style.display = 'none';
            if(arg == '1')
                document.getElementById("dd").innerHTML = e.tag()[1];
            else if(arg == '2')
                document.getElementById("dd").innerHTML = e.tag()[2];
            else if(arg == '3')
                document.getElementById("dd").innerHTML = e.tag();
        }
        else if (arg == '0')
        {
            document.getElementById("PlaceHolder").style.display = 'none';
            document.getElementById("PlaceHolder1").style.display = 'block';
            document.getElementById("dd").innerHTML = "";            
        }
        return false; 
    }
    
    function fnNext()
    {
        //window.location.hash = '#dataLoadingDIV'; //This is also working to focus on DIV
        var hdnView = document.getElementById("hdnView")
        
        if(hdnView.value == "1")
        {
            document.getElementById('dataLoadingDIV').scrollIntoView()            
            document.getElementById('btnNext').click();
        }
        else if(hdnView.value == "2")
        {
            document.getElementById('dataLoadingDIVWeek').scrollIntoView()            
            document.getElementById('btnNextWeek').click();
        }   
        return false;
    }
    
     function fnPrevious()
    {
        //window.location.hash = '#dataLoadingDIV'; //This is also working to focus on DIV
        document.getElementById('dataLoadingDIV').scrollIntoView()
         if(hdnView.value == "1")
         {
            document.getElementById('dataLoadingDIV').scrollIntoView() 
            document.getElementById('btnPrevious').click();
         }
        else if(hdnView.value == "2")
        {
            document.getElementById('dataLoadingDIVWeek').scrollIntoView() 
            document.getElementById('btnPreviousWeek').click();
        }
        return false;
    }
    
    </script>

    <style type="text/css">
        .btnCorner
        {
            border: 1px solid #a1a1a1;
            border-radius: 5px;
        }
        
        .wrapper1
        {
            width:320px;
            height:0px;
            overflow:hidden;
        }
        
        .scroller1{
            height:1000px;
            width:1000px;
            overflow:hidden;
        }
        
        #schDaypilotweek div table td[class*="calendar_silver_cornerright"] /* :nth-child(3) */
        {
        	display:none;
        }
        
​
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600"></asp:ScriptManager>
    <input type="hidden" id="HdnMonthlyXml" runat="server" />
    <input type="hidden" id="IsMonthChanged" runat="server" />
    <input type="hidden" id="IsWeekOverLap" runat="server" />
    <input type="hidden" id="IsWeekChanged" runat="server" />
    <input type="hidden" id="hdnView" runat="server" />
    <input type="hidden" id="hdnWeekScrollEnable" runat="server" />
    <div id="PlaceHolder" runat="server" align="center" style="top: 0px; left: 0px;
        position: absolute; width: 310px; height: 450px; visibility: visible; z-index: 3;
        display: none">
        <table cellpadding="2" cellspacing="1" width="100%" class="tableBody" align="center" style="background-color:lightgray;">
            <tr>
                <td width="9%">
                </td>
                <td align="left">
                    <div style="width: 100%; height:200px; overflow: auto;" id="dd" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="BtnClose" Text="Back" CssClass="btnCorner" runat="server" OnClientClick="javascript:return fnShowHide('0')"></asp:Button>
                </td>
            </tr>
        </table>
    </div>
    <div id="PlaceHolder1" runat="server">
        <table width="100%" >
            <tr>
                <td>
                    <table style="background-color:White; table-layout:fixed; z-index:1000; margin-top:-15px;" width="100%" >
                        <tr>
                            <td>
                                <br />
                                <img id="ImagePathTopForCalendar"  alt="" src=""  /><br /><%--ZD 103301--%>
                                <input id="btnLogout" class="btnCorner" type="button" value="Log out" onclick="fnLogout();" />
                                <input id="Button1" class="btnCorner" type="button" value="Create Conference" onclick="fnConf('B');" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input id="btnDay" class="btnCorner" type="submit" value="Day" onclick="javascript:return fnView(1);" />
                                <input id="btnWeek" class="btnCorner" type="submit" value="Week" onclick="javascript:return fnView(2);" />
                                <input id="btnMonth" class="btnCorner" type="submit" value="Month" onclick="javascript:return fnView(3);" style="display:none;" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                    <input type="hidden" id="hdnMonthDate" runat="server" />      
                    <table width="19%">
                        <tr>
                            <td>
                                <table width="100%" >
                                    <tr>
                                        <td align="left">
                                            <asp:ImageButton ID="Previous" runat="server" ImageUrl="~/en/image/prevpage.gif"
                                                ToolTip="Previous" OnClientClick="return fnPrevious();" />
                                        </td>
                                        <td align="center" nowrap>
                                            <asp:UpdatePanel runat="server" ID="uplDetail" UpdateMode="Conditional">
                                               <ContentTemplate>
                                                    <asp:Label ID="CalHeaderTextDay" runat="server" Style="display: none;"></asp:Label>
                                                    <asp:Label ID="CalHeaderTextWeek" runat="server" Style="display: none;"></asp:Label>
                                                    <asp:Label ID="CalHeaderTextMonth" runat="server" Style="display: none;"></asp:Label>
                                               </ContentTemplate>
                                           </asp:UpdatePanel>
                                            
                                        </td>
                                        <td align="right">
                                            <asp:ImageButton ID="Next" runat="server" ImageUrl="~/en/image/nextpage.gif" ToolTip="Next"
                                                OnClientClick="javascript:return fnNext();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="DayScheduleRow" runat="server">
                            <td>
                                <div id='scrollbutton2_div'  style='border: 1px solid black; overflow: auto; width: 350px; height: auto;' >
                                <asp:UpdatePanel ID="PanelTab" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="dataLoadingDIV" align="center" ></div>
                                        <input type="hidden" id="hdnDayDate" runat="server" />
                                        <div id="divDay" runat="server">
                                        <DayPilot:DayPilotCalendar ID="schDaypilot" runat="server" Days="1" Visible="true" 
                                            Width="350" DataStartField="start" DataEndField="end" DataTextField="confDetails"
                                            DataValueField="ConfID" DataTagFields="ConferenceType,CustomDescription" HeaderFontSize="8pt"
                                            HeaderHeight="17" CellDuration="30" BackColor="Gray" HourNameBackColor="lightGray"
                                            NonBusinessBackColor="Gray" ShowToolTip="false" EventClickHandling="JavaScript"
                                            EventClickJavaScript="fnShowHide(1,e);" EventFontSize="8pt" ClientObjectName="dps1"
                                            TimeRangeSelectedHandling="JavaScript"
                                            UseEventBoxes="Never" TimeRangeSelectedJavaScript="fnConf('T',start)" /> <%-- ZD 103034 --%>
                                        </div>                               
                                        <asp:Button ID="btnNext" style="display:none;" runat="server" OnClick="NextCal" OnClientClick="DataLoadingNew('1');"/>
                                        <asp:Button ID="btnPrevious" style="display:none;" runat="server" OnClick="PreviousCal" OnClientClick="DataLoadingNew('1');"/>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr id="WeekScheduleRow" runat="server" style="display: none;">
                            <td>
                                <div id='scrollbutton2_div_week'  style='border: 1px solid black; overflow: auto; width: 350px; height: auto;' >
                                    <asp:UpdatePanel ID="UpdatePanelWeek" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div id="dataLoadingDIVWeek" align="center" ></div>
                                            <input type="hidden" id="hdnWeekDate" runat="server" />
                                            <DayPilot:DayPilotCalendar CssClass="tableHeader" ID="schDaypilotweek" runat="server"
                                                Days="7"  Width="350" DataStartField="start" DataEndField="end" DataTextField="confDetails"
                                                DataValueField="ConfID" DataTagFields="ConferenceType,ConfID,CustomDescription"
                                                HeaderFontSize="8pt" HeaderHeight="27" CellDuration="30" 
                                                ShowToolTip="false" EventClickHandling="JavaScript"
                                                EventClickJavaScript="fnShowHide(2,e);" EventFontSize="8pt" CssClassPrefix="calendar_silver_"
                                                NonBusinessBackColor="Gray" ClientObjectName="dps1" UseEventBoxes="Never" BackColor="Gray"
                                                AllDayEventBackColor="lightGray" HourNameBackColor="lightGray" />  <%-- ZD 103034 --%> 
                                                
                                            <asp:Button ID="btnNextWeek" style="display:none;" runat="server" OnClick="NextCal" OnClientClick="DataLoadingNew('1');"/>
                                            <asp:Button ID="btnPreviousWeek" style="display:none;" runat="server" OnClick="PreviousCal" OnClientClick="DataLoadingNew('1');"/>  
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>                                      
                            </td>
                        </tr>
                        <tr id="MonthScheduleRow" runat="server" style="display: none;">
                            <td>
                                <div id="DayPilotCalendarMonth" style="height:150px;">
                                    <DayPilot:DayPilotMonth CssClass="tableHeader" ID="schDaypilotMonth" runat="server"
                                        Visible="false" Width="250" DataStartField="start" DataEndField="end" DataTextField="confDetails"
                                        DataValueField="ConfID" DataTagFields="ConferenceType,CustomDescription" 
                                        HeaderFontSize="8pt" HeaderHeight="17" 
                                        EventClickHandling="JavaScript" EventClickJavaScript="fnShowHide(3,e);" JavaScript="fnShowHide(3,e);"
                                        EventFontSize="10" NonBusinessBackColor="Gray" ClientObjectName="dps1" 
                                        ShowToolTip="false" BackColor="Gray" CellHeaderBackColor="lightGray" HeaderBackColor="lightGray" />                                            
                                </div>
                            </td>
                        </tr>
                    </table>
                        
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
 <script type="text/javascript">
                                                                              
    function scroll_2nd_div()
    {
        if (document.getElementById('scrollbutton2_div').offsetHeight == 0)
        {
            setTimeout(function (){ scroll_2nd_div(); }, 250);
            return;
        }

        scroll_opt=0; scroll_pos=1;
        var scrollbutton2_div = document.getElementById("scrollbutton2_div");
        //scrollbutton('scrollbutton2_div');
    }
    
    function scroll_2nd_div_week()
    {
        if (document.getElementById('scrollbutton2_div_week').offsetHeight == 0)
        {
            setTimeout(function (){ scroll_2nd_div(); }, 250);
            return;
        }

        scroll_opt=0; scroll_pos=1;
        var scrollbutton2_div = document.getElementById("scrollbutton2_div_week");
        //scrollbutton('scrollbutton2_div_week');
    }
    var hdnView = document.getElementById("hdnView");

    if(hdnView && hdnView.value == "1")
        scroll_2nd_div();
    if(hdnView && hdnView.value == "2")
        scroll_2nd_div_week();
    
    //ZD 103301 START
    var obj = document.getElementById('ImagePathTopForCalendar');
    obj.src = "..\\en\\Organizations\\Org_" + '<%=Session["organizationID"]%>' + "\\CSS\\Mirror\\Image\\lobbytop1024.jpg";
    //ZD 103301 END
</script>