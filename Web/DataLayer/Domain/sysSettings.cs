/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;

namespace myVRM.DataLayer
{
    #region sysData
    /// <summary>
	/// Summary description for system settings.
	/// (this is a static class as system settings dont change);
	/// </summary>
	public class sysData
	{
        /* ** This class is modified as per Organization Module ** */

		#region Private Internal Members

        public static readonly int SUPER_ADMIN_ID = 11;
        public static readonly int PHANTOM_ROOM_ID = 11;

		private DateTime m_LastModified;
		private int m_Admin;
		private string m_SecurityKey;
        private int m_superAdmin, m_TimeZone;
		private string m_foodsecuritykey, m_resourcesecuritykey;
		private string m_emaildisclaimer, m_license;
        private int m_LastModifiedUser;
		private DateTime m_ExpiryDate;
        private int m_RoomLimit, m_MCULimit, m_UserLimit, m_MaxGuests, m_ExtRoomLimit, m_MCUEnchancedLimit, m_Cloud;//FB 2426 //FB 2486//FB 2426 //FB 2262 //FB 2599 
					//m_MaxGroups,m_MaxTemplates, - Depricated as per new license

        private List<String> m_CpuID; //Activation module
        private List<String> m_MacId;
        private int m_IsDemo;
        private DateTime m_Activated;
        //System LOGO..
        private int m_SiteLogoId;
        private string m_Companymessage;
        private int m_StdBannerId;
        private int m_HighBannerId;
        private int m_LoginBackgroundId, m_LoginBackgroundNormalId; // FB 2719 //ZD 103581
        //Merging for License START
        private int m_MaxOrganizations, m_IsLDAP, m_MaxNVidRooms, m_MaxVidRooms, m_EnablePublicRooms, m_MaxVMRooms, //FB 2594 //FB 2586
                    m_MaxEndPts, m_MaxCDR, m_MaxGstPerUsrs,
                    m_MaxExcUsrs, m_MaxDomUsrs,m_MaxMobUsrs, m_MaxFacilities, m_MaxAPIs, m_MaxCatering, m_MaxWebexUsrs, //FB 1979 //ZD 100221
                    m_MaxHousekeeping, // m_MaxPCModule, //FB 2347 //FB 2693
                    m_MaxSpanish, m_MaxMandarin, m_MaxHindi, m_MaxFrench;
        //Merging for License END
        private int m_LaunchBuffer;//FB 2007
        private int m_EnableLaunchBufferP2P; //FB 2437
        private int m_StartMode; //FB 2501
        private int m_EnableCloudInstallation;//FB 2659
        private string m_EM7URI, m_EM7Username, m_EM7Password;//FB 2501 EM7
        private int m_EM7Port; //FB 2501 EM7
        private int m_IndividualOrgExpiry, m_SessionTimeout, m_AutoPurgeLogDuration;//FB 2678 // ZD 101011 //ZD 104846
        private int m_MemcacheEnabled; //103496
		private int m_ConciergeSupport;//FB 2670
        private int m_ViewPublicConf;//FB 2858
        private int m_ReqUsrAcc;//ZD 101846
		private int m_MaxPCUsers, m_MaxJabber, m_MaxLync, m_MaxVidtel; //FB 2693 //ZD 104021
		private int m_MaxROHotdesking, m_MaxVCHotdesking;//FB 2694
        private int m_MaxAdvancedReport;//FB 2593
        private int m_EnableNetworkFeatures, m_roomCascadingControl;//FB 2993 //ZD 100263
        private string m_GoogleClientID, m_GoogleSecretID, m_GoogleAPIKey, m_AdminEmailaddress; //ZD 100152 //ALLDEV-856
        private int m_PushtoGoogle, m_PollCount, m_GoogleIntegration;//ZD 100152 //ALLDEV-856
        private int m_ConfIDStartValue;//ZD 100526
        private int m_MaxiControlRooms;//ZD 101098
        private int m_ConfRecurLimit, m_DisplayRoomCalLink;//ALLDEV-644//ZD 101837
        private int m_MaxBlueJeansUsers;//ZD 104021 -Blue Jeasns user license supports unlimited, so unlimited is referred by "-2"
		#endregion

		#region Public Properties

		public DateTime LastModified
		{
			get {return m_LastModified;}
			set {m_LastModified = value;}
		}
		public int Admin
		{
			get {return m_Admin;}
			set {m_Admin = value;}
		}	
		public string SecurityKey
		{
			get {return m_SecurityKey;}
			set {m_SecurityKey = value;}	
		}	
		public int superAdmin 
		{
			get {return m_superAdmin;}
			set {m_superAdmin = value;}
		}
        public int TimeZone 
		{
            get { return m_TimeZone; }
            set { m_TimeZone = value; }
		}
		public string foodsecuritykey 
		{
			get {return m_foodsecuritykey;}
			set {m_foodsecuritykey = value;}
		}
		public string resourcesecuritykey 
		{
			get {return m_resourcesecuritykey;}
			set {m_resourcesecuritykey = value;}
		}
		public string emaildisclaimer
		{
			get {return m_emaildisclaimer;}
			set {m_emaildisclaimer = value;}
		}
		public string license
		{
			get {return m_license;}
			set {m_license = value;}
		}
        public int LastModifiedUser
		{
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
		}	
		public  DateTime ExpiryDate
		{
			get {return m_ExpiryDate;}
			set {m_ExpiryDate = value;}
		}
		public int RoomLimit
		{
			get {return m_RoomLimit;}
			set {m_RoomLimit = value;}
		}
		public int MCULimit 
		{
			get {return m_MCULimit;}
			set {m_MCULimit = value;}
		}
        public int MCUEnchancedLimit //FB 2486
        {
            get { return m_MCUEnchancedLimit; }
            set { m_MCUEnchancedLimit = value; }
        }
        
		public int UserLimit 
		{
			get {return m_UserLimit;}
			set {m_UserLimit = value;}
		}
        public int ExtRoomLimit //FB 2426
        {
            get { return m_ExtRoomLimit; }
            set { m_ExtRoomLimit = value; }
        }
        //FB 2599 Start
        public int Cloud //FB 2262 - J
        {
            get { return m_Cloud; }
            set { m_Cloud = value; }

        }
        //FB 2599 End
        //System LOGO..
        public int SiteLogoId
        {
            get { return m_SiteLogoId; }
            set { m_SiteLogoId = value; }
        }
        public string Companymessage
        {
            get { return m_Companymessage; }
            set { m_Companymessage = value; }
        }
        public int StdBannerId
        {
            get { return m_StdBannerId; }
            set { m_StdBannerId = value; }
        }
        public int HighBannerId
        {
            get { return m_HighBannerId; }
            set { m_HighBannerId = value; }
        }

        // FB 2719 Starts
        public int LoginBackgroundId
        {
            get { return m_LoginBackgroundId; }
            set { m_LoginBackgroundId = value; }
        }
        // FB 2719 Ends
        //ZD 103581 - Start
        public int LoginBackgroundNormalId
        {
            get { return m_LoginBackgroundNormalId; }
            set { m_LoginBackgroundNormalId = value; }
        }
        //ZD 103581 - End
        //FB 2007
        public int LaunchBuffer
        {
            get { return m_LaunchBuffer; }
            set { m_LaunchBuffer = value; }
        }
        //FB 2007
        public int EnableLaunchBufferP2P //FB 2437
        {
            get { return m_EnableLaunchBufferP2P; }
            set { m_EnableLaunchBufferP2P = value; }
        }
        public int IndividualOrgExpiry //FB 2678
        {
            get { return m_IndividualOrgExpiry; }
            set { m_IndividualOrgExpiry = value; }
        }
		//103496	
        public int MemcacheEnabled 
        {
            get { return m_MemcacheEnabled; }
            set { m_MemcacheEnabled = value; }
        }
        public int SessionTimeout // ZD 101011
        {
            get { return m_SessionTimeout; }
            set { m_SessionTimeout = value; }
        }
        public int ViewPublicConf //FB 2858
        {
            get { return m_ViewPublicConf; }
            set { m_ViewPublicConf = value; }
        }
        //FB 2501 starts
        public int StartMode
        {
            get { return m_StartMode; }
            set { m_StartMode = value; }
        }
        
        //FB 2659 Starts
        public int EnableCloudInstallation
        {
            get { return m_EnableCloudInstallation; }
            set { m_EnableCloudInstallation = value; }
        }
        //FB 2659 Ends

        //FB 2501 EM7 Starts
        public string EM7URI
        {
            get { return m_EM7URI; }
            set { m_EM7URI = value; }
        }
        public string EM7Username
        {
            get { return m_EM7Username; }
            set { m_EM7Username = value; }
        }
        public string EM7Password
        {
            get { return m_EM7Password; }
            set { m_EM7Password = value; }
        }
         public int EM7Port
        {
            get { return m_EM7Port; }
            set { m_EM7Port = value; }
        }
        //FB 2501 EM7 Ends

          //FB 2670  start
         public int  ConciergeSupport
        {
            get { return m_ConciergeSupport; }
            set { m_ConciergeSupport = value; }

        }
        //FB 2670  start End
        //public int MaxGroups - Depricated as per new license
        //{
        //    get {return m_MaxGroups;}
        //    set {m_MaxGroups = value;}
        //}
        //public int MaxTemplates
        //{
        //    get {return m_MaxTemplates;}
        //    set {m_MaxTemplates = value;}
        //}
		public int MaxGuests
		{
			get {return m_MaxGuests;}
			set {m_MaxGuests = value;}
		}
        /** Code added for Activation -- Start **/

        public List<String> CPUID
        {
            get { return m_CpuID; }
            set { m_CpuID = value; }
        }
        public List<String> MacID
        {
            get { return m_MacId; }
            set { m_MacId = value; }
        }

        public Int32 IsDemo
        {
            get { return m_IsDemo; }
            set { m_IsDemo = value; }
        }

        public DateTime ActivatedDate
        {
            get { return m_Activated; }
            set { m_Activated = value; }
        }

        /** Code added for Activation -- End **/
        //License modification START
        public int MaxOrganizations
        {
            get { return m_MaxOrganizations; }
            set { m_MaxOrganizations = value; }
        }
        public int EnablePublicRooms //FB 2594
        {
            get { return m_EnablePublicRooms; }
            set { m_EnablePublicRooms = value; }
        }
        public int IsLDAP
        {
            get { return m_IsLDAP; }
            set { m_IsLDAP = value; }
        }
        public int MaxNVidRooms
        {
            get { return m_MaxNVidRooms; }
            set { m_MaxNVidRooms = value; }
        }
        public int MaxVidRooms
        {
            get { return m_MaxVidRooms; }
            set { m_MaxVidRooms = value; }
        }
        //FB 2586 Start
        public int MaxVMRooms
        {
            get { return m_MaxVMRooms; }
            set { m_MaxVMRooms = value; }
        }
        //FB 2586 End
        public int MaxEndPts
        {
            get { return m_MaxEndPts; }
            set { m_MaxEndPts = value; }
        }
        public int MaxCDR
        {
            get { return m_MaxCDR; }
            set { m_MaxCDR = value; }
        }
        public int MaxGstPerUsrs
        {
            get { return m_MaxGstPerUsrs; }
            set { m_MaxGstPerUsrs = value; }
        }
        public int MaxExcUsrs
        {
            get { return m_MaxExcUsrs; }
            set { m_MaxExcUsrs = value; }
        }
        public int MaxDomUsrs
        {
            get { return m_MaxDomUsrs; }
            set { m_MaxDomUsrs = value; }
        }
        public int MaxMobUsrs //FB 1979
        {
            get { return m_MaxMobUsrs; }
            set { m_MaxMobUsrs = value; }
        }
        public int MaxWebexUsrs //ZD 100221
        {
            get { return m_MaxWebexUsrs; }
            set { m_MaxWebexUsrs = value; }
        }

        public int MaxBlueJeansUsers //ZD 104021
        {
            get { return m_MaxBlueJeansUsers; }
            set { m_MaxBlueJeansUsers = value; }
        }
        public int MaxFacilities
        {
            get { return m_MaxFacilities; }
            set { m_MaxFacilities = value; }
        }
        public int MaxCatering
        {
            get { return m_MaxCatering; }
            set { m_MaxCatering = value; }
        }
        public int MaxHousekeeping
        {
            get { return m_MaxHousekeeping; }
            set { m_MaxHousekeeping = value; }
        }
        public int MaxAPIs
        {
            get { return m_MaxAPIs; }
            set { m_MaxAPIs = value; }
        }
        public int MaxSpanish
        {
            get { return m_MaxSpanish; }
            set { m_MaxSpanish = value; }
        }
        public int MaxMandarin
        {
            get { return m_MaxMandarin; }
            set { m_MaxMandarin = value; }
        }
        public int MaxHindi
        {
            get { return m_MaxHindi; }
            set { m_MaxHindi = value; }
        }
        public int MaxFrench
        {
            get { return m_MaxFrench; }
            set { m_MaxFrench = value; }
        }
        //public int MaxPCModule //FB 2347 //FB 2693
        //{
        //    get { return m_MaxPCModule; }
        //    set { m_MaxPCModule = value; }
        //}
        //License modification END
        //FB 2693 Starts
        public int MaxJabber
        {
            get { return m_MaxJabber; }
            set { m_MaxJabber = value; }
        }
        public int MaxLync
        {
            get { return m_MaxLync; }
            set { m_MaxLync = value; }
        }
        public int MaxVidtel
        {
            get { return m_MaxVidtel; }
            set { m_MaxVidtel = value; }
        }
        public int MaxPCUsers
        {
            get { return m_MaxPCUsers; }
            set { m_MaxPCUsers = value; }
        }
        //FB 2693 Ends
		//FB 2694 Starts
        public int MaxROHotdesking
        {
            get { return m_MaxROHotdesking; }
            set { m_MaxROHotdesking = value; }
        }
        public int MaxVCHotdesking 
        {
            get { return m_MaxVCHotdesking; }
            set { m_MaxVCHotdesking = value; }
        }
        //FB 2694 - End
        //FB 2593 - Start
        public int MaxAdvancedReport
        {
            get { return m_MaxAdvancedReport; }
            set { m_MaxAdvancedReport = value; }
        }
        //FB 2593 - End
        //FB 2993 Start
        public int EnableNetworkFeatures
        {
            get { return m_EnableNetworkFeatures; }
            set { m_EnableNetworkFeatures = value; }
        }
        //2993 End
        public int roomCascadingControl //ZD 100263
        {
            get { return m_roomCascadingControl; }
            set { m_roomCascadingControl = value; }
        }
		//ZD 100152 Starts
        public string GoogleClientID
        {
            get { return m_GoogleClientID; }
            set { m_GoogleClientID = value; }
        }
        public string GoogleSecretID
        {
            get { return m_GoogleSecretID; }
            set { m_GoogleSecretID = value; }
        }
        public string GoogleAPIKey
        {
            get { return m_GoogleAPIKey; }
            set { m_GoogleAPIKey = value; }
        }
        public int PushtoGoogle
        {
            get { return m_PushtoGoogle; }
            set { m_PushtoGoogle = value; }
        }
        //ZD 100152 Ends
        //ALLDEV-856 Start
        public int GoogleIntegration
        {
            get { return m_GoogleIntegration; }
            set { m_GoogleIntegration = value; }
        }
        public string AdminEmailaddress
        {
            get { return m_AdminEmailaddress; }
            set { m_AdminEmailaddress = value; }
        }
        public int PollCount
        {
            get { return m_PollCount; }
            set { m_PollCount = value; }
        }
        //ALLDEV-856 End
        //ZD 100526
        public int ConfIDStartValue
        {
            get { return m_ConfIDStartValue; }
            set { m_ConfIDStartValue = value; }
        }

        //ZD 101098 Start
        public int MaxiControlRooms
        {
            get { return m_MaxiControlRooms; }
            set { m_MaxiControlRooms = value; }
        }
        //ZD 101098 End

        //ZD 101837 - Start
        public int ConfRecurLimit
        {
            get { return m_ConfRecurLimit; }
            set { m_ConfRecurLimit = value; }
        }
        //ZD 101837 - End
        //ZD 101846 - Start
        public int ReqUsrAcc
        {
            get { return m_ReqUsrAcc; }
            set { m_ReqUsrAcc = value; }
        }
        //ZD 101846 - End
        
        //ZD 104846 start
        public int AutoPurgeLogDuration
        {
            get {return m_AutoPurgeLogDuration;}
            set {m_AutoPurgeLogDuration =  value;}

        }
        //ZD 104846 End

        //ALLDEV-644 start
        public int DisplayRoomCalLink
        {
            get { return m_DisplayRoomCalLink; }
            set { m_DisplayRoomCalLink = value; }
        }
        //ALLDEV-644 End
		#endregion	
    }
    #endregion

    #region sysSettings

    public class sysSettings 
	{
		private static string m_configPath;
		private static sysData m_sysData;
      
		public sysSettings(string configPath)
		{


            try
            {

                m_configPath = configPath;
                m_sysData = new sysData();
                Init(m_configPath);
            }
            catch (Exception e)
            {
                throw e;
            }
		}
		
		public static bool Init(string configPath)
		{
			try
			{
                if (m_sysData == null || m_sysData.ExpiryDate <= DateTime.MinValue) //ZD 104095
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    m_sysData = (sysData)session.Load(typeof(sysData), 11);

                    ParseLicenseXML();
                }
            }
			catch (Exception e)
			{
				throw e;
			}
			return true;
        }
        public static bool Init(string configPath, bool isReloadValues) //ZD 104095
        {
            try
            {
                if (isReloadValues)
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    m_sysData = (sysData)session.Load(typeof(sysData), 11);

                    ParseLicenseXML();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }


        #region ParseLicenseXML
        /// <summary>
        /// ParseLicenseXML
        /// </summary>
        private static void ParseLicenseXML()
        {
            // Load the string into xml parser and retrieve the tag values.
            XmlDocument xd = new XmlDocument();
            cryptography.Crypto crypto = new cryptography.Crypto();
            string templicense = crypto.decrypt(license);

            xd.LoadXml(templicense);
            XmlNode node;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ExpirationDate");
            string ExpirationDate = node.InnerXml.Trim();
            m_sysData.ExpiryDate = DateTime.Parse(ExpirationDate, CultureInfo.InvariantCulture);  // FB 1774

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/MaxOrganizations");
            m_sysData.MaxOrganizations = Int32.Parse(node.InnerXml.Trim());

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/Cloud");//FB 2599
            int maxCloud = 0;
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out maxCloud);
            m_sysData.Cloud = maxCloud;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/PublicRoomService"); //FB 2594
            int enablePublicRooms = 0;
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out enablePublicRooms);
            m_sysData.EnablePublicRooms = enablePublicRooms;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/IsLDAP");
            //if (node.InnerText.Trim().ToLower() == "enabled")
            if (node.InnerText.Trim().Equals("1"))
                m_sysData.IsLDAP = 1;
            else
                m_sysData.IsLDAP = 0;

            /** Code added for Activation -- Start **/

            
            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ServerActivation/ProcessorIDs");

            m_sysData.CPUID = new List<string>();

            if (node != null)
            {

                XmlNodeList nodes = node.SelectNodes("ID");

                if (nodes != null)
                {


                    foreach (XmlNode nde in nodes)
                        m_sysData.CPUID.Add(nde.InnerText);
                }
            }


            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ServerActivation/MACAddresses");

            m_sysData.MacID = new List<string>();

            if (node != null)
            {
                XmlNodeList nodes = node.SelectNodes("ID");

                if (nodes != null)
                {


                    foreach (XmlNode nde in nodes)
                        m_sysData.MacID.Add(nde.InnerText);
                }
            }

            /** Code added for Activation -- End **/

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxNonVideoRooms");
            int maxnvrm = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxnvrm);
            m_sysData.MaxNVidRooms = maxnvrm;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVideoRooms");
            int maxvrm = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxvrm);
            m_sysData.MaxVidRooms = maxvrm;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVMRRooms");//FB 2586
            int maxvmr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxvmr);
            m_sysData.MaxVMRooms = maxvmr;

            //ZD 101098 START
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxiControlRooms");//FB 2586
            int maxicontrol = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxicontrol);
            m_sysData.MaxiControlRooms = maxicontrol;
            //ZD 101098 END

            m_sysData.RoomLimit = m_sysData.MaxNVidRooms + m_sysData.MaxVidRooms + m_sysData.MaxVMRooms + m_sysData.MaxiControlRooms; //FB 2586 //ZD 101098

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxGuestRooms"); //FB 2426
            int maxextrooms = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxextrooms);
            m_sysData.ExtRoomLimit = maxextrooms;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxStandardMCUs"); 
            int maxmcu = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxmcu);
            m_sysData.MCULimit = maxmcu;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEnhancedMCUs");//FB 2486
            int maxenchamcu = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxenchamcu);
            m_sysData.MCUEnchancedLimit = maxenchamcu;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEndpoints");
            int maxep = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxep);
            m_sysData.MaxEndPts = maxep;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxCDRs");
            int maxcdr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxcdr);
            m_sysData.MaxCDR = maxcdr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxTotalUsers");
            int maxusers = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxusers);
            m_sysData.UserLimit = maxusers;
                       
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxGuestsPerUser");
            int maxGuest = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxGuest);
            m_sysData.MaxGstPerUsrs = maxGuest;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxExchangeUsers");
            int maxexusr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxexusr);
            m_sysData.MaxExcUsrs = maxexusr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxDominoUsers");
            int maxDominousr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxDominousr);
            m_sysData.MaxDomUsrs = maxDominousr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxMobileUsers"); //FB 1979
            int maxMobileusr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxMobileusr);
            m_sysData.MaxMobUsrs = maxMobileusr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxWebexUsers"); //ZD 100221
            int maxWebexusr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxWebexusr);
            m_sysData.MaxWebexUsrs = maxWebexusr;

            //FB 2693 Starts
            int maxPCUser = 0;
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxPCUsers");
            Int32.TryParse(node.InnerXml.Trim(), out maxPCUser);
            m_sysData.MaxPCUsers = maxPCUser;
            //FB 2693 Ends

            //ZD 104021 start
            int maxBJ=0;
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxBlueJeansUsers");
            if (node.InnerXml.Trim().ToUpper() == "UL")
                maxBJ = -2; //Referring Unlimited license as -2
            else
            {
                Int32.TryParse(node.InnerXml.Trim(), out maxBJ);
                if (maxBJ<0) maxBJ = 0;
            }
            m_sysData.MaxBlueJeansUsers = maxBJ;
            //ZD 104021 end

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxFacilitiesModules");
            int maxav = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxav);
            m_sysData.MaxFacilities = maxav;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxAPIModules");
            int maxapi = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxapi);
            m_sysData.MaxAPIs = maxapi;
            
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxCateringModules");
            int maxcat = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxcat);
            m_sysData.MaxCatering = maxcat;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxHousekeepingModules");
            int maxhkg = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxhkg);
            m_sysData.MaxHousekeeping = maxhkg;

            //FB 2693 Starts 
            int maxJabber, maxLync, maxVidtel = 0; //ZD 104021
            
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxJabber");
            int.TryParse(node.InnerXml.Trim(), out maxJabber);
            m_sysData.MaxJabber = maxJabber;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxLync");
            int.TryParse(node.InnerXml.Trim(), out maxLync);
            m_sysData.MaxLync = maxLync;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxVidtel");
            if (node != null) //ZD 102004
                int.TryParse(node.InnerXml.Trim(), out maxVidtel);
            m_sysData.MaxVidtel = maxVidtel;

            //FB 2593 Start
            int MaxAdvReport = 0;
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxAdvancedReport");
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out MaxAdvReport);
            m_sysData.MaxAdvancedReport = MaxAdvReport;
            //FB 2593 - End

            //node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxVidyo");
            //int.TryParse(node.InnerXml.Trim(), out maxVidyo);
            //m_sysDat.E = maxVidyo;

            ////Commented for FB 2693
            //node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxPCModules"); //FB 2347
            //int MaxPC = 0;
            //Int32.TryParse(node.InnerXml.Trim(), out MaxPC);
            //m_sysData.MaxPCModule = MaxPC;
            //FB 2693 Ends

            //FB 2694 - Starts
            int maxVCHotRoom = 0;
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVCHotdesking");
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out maxVCHotRoom);
            m_sysData.MaxVCHotdesking = maxVCHotRoom;

            int maxROHotRoom = 0;
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxROHotdesking");
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out maxROHotRoom);
            m_sysData.MaxROHotdesking = maxROHotRoom;
            //FB 2694 - End

            //<Languages> moved out of <Module> tag during FB 2693 Starts
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Languages/MaxSpanish");
            int maxspanish = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxspanish);
            m_sysData.MaxSpanish = maxspanish;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Languages/MaxMandarin");
            int maxMandarin = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxMandarin);
            m_sysData.MaxMandarin = maxMandarin;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Languages/MaxHindi");
            int MaxHindi = 0;
            Int32.TryParse(node.InnerXml.Trim(), out MaxHindi);
            m_sysData.MaxHindi = MaxHindi;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Languages/MaxFrench");
            int MaxFrench = 0;
            Int32.TryParse(node.InnerXml.Trim(), out MaxFrench);
            m_sysData.MaxFrench = MaxFrench;
            //<Languages> moved out of <Module> tag during FB 2693 Ends
        }
        #endregion

        #region getSysMail
        public bool getSysMail(ref sysMailData mailData)
        {
            try
            {
                SessionManagement.GetSession(m_configPath);
                ISession session = SessionManagement.GetSession(m_configPath);
                mailData =(sysMailData)session.Load(typeof(sysMailData), 1);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion 

        #region Public Properties

        public static DateTime LastModified{get {return m_sysData.LastModified;}}
		public static int Admin{get  {return m_sysData.Admin;}}
		public static string SecurityKey{get  {return m_sysData.SecurityKey;}}
		public static int superAdmin{get  {return m_sysData.superAdmin;}}
        public static int TimeZone { get { return m_sysData.TimeZone; } }
		public static string foodsecuritykey{get  {return m_sysData.foodsecuritykey;}}
		public static string resourcesecuritykey{get  {return m_sysData.resourcesecuritykey;}}
		public static string emaildisclaimer{get  {return m_sysData.emaildisclaimer;}}
		public static string license{get  {return m_sysData.license;}}
        public static int LastModifiedUser { get { return m_sysData.LastModifiedUser; } }
		/// <summary>
		/// Summary description for License information.
		/// (this is a static class as system settings dont change);
		/// </summary>
		public static DateTime ExpiryDate{get {return m_sysData.ExpiryDate;}}
		public static int RoomLimit{get {return m_sysData.RoomLimit;}}
		public static int MCULimit{get {return m_sysData.MCULimit;}}
        public static int MCUEnchancedLimit { get { return m_sysData.MCUEnchancedLimit; } }//FB 2486
        public static int UserLimit{get {return m_sysData.UserLimit;}}
        public static int ExtRoomLimit { get { return m_sysData.ExtRoomLimit; } } //FB 2426
        public static int Cloud { get { return m_sysData.Cloud; } } //FB 2262 - J //FB 2599
        //public static int MaxGroups{get {return m_sysData.MaxGroups;}} - Depricated as per new license
        //public static int MaxTemplates{get {return m_sysData.MaxTemplates;}}
		public static int MaxGuests{get {return m_sysData.MaxGuests;}}

        /** Code added for Activation -- Start **/
        public static int IsDemo { get { return m_sysData.IsDemo; } }
        public static List<String> CPUID { get { return m_sysData.CPUID; } }
        public static List<String> MACID { get { return m_sysData.MacID; } }
        public static DateTime ActivatedDate { get { return m_sysData.ActivatedDate; } }
        /** Code added for Activation -- End **/
        //Merging for License modification START
        public static int MaxOrganizations { get { return m_sysData.MaxOrganizations; } }
        public static int EnablePublicRooms { get { return m_sysData.EnablePublicRooms; } } //FB 2594
        public static int IsLDAP { get { return m_sysData.IsLDAP; } }
        public static int MaxNVidRooms { get { return m_sysData.MaxNVidRooms; } }
        public static int MaxVidRooms { get { return m_sysData.MaxVidRooms; } }
        public static int MaxROHotdesking { get { return m_sysData.MaxROHotdesking; } } //FB 2694
        public static int MaxVCHotdesking { get { return m_sysData.MaxVCHotdesking; } } //FB 2694
        public static int MaxEndPts { get { return m_sysData.MaxEndPts; } }
        public static int MaxCDR { get { return m_sysData.MaxCDR; } }
        public static int MaxGstPerUsrs { get { return m_sysData.MaxGstPerUsrs; } }
        public static int MaxExcUsrs { get { return m_sysData.MaxExcUsrs; } }
        public static int MaxDomUsrs { get { return m_sysData.MaxDomUsrs; } }
        public static int MaxMobUsrs { get { return m_sysData.MaxMobUsrs; } } //FB 1979
        public static int MaxWebexUsr { get { return m_sysData.MaxWebexUsrs; } } //ZD 100221
        //public static int MaxWebexUsr { get { return m_sysData.MaxWebexUsrs; } } //ZD 104021
        public static int MaxBlueJeansUsers { get { return m_sysData.MaxBlueJeansUsers; } }//ZD 104021
        public static int MaxFacilities { get { return m_sysData.MaxFacilities; } }
        public static int MaxHousekeeping { get { return m_sysData.MaxHousekeeping; } }
        public static int MaxCatering { get { return m_sysData.MaxCatering; } }
        public static int MaxAPIs { get { return m_sysData.MaxAPIs; } }
        public static int MaxSpanish { get { return m_sysData.MaxSpanish; } }
        public static int MaxMandarin { get { return m_sysData.MaxMandarin; } }
        public static int MaxHindi { get { return m_sysData.MaxHindi; } }
        public static int MaxFrench { get { return m_sysData.MaxFrench; } }
        //public static int MaxPCModule { get { return m_sysData.MaxPCModule; } } //FB 2347 //FB 2693
        public static int MaxVMRooms { get { return m_sysData.MaxVMRooms; } } //FB 2586
        public static int MaxiControlRooms { get { return m_sysData.MaxiControlRooms; } } //ZD 101098
        //FB 2693 Starts
        public static int MaxJabber { get { return m_sysData.MaxJabber; } }
        public static int MaxLync { get { return m_sysData.MaxLync; } }
        public static int MaxVidtel { get { return m_sysData.MaxVidtel; } }
        public static int MaxPCUsers { get { return m_sysData.MaxPCUsers; } } 
        //FB 2693 Ends
        public static int MaxAdvancedReport { get { return m_sysData.MaxAdvancedReport; } }//FB 2593
        //Merging for License modification END
        //System LOGO..
        public static int SysData { get { return m_sysData.SiteLogoId; } }
        public static string m_Companymessage { get { return m_sysData.Companymessage; } }
        public static int StdBannerId { get { return m_sysData.StdBannerId; } }
        public static int HighBannerId { get { return m_sysData.HighBannerId; } }

        public static int LaunchBuffer { get { return m_sysData.LaunchBuffer; } }//FB 2007
        public static int EnableLaunchBufferP2P { get { return m_sysData.EnableLaunchBufferP2P; } }//FB 2437
        public static int StartMode { get { return m_sysData.StartMode; } } //FB 2051
        public static int EnableCloudInstallation { get { return m_sysData.EnableCloudInstallation; } } //FB 2659
        //FB 2501 EM7 Starts
		public static string EM7URI { get { return m_sysData.EM7URI; } }
        public static string EM7Username { get { return m_sysData.EM7Username; } }
        public static string EM7Password { get { return m_sysData.EM7Password; } }
        public static int EM7Port { get { return m_sysData.EM7Port; } }
		//FB 2501 EM7 Ends
        public static int IndividualOrgExpiry { get { return m_sysData.IndividualOrgExpiry; } }//FB 2678
        public static int MemcacheEnabled { get { return m_sysData.MemcacheEnabled; } } //RJ 001
        public static int ViewPublicConf { get { return m_sysData.ViewPublicConf; } }//FB 2858        
		public static int ConciergeSupport { get { return m_sysData.ConciergeSupport; } }//FB 2670 
        public static int EnableNetworkFeatures { get { return m_sysData.EnableNetworkFeatures; } }//FB 2993
        public static int roomCascadingControl { get { return m_sysData.roomCascadingControl; } }//ZD 100263
		//ZD 100152 Starts
        public static string GoogleClientID { get { return m_sysData.GoogleClientID; } }
        public static string GoogleSecretID { get { return m_sysData.GoogleSecretID; } }
        public static string GoogleAPIKey { get { return m_sysData.GoogleAPIKey; } }
        public static int PushtoGoogle { get { return m_sysData.PushtoGoogle; } }
        //ZD 100152 Ends
        public static int PollCount { get { return m_sysData.PollCount; } } //ALLDEV-856
        public static int GoogleIntegration { get { return m_sysData.GoogleIntegration; } } //ALLDEV-856
        public static string AdminEmailaddress { get { return m_sysData.AdminEmailaddress; } } //ALLDEV-856
        public static int ConfIDStartValue { get { return m_sysData.ConfIDStartValue; } }//ZD 100526
        public static int SessionTimeout { get { return m_sysData.SessionTimeout; } } // ZD 101011
        public static int ConfRecurLimit { get { return m_sysData.ConfRecurLimit; } } // ZD 101837
        public static int ReqUsrAcc { get { return m_sysData.ReqUsrAcc; } } // ZD 101846
        public static int AutoPurgeLogDuration { get { return m_sysData.AutoPurgeLogDuration; } }//ZD 104846
        public static int DisplayRoomCalLink { get { return m_sysData.DisplayRoomCalLink; } } //ALLDEV-644
        
        #endregion
    }
    #endregion

    #region sysMailData
    /// <summary>
	/// Summary description for system Email Server settings.
	/// (this is a static class as system settings dont change);
	/// </summary>
	public class sysMailData
	{
        /* *** This class is modified during organization module *** */

		#region Private Internal Members
        private int m_id;
		private	string m_websiteURL;
		private	int m_IsRemoteServer;
		private	string m_ServerAddress, m_Login, m_password;
        private int m_portNo, m_ConTimeOut;
        private string m_CompanyMail, m_displayname, m_messagetemplate;
		private DateTime m_LastRunDateTime; //Dashboard
        private int m_RetryCount,m_ServerTimeout;//FB 2552 //ZD 100317
        
		#endregion

		#region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
		public	string websiteURL
		{
			get {return m_websiteURL;}
			set { m_websiteURL = value;}
		}
		public	int IsRemoteServer
		{
			get {return m_IsRemoteServer;}
			set {m_IsRemoteServer = value;}
		}
		public	string ServerAddress
		{
			get {return m_ServerAddress;}
			set {m_ServerAddress = value;}
		}
		public	string Login
		{
			get {return m_Login;}
			set {m_Login = value;}
		}
		public	string password
		{
			get {return m_password;}
			set {m_password = value;}
		}
		public	int portNo
		{
			get {return m_portNo;}
			set { m_portNo = value; }
		}
		public	int ConTimeOut
		{
            get { return m_ConTimeOut; }
			set { m_ConTimeOut = value; }
		}
		public string displayname
		{
			get {return m_displayname;}
			set {m_displayname = value;}
		}
		public string CompanyMail
		{
			get {return m_CompanyMail;}
			set {m_CompanyMail = value;}
		}
		public string messagetemplate
		{
			get {return m_messagetemplate;}
			set {m_messagetemplate = value;}
		}
        public int RetryCount //FB 2552
        {
            get { return m_RetryCount; }
            set { m_RetryCount = value; }
        }
        public int ServerTimeout //ZD 100317
        {
            get { return m_ServerTimeout; }
            set { m_ServerTimeout = value; }
        }
		#endregion

        #region Added for DashBoard
        public DateTime LastRunDateTime
        {
            get { return m_LastRunDateTime; }
            set { m_LastRunDateTime = value; }
        }
        #endregion
    }
    #endregion
        
    #region vrmUtility

    public class vrmUtility
    {
        private static string PASSPHRASE = "Ac923rKJbsf98hoAFG`1=-] `12r7cb0l";

        public bool simpleEncrypt(ref string data)
        {
            try
            {
                string temp = string.Empty;
                string pass = PASSPHRASE;
                int i, p = 0;
                for (i = 0; i < data.Length; i++) /* process all character in the buffer */
                {
            
                    int iChar = ( data[i] ^ pass[p]);
                    int x = data[i];
                    int y = pass[p];       
                    temp += String.Format("{0:x2}", iChar);
                    p = (p + 1) % pass.Length;  /* determin the next character in the password string to be used; wrap is necessary */
                }
                data = temp.ToUpper(); //Email Link Changes
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
       
        }

        private string StringToHex(string asciiString)
        {
            string hex = "";
            try
            {
                foreach (char c in asciiString)
                {
                    int tmp = c;
                    hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
       
            return hex;
        }
        //FB 2027 - Starts
        private string HexToString(string buff)
        {
            string stringValue = "";
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= buff.Length - 2; i += 2)
                {
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(buff.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                stringValue = sb.ToString();
            }
            catch (Exception e)
            {

                throw e;
            }
            return stringValue;
        }


        private string HexString2Ascii(string hexString)
        {
            hexString = hexString.Substring(2, hexString.Length - 2);
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= hexString.Length - 2; i += 2)
                {
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                return sb.ToString();

            }
            catch (Exception e)
            {
                throw e;
            }
               }
		//FB 2007
        public bool simpleDecrypt(ref string data)
        {
            try
            {
                data = HexString2Ascii(data);
                string temp = string.Empty;
                string pass = PASSPHRASE;
                int i, p = 0;
                for (i = 0; i < data.Length; i++) /* process all character in the buffer */
                {

                    int iChar = (data[i] ^ pass[p]);
                    int x = data[i];
                    int y = pass[p];
                    temp += String.Format("{0:x2}", iChar);
                    p = (p + 1) % pass.Length;  /* determin the next character in the password string to be used; wrap is necessary */
                }
                data = HexToString(temp.ToUpper()); //Email Link Changes
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
    #endregion

    //FB 2027 - Ends

    #region sysTimeZonePref
    // Added for the FB  1069,1070 Starts
    public class sysTimeZonePref
    {
        #region Private Internal Members

        private int m_systemID;
        private string m_name;

        #endregion

        #region Public Properties
        public int systemid
        {
            get { return m_systemID; }
            set { m_systemID = value; }
        }

        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion
    }
    // Added for the FB  1069,1070 Starts
    #endregion

    #region AccountScheme

    public class sysAccScheme
    {
        #region Private Internal Members
        private int m_id;
        private string m_name;
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion
    }
    #endregion

    //FB 2363
    #region sysExternalScheduling

    public class sysExternalScheduling
    {
        #region Private Internal Members

        private int m_id;        
        private string m_partnerName;
        private string m_partnerEmail;
        private string m_partnerURL;
        private string m_userName;
        private string m_password;
        private DateTime m_modifiedDate = DateTime.Now;
        private int m_timeoutValue; //FB 2363

        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public string PartnerName
        {
            get { return m_partnerName; }
            set { m_partnerName = value; }
        }

        public string PartnerEmail
        {
            get { return m_partnerEmail; }
            set { m_partnerEmail = value; }
        }

        public string PartnerURL
        {
            get { return m_partnerURL; }
            set { m_partnerURL = value; }
        }

        public string UserName
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public DateTime ModifiedDate
        {
            get { return m_modifiedDate; }
            set { m_modifiedDate = value; }
        }
        public int TimeoutValue //FB 2363
        {
            get { return m_timeoutValue; }
            set { m_timeoutValue = value; }

        }
        #endregion
    }
    #endregion

    #region ESMailUsrRptSettings

    public class ESMailUsrRptSettings
    {
        #region Private Internal Members

        private int m_UId,m_DeliveryType, m_FrequencyType, m_FrequencyCount, m_Sent,m_orgid;
        private string m_CustomerName, m_RptDestination,m_type;//FB 2363
        private DateTime m_StartTime = DateTime.Now;
        private DateTime m_SentTime = DateTime.Now;
        
        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_UId; }
            set { m_UId = value; }
        }

        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }

        public string RptDestination
        {
            get { return m_RptDestination; }
            set { m_RptDestination = value; }
        }

        public int DeliveryType
        {
            get { return m_DeliveryType; }
            set { m_DeliveryType = value; }
        }

        public int FrequencyType
        {
            get { return m_FrequencyType; }
            set { m_FrequencyType = value; }
        }

        public int Sent
        {
            get { return m_Sent; }
            set { m_Sent = value; }
        }

        public int FrequencyCount
        {
            get { return m_FrequencyCount; }
            set { m_FrequencyCount = value; }
        }
        
        public DateTime StartTime
        {
            get { return m_StartTime; }
            set { m_StartTime = value; }
        }

        public DateTime SentTime
        {
            get { return m_SentTime; }
            set { m_SentTime = value; }
        }

        public string Type
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public int orgId
        {
            get { return m_orgid; }
            set { m_orgid = value; }
        }
        #endregion
    }
    #endregion
	//FB 2392
    #region sysWhygo
    public class sysWhygoSettings
    {
        #region Private Internal Members

        private int m_id;
        private String m_WhyGoURL, m_WhyGoUserName, m_WhyGoPassword, m_WhyGoAdminEmail; //FB 2392 //ZD 100694
        private DateTime m_modifiedDate = DateTime.Now;

        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_id; }
            set { m_id = value; }
        }

        
        public string WhyGoURL
        {
            get { return m_WhyGoURL; }
            set { m_WhyGoURL = value; }
        }
        public string WhyGoUserName
        {
            get { return m_WhyGoUserName; }
            set { m_WhyGoUserName = value; }
        }
        public string WhyGoPassword
        {
            get { return m_WhyGoPassword; }
            set { m_WhyGoPassword = value; }
        }

        public DateTime ModifiedDate
        {
            get { return m_modifiedDate; }
            set { m_modifiedDate = value; }
        }
        //ZD 100694 start
        public string WhyGoAdminEmail
        {
            get { return m_WhyGoAdminEmail; }
            set { m_WhyGoAdminEmail = value; }
        }
        //ZD 100694 End
        #endregion
    }
    #endregion

    //FB 2659 Starts
    #region vrmDefaultLicense
    public class vrmDefaultLicense
    {
        #region Private Internal Members
        private int m_UID, m_UserId, m_MaxVideoRooms, m_MaxNonVideoRooms, m_MaxVMRRooms, m_MCULimit, m_MCUEnhancedLimit;
        private int m_MaxEndpoints, m_UserLimit, m_ExchangeUserLimit, m_DominoUserLimit, m_MobileUserLimit, m_GuestRoomLimit;
        private int m_GuestRoomPerUser, m_EnableFacilities, m_EnableCatering, m_EnableHousekeeping, m_EnableAPI;
        private int m_MaxPCUsers, m_BlueJeansUserLimit, m_MaxenableJabber, m_MaxenableLync, m_MaxenableVidtel, m_MaxVCHotRooms, m_MaxROHotRooms;//ZD 104021
        private int m_EnableAdvancedReport;
        private int m_LastModifiedUser, m_MaxWebexUsers;//FB 3002 //ZD 100221
        private int m_MaxiControlRooms;//ZD 101098
        #endregion

        #region Public Properties
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }
        public int MaxVideoRooms
        {
            get { return m_MaxVideoRooms; }
            set { m_MaxVideoRooms = value; }
        }
        public int MaxNonVideoRooms
        {
            get { return m_MaxNonVideoRooms; }
            set { m_MaxNonVideoRooms = value; }
        }
        public int MaxVMRRooms
        {
            get { return m_MaxVMRRooms; }
            set { m_MaxVMRRooms = value; }
        }
        
        //ZD 101098 START
        public int MaxiControlRooms
        {
            get { return m_MaxiControlRooms; }
            set { m_MaxiControlRooms = value; }
        }
        //ZD 101098 END
        
        public int MCULimit
        {
            get { return m_MCULimit; }
            set { m_MCULimit = value; }
        }
        public int MCUEnhancedLimit
        {
            get { return m_MCUEnhancedLimit; }
            set { m_MCUEnhancedLimit = value; }
        }
        public int MaxEndpoints
        {
            get { return m_MaxEndpoints; }
            set { m_MaxEndpoints = value; }
        }
        public int UserLimit
        {
            get { return m_UserLimit; }
            set { m_UserLimit = value; }
        }
        public int ExchangeUserLimit
        {
            get { return m_ExchangeUserLimit; }
            set { m_ExchangeUserLimit = value; }
        }
        public int DominoUserLimit
        {
            get { return m_DominoUserLimit; }
            set { m_DominoUserLimit = value; }
        }

        public int MobileUserLimit
        {
            get { return m_MobileUserLimit; }
            set { m_MobileUserLimit = value; }
        }
        public int BlueJeansUserLimit //ZD 104021
        {
            get { return m_BlueJeansUserLimit; }
            set { m_BlueJeansUserLimit = value; }
        }
        public int GuestRoomLimit
        {
            get { return m_GuestRoomLimit; }
            set { m_GuestRoomLimit = value; }
        }
        public int GuestRoomPerUser
        {
            get { return m_GuestRoomPerUser; }
            set { m_GuestRoomPerUser = value; }
        }
        public int EnableFacilities
        {
            get { return m_EnableFacilities; }
            set { m_EnableFacilities = value; }
        }
        public int EnableCatering
        {
            get { return m_EnableCatering; }
            set { m_EnableCatering = value; }
        }
        public int EnableHousekeeping
        {
            get { return m_EnableHousekeeping; }
            set { m_EnableHousekeeping = value; }
        }
        public int EnableAPI
        {
            get { return m_EnableAPI; }
            set { m_EnableAPI = value; }
        }
        public int MaxPCUsers
        {
            get { return m_MaxPCUsers; }
            set { m_MaxPCUsers = value; }
        }
        public int MaxenableJabber
        {
            get { return m_MaxenableJabber; }
            set { m_MaxenableJabber = value; }
        }
        public int MaxenableLync
        {
            get { return m_MaxenableLync; }
            set { m_MaxenableLync = value; }
        }
        public int MaxenableVidtel
        {
            get { return m_MaxenableVidtel; }
            set { m_MaxenableVidtel = value; }
        }
        public int MaxVCHotRooms
        {
            get { return m_MaxVCHotRooms; }
            set { m_MaxVCHotRooms = value; }
        }
        public int MaxROHotRooms
        {
            get { return m_MaxROHotRooms; }
            set { m_MaxROHotRooms = value; }
        }
        public int EnableAdvancedReport
        {
            get { return m_EnableAdvancedReport; }
            set { m_EnableAdvancedReport = value; }
        }
        //FB 3002 Start
        public int LastModifiedUser
        {
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
        }
        //FB 3002 End
        //ZD 100221 Starts
        public int MaxWebexUsers
        {
            get { return m_MaxWebexUsers; }
            set { m_MaxWebexUsers = value; }
        }
        //ZD 100221 Ends
        #endregion
    }
    #endregion
    //FB 2659 End

    //ZD 103263 Start
    #region BJNSettings

    public class BJNSettings
    {
        #region Private Internal Members

        private int m_Id, m_BJNExpiry;
        private string m_BJNAppKey, m_BJNAppSecret, m_BJNAccessToken;
        private DateTime m_Lastmodifieddatetime = DateTime.Now;
        private DateTime m_ExpiryDateTime = DateTime.Now;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public int BJNExpiry
        {
            get { return m_BJNExpiry; }
            set { m_BJNExpiry = value; }
        }
        public string BJNAppKey
        {
            get { return m_BJNAppKey; }
            set { m_BJNAppKey = value; }
        }
        public string BJNAppSecret
        {
            get { return m_BJNAppSecret; }
            set { m_BJNAppSecret = value; }
        }
        public string BJNAccessToken
        {
            get { return m_BJNAccessToken; }
            set { m_BJNAccessToken = value; }
        }
        public DateTime Lastmodifieddatetime
        {
            get { return m_Lastmodifieddatetime; }
            set { m_Lastmodifieddatetime = value; }
        }
        public DateTime ExpiryDateTime
        {
            get { return m_ExpiryDateTime; }
            set { m_ExpiryDateTime = value; }
        }
        #endregion
    }
    #endregion
    //ZD 103263 End
}
