//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End  //100886
namespace NS_MESSENGER
{
	#region references
	using System;
	using System.Collections;
	#endregion 

	class ConfigParams
	{
		public string localConfigPath,globalConfigPath,logFilePath,siteUrl,reportFilePath;//FB 2363		
		public string databaseLogin,databaseServer,databasePwd,databaseName;		
		public bool debugEnabled;
        public double activationTimer;
		public ConfigParams()
		{
            activationTimer = Convert.ToDouble(24 * 60 * 60 * 1000);
			debugEnabled = false;
		}
	}
}